const functions = require('firebase-functions');
const cors = require('cors');
const express = require('express');
const bodyParser = require('body-parser');

const deleteIncoherentTrip = require('./helpers/deleteIncoherentTrip');
const geo = require('./routes');

/* Express with CORS */
const app = express();
app.use(cors({ origin: true }));
app.use(bodyParser.json());

app.use('/', geo);

const api = functions.https.onRequest(app);


const periodicTasks = {
  deleteIncoherentTrips: functions.pubsub.schedule('every 12 hours').onRun(async () => {
    await deleteIncoherentTrip();
    console.log('The delete incoherent trip function successfully finished!');
    return;
  })
};

module.exports = { api, periodicTasks };
