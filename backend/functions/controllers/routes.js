const admin = require('firebase-admin');
const { validationResult } = require('express-validator');

const findPassenger = require('../helpers/findPassenger');
const calcCommonRoute = require('../helpers/calcCommonRoute');
const getRouteInfo = require('../helpers/getRouteInfo');
const geoPointToArray = require('../helpers/geoPointToArray');
const isRouteWorthIt = require('../helpers/isRouteWorthIt');
const reverseGeoCode = require('../helpers/reverseGeoCode');
const serviceAccount = require('../serviceAccountKey.json');

//Initialize Firebase app.
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: 'https://mandy-app-ikontent.firebaseio.com'
});

const db = admin.firestore();

const WALKING_TIME_LIMIT = 15 * 60;

// The limit of the maximum age (second) of route during searching.
const TIME_LIMIT = 5;

exports.searchMatchingRoute = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  }

  const { id } = req.query;
  const pickUpLocation = JSON.parse(req.query.pickUpLocation);
  const destination = JSON.parse(req.query.destination);

  // The user which data (id, pickUpCoordinates, destinationCoordinates) collected from API query params.
  let firstUserData = {
    id: id,
    pickUpLocation: pickUpLocation,
    destination: destination
  };

  //Find routes.
  let routes = await findPassenger(
    firstUserData.id,
    new admin.firestore.GeoPoint(pickUpLocation[0], pickUpLocation[1])
  );

  let routePromises = [];

  // Collect all routes within 1 km radius.
  routes.forEach(doc => routePromises.push(doc));

  // Resolve the promises.
  let passengerDocuments = await Promise.all(routePromises);

  // Filter the collected passengers  by:
  //    1, Distance: 0 distance means firstUser. TODO: I am not sure that this will be necessary in the future.
  //    2. Time limit: check wether the last update older the 5 seconds.
  //    3. Walking time: the walking time should be less then 15 minutes.
  //    4. Is the route worth it: wether the selected route is worth it for the firstUser.

  // Use map instead of filter because filter do not support async method.

  let isAlreadyMatched = false;

  const filteredRoutePromises = passengerDocuments.map(async passenger => {
    if (isAlreadyMatched) {
      return false;
    }

    // 1, Distance
    const isFirstUser = passenger.data().id === firstUserData.id;

    if (isFirstUser) {
      return false;
    }

    const routeLastUpdate = passenger.data().createdAt._seconds;
    const ageOfRoute = admin.firestore.Timestamp.now()._seconds - TIME_LIMIT;

    // 2. Time limit
    // TODO: Please invert the relation in production mode.
    const isExceededTimeLimit = routeLastUpdate < ageOfRoute;

    if (isExceededTimeLimit) {
      return false;
    }

    // Fill the potential passenger object with data.
    let potentialPassenger = {
      name: 'passenger',
      id: passenger.data().id,
      pickUpLocation: geoPointToArray(passenger.data().coordinates),
      destination: geoPointToArray(passenger.data().destination)
    };

    let walkingTime = await getRouteInfo(
      'walking',
      potentialPassenger.pickUpLocation,
      firstUserData.pickUpLocation,
      'duration'
    );

    // 3, Walking time
    const isEnoughToWalk = walkingTime < WALKING_TIME_LIMIT;

    if (!isEnoughToWalk) {
      return false;
    }

    // Check the two possible route combination to check which is the fastest.

    // PickUpLocation --> First user destination --> Potential user destination
    const commonDirectionFirst = await calcCommonRoute(
      firstUserData.pickUpLocation,
      potentialPassenger,
      firstUserData
    );

    // PickUpLocation --> Potential user destination --> First user destination
    const commonDirectionSecond = await calcCommonRoute(
      firstUserData.pickUpLocation,
      firstUserData,
      potentialPassenger
    );

    // Get the fastest route based on duration.
    let bestCommonRoute = [commonDirectionFirst, commonDirectionSecond].reduce((prev, curr) =>
      prev.totalDuration < curr.totalDuration ? prev : curr
    );

    let pickUpLocation = {
      address: await reverseGeoCode(potentialPassenger.pickUpLocation),
      coordinates: {
        lat: potentialPassenger.pickUpLocation[0],
        lng: potentialPassenger.pickUpLocation[1]
      }
    };
    bestCommonRoute = {pickUpLocation, ...bestCommonRoute };

    // Get direct distance without intermediate stop. (Fist user's  pickup location --> Best route's drop off location )
    const directRouteLength = await getRouteInfo(
      'driving',
      firstUserData.pickUpLocation,
      bestCommonRoute.dropOffLocation.address,
      'distance'
    );

    //4,  Check wether the route worth it for the first user.
    const isFirstUserWorthIt = isRouteWorthIt(
      directRouteLength,
      bestCommonRoute.intermediateStop.length,
      bestCommonRoute.dropOffLocation.length
    );

    if (!isFirstUserWorthIt) {
      return false;
    }

    // eslint-disable-next-line require-atomic-updates
    isAlreadyMatched = true;

    return { route: bestCommonRoute, id: potentialPassenger.id, routeId: passenger.id };
  });

  try {
    const matchedTrip = (await Promise.all(filteredRoutePromises)).filter(value => value)[0];
    if (matchedTrip) {
      let userRef = db.collection('users');

      let passengerQuerySnapshot = await userRef
        .where('id', '==', matchedTrip.route.intermediateStop.userId)
        .get();
      let initiatorQuerySnapshot = await userRef
        .where('id', '==', matchedTrip.route.dropOffLocation.userId)
        .get();
      let users = {};

      passengerQuerySnapshot.forEach(doc => (users.passenger = doc.data()));
      initiatorQuerySnapshot.forEach(doc => (users.initiator = doc.data()));
      res.status(200).json({
        result: {
          trip: matchedTrip.route,
          users: users,
          matchedRouteId: matchedTrip.routeId
        }
      });
    } else {
      res.status(200).json({ result: [] });
    }
  } catch (err) {
    res.status(500).json({
      result: 'An error occurred during the process of matching route search!'
    });
  }
};
