const admin = require('firebase-admin');
const getRouteInfo = require('../helpers/getRouteInfo');
const calcRouteFare = require('../helpers/calcRouteFare');

const db = admin.firestore();

exports.deleteIntermediateRoute = async (req, res) => {
  const { tripId, pickUpLocation, destination } = req.body;

  const newDuration = await getRouteInfo('driving', pickUpLocation, destination, 'duration');
  const newDistance = await getRouteInfo('driving', pickUpLocation, destination, 'distance');
  const newFare = calcRouteFare.withoutIntermediateStop(newDistance);

  let updatedTripFields = {
    type: 'single',
    duration: newDuration,
    finances: {
      passenger: {
        fare: ''
      },
      initiator: {
        fare: newFare
      }
    },
    'places.intermediateStop': {},
    'users.passenger': {}
  };

  const updateTrip = async () => {
    try {
      await db
        .collection('trips')
        .doc(tripId)
        .update(updatedTripFields);
      return true;
    } catch (error) {
      return false;
    }
  };

  let isTriUpdated = await updateTrip();
  if (isTriUpdated) {
    let tripRef = await db
      .collection('trips')
      .doc(tripId)
      .get();
    if (tripRef.exists) res.status(200).json(tripRef.data());
    res.status(404).json({ result: 'The tripId is not exist!' });
  } else {
    res.status(500).json({ result: 'An error occurred during the trip update' });
  }
};
