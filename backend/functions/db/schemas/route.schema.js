// Routes for every user
const Route = {
  id: '', // firebase api: auth id
  createdAt: new Date.now(),
  coordinates: {
    pickUpLocation: null, // user's current location || selected location; firestore.GeoPoint
    destination: null, // selected location; firestore.GeoPoint
  },
};

module.exports = Route;
