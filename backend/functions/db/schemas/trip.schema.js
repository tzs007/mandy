const Trip = {
  type: 'single', // 'single' or 'joint'
  duration: '', // v0.1 calculated by local algorithm, v0.2 get from Főtaxi API
  status: null, // 'waiting-for-passenger', 'waiting-for-taxi', 'taxi-arrived', 'declined-by-initiator', 'declined-by-passenger', 'started', 'arrived-to-intermediate-stop', 'arrived-to-initiators-destination'
  isInitiatorFoundByPassenger: false,
  isPassengerFoundByInitiator: false,
  startTime: null, // Date
  endTime: null, // Date

  // User data
  users: {
    initiator: {
      id: '',
      name: {}, // user: name object
      rating: 0,
    },
    passenger: {
      id: '',
      name: {}, // user: name object
      rating: 0,
    },
  },

  // Financial data
  finances: {
    isPaid: false,
    receiptId: '', // számlázz.hu api: nyugtaszam
    serviceCommission: 0,
    initiator: {
      earning: 0,
      fare: 0,
    },
    passenger: {
      fare: 0,
    },
  },

  // Initiator's and passenger's route coordinates
  // routes: {
  //  initiatorRoute: null, // firestore.GeoPoint
  //  passengerRoute: null, // firestore.GeoPoint
  // },

  // Trip route coordinates
  coordinates: {
    pickUpLocation: null, // firestore.GeoPoint
    passengerDestination: null, // firestore.GeoPoint
    initiatorDestination: null, // firestore.GeoPoint
  },

  // Estimated Arrival Time
  ETA: {
    atPickUpLocation: null, // Date
    atPassengerDestination: null, // Date
    atInitiatorDestination: null, // Date
  },
};

module.exports = Trip;
