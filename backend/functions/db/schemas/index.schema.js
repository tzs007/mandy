const firebase = require('firebase');
require('@firebase/firestore');
const RouteSchema = require('./route.schema');
const TripSchema = require('./trip.schema');
const UserSchema = require('./user.schema');

const Schema = {
  user: {
    fields: UserSchema,
    collections: ['creditCards', 'addresses', 'ratings'],
  },
  route: {
    fields: RouteSchema,
    collections: [],
  },
  trip: {
    fields: TripSchema,
    collections: [],
  },

  // collections
  ratings: {
    fields: {
      tripId: 0,
      rating: 0,
    },
    collections: [],
  },

  addresses: {
    fields: {
      name: '',
      address: '',
      coordinates: new firebase.firestore.GeoPoint(19.040235, 47.497912), // Budapest, 0km
    },
    collections: [],
  },

  creditCards: {
    fields: {
      externalId: '',
      token: '',
      validity: '',
      mask: '',
    },
    collections: [],
  },

  TEMPLATE: {
    fields: {},
    collections: [],
  },
};

module.exports = Schema;
