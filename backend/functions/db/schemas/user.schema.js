const User = {
  id: '', // firebase api: auth id
  facebookId: '', // facebook api: id
  name: {
    first: '', // facebook api: first_name
    last: '', // facebook api: last_name
    middle: '', // facebook api: middle_name
    short: '', // facebook api: short_name
  },
  picture: '', // facebook api: picture
  email: '', // facebook api: email
  creditCards: [], // stored credit card collection (during MVP, we handle only one credit card)
  addresses: [
    { 
      name: 'Home', 
      address: '',  
      coordinates: null, // firestore.GeoPoint()
    },
    { 
      name: 'Office', 
      address: '', 
      coordinates: null, // firestore.GeoPoint() 
    },
  ],
  ratings: [],
};

module.exports = User;
