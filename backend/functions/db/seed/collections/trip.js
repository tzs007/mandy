const Trip = [
  {
    type: 'joint', 
    duration: 4534, 
    status: 'taxi-arrived', 
    isInitiatorFoundByPassenger: true,
    isPassengerFoundByInitiator: false,
    startTime: '2019-07-11T06:18:02.952Z', 
    endTime: '2019-07-11T07:18:02.952Z', 
    users: {
      initiator: {
        id: '1ce09d7b-795b-4d84-a4f5-2420e1fccd54',
        name: {
          first: 'Ármin', 
          last: 'Szepessy ', 
          middle: '', 
          short: 'Ármin Szepessy'
        }, 
        rating: 4
      },
      passenger: {
        id: '197e130b-ac4a-484d-8e22-2f90798c78af',
        name: {
          first: 'Anna', 
          last: 'Sebők', 
          middle: '', 
          short: 'Anna Sebők'
        },
        rating: 4
      }
    },

    finances: {
      isPaid: true,
      receiptId: '438574357286', 
      serviceCommission: 0,
      initiator: {
        earning: 2000,
        fare: 1000
      },
      passenger: {
        fare: 1000
      }
    }
  },
  {
    type: 'joint', 
    duration: 6434, 
    status: 'waiting-for-passenger', 
    isInitiatorFoundByPassenger: false,
    isPassengerFoundByInitiator: false,
    startTime: '', 
    endTime: '', 
    users: {
      initiator: {
        id: '03c64216-8e58-41bd-a9d5-eb48cd0eafa2',
        name: {
          first: 'Dominika', 
          last: 'Bagi ', 
          middle: '', 
          short: 'Dominika Bagi'
        },
        rating: 4
      },
      passenger: {
        id: '',
        name: {
          first: '', 
          last: '', 
          middle: '', 
          short: ''
        },
        rating: 4
      }
    },

    finances: {
      isPaid: false,
      receiptId: '', 
      serviceCommission: 0,
      initiator: {
        earning: 0,
        fare: 0
      },
      passenger: {
        fare: 0
      }
    }
  },
  {
    type: 'joint', 
    duration: 1234, 
    status: 'waiting-for-taxi', 
    isInitiatorFoundByPassenger: false,
    isPassengerFoundByInitiator: false,
    startTime: '', 
    endTime: '', 
    users: {
      initiator: {
        id: '1ce09d7b-795b-4d84-a4f5-2420e1fccd54',
        name: {
          first: 'Ármin', 
          last: 'Szepessy ', 
          middle: '', 
          short: 'Ármin Szepessy'
        }, 
        rating: 4
      },
      passenger: {
        id: '197e130b-ac4a-484d-8e22-2f90798c78af',
        name: {
          first: 'Anna', 
          last: 'Sebők', 
          middle: '', 
          short: 'Anna Sebők'
        },
        rating: 4
      }
    },
    finances: {
      isPaid: false,
      receiptId: '', 
      serviceCommission: 0,
      initiator: {
        earning: 0,
        fare: 0
      },
      passenger: {
        fare: 0
      }
    }
  },
  {
    type: 'single', 
    duration: 434, 
    status: 'waiting-for-passenger', 
    isInitiatorFoundByPassenger: false,
    isPassengerFoundByInitiator: true,
    startTime: '2019-07-10, 11:21:34', 
    endTime:  '', 
    users: {
      initiator: {
        id: '1ce09d7b-795b-4d84-a4f5-2420e1fccd54',
        name: {
          first: 'Ármin', 
          last: 'Szepessy ', 
          middle: '', 
          short: 'Ármin Szepessy'
        }, 
        rating: 4
      },
      passenger: {
        id: '',
        name: {
          first: '', 
          last: '', 
          middle: '', 
          short: ''
        },
        rating: 0
      }
    },
    finances: {
      isPaid: false,
      receiptId: '', 
      serviceCommission: 0,
      initiator: {
        earning: 0,
        fare: 0
      },
      passenger: {
        fare: 0
      }
    }
  },
  {
    type: 'joint', 
    duration: 434, 
    status: 'arrived-to-initiators-destination', 
    isInitiatorFoundByPassenger: false,
    isPassengerFoundByInitiator: true,
    startTime: '2019-07-10, 11:21:34', 
    endTime:  '', 
    users: {
      initiator: {
        id: '197e130b-ac4a-484d-8e22-2f90798c78af',
        name: {
          first: 'Anna', 
          last: 'Sebők', 
          middle: '', 
          short: 'Anna Sebők'
        },
        rating: 4
      },
      passenger: {
        id: '03c64216-8e58-41bd-a9d5-eb48cd0eafa2',
        name: {
          first: 'Dominika', 
          last: 'Bagi ', 
          middle: '', 
          short: 'Dominika Bagi'
        },
        rating: 4
      }
    },
    finances: {
      isPaid: false,
      receiptId: '', 
      serviceCommission: 0,
      initiator: {
        earning: 0,
        fare: 0
      },
      passenger: {
        fare: 0
      }
    }
  }
];

module.exports = Trip;
