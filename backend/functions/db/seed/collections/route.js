const admin = require('firebase-admin');


const Route = [
  {
    id: '1ce09d7b-795b-4d84-a4f5-2420e1fccd54', // Ármin Szepessy
    createdAt: admin.firestore.Timestamp.now(),
    coordinates: new admin.firestore.GeoPoint(47.505242, 19.023769), // Krisztina krt. 19
    destination: new admin.firestore.GeoPoint(47.500982, 19.110977) // Őrnagy u. 2
  },
  {
    id: '197e130b-ac4a-484d-8e22-2f90798c78af', // Anna Sebők
    createdAt: admin.firestore.Timestamp.now(),
    coordinates: new admin.firestore.GeoPoint(47.497237, 19.024859), // ALkotás u. 2
    destination: new admin.firestore.GeoPoint(47.499737, 19.080627) // Baross tér
  },
  {
    id: '591e28b0-6704-45e1-88f2-adb8c9f03563', // Györffy Renáta Anna
    createdAt: admin.firestore.Timestamp.now(),
    coordinates: new admin.firestore.GeoPoint(47.501295, 19.024694), //Déli páyaudvar
    destination: new admin.firestore.GeoPoint(47.50851, 19.01021) // Szent jános korház
  },
  {
    id: '03c64216-8e58-41bd-a9d5-eb48cd0eafa2', // Dominika Bagi
    createdAt: admin.firestore.Timestamp.now(),
    coordinates: new admin.firestore.GeoPoint(47.507201, 19.025082), //  Széll Kálmán tér
    destination: new admin.firestore.GeoPoint(47.520016, 19.082665) // Város liget
  },
  {
    id: 'bd51bb37-a2dd-483c-8d87-92a6e6b11322', // Bálint Vanda
    createdAt: admin.firestore.Timestamp.now(),
    coordinates: new admin.firestore.GeoPoint(47.508494, 19.019656), // Szilágyi Erzsébet fasor 12
    destination: new admin.firestore.GeoPoint(47.518045, 19.044011) // Margit sziget
  }
];

module.exports = Route;
