const admin = require('firebase-admin');

const User = [
  {
    id: '1ce09d7b-795b-4d84-a4f5-2420e1fccd54',
    facebookId: 'be9766ea-8225-4eac-ad8d-7c4eada6689b',
    name: {
      first: 'Ármin', 
      last: 'Szepessy ', 
      middle: '', 
      short: 'Ármin Szepessy'
    },
    picture: '', 
    email: 'SzepessyArmin@dayrep.com',
    creditCards: [4478897242014490], 
    addresses: [
      {
        name: 'Home',
        address: 'Budapest, Széll Kálmán tér 4',
        coordinates: new admin.firestore.GeoPoint(47.508149, 19.023321) 
      },
      {
        name: 'Office',
        address: 'Baross u. 80, Budapest, 1047',
        coordinates: new admin.firestore.GeoPoint(47.572911, 19.086749)
      }
    ],
    ratings: [
      {
        tripId: 2334,
        rating: 5
      },
      {
        tripId: 26534,
        rating: 4
      }
    ]
  },
  {
    id: '197e130b-ac4a-484d-8e22-2f90798c78af',
    facebookId: '30786620-7eb8-4fc5-a071-af22245a7a2d',
    name: {
      first: 'Anna', 
      last: 'Sebők', 
      middle: '', 
      short: 'Anna Sebők'
    },
    picture: '', 
    email: 'SebokMara@teleworm.us',
    creditCards: [5529939416365666], 
    addresses: [
      {
        name: 'Home',
        address: 'Mézeskalács tér 2, Budapest, 1155',
        coordinates: new admin.firestore.GeoPoint(47.542171, 19.124861) 
      },
      {
        name: 'Office',
        address: 'Kossuth Lajos tér, Budapest, 1055',
        coordinates: new admin.firestore.GeoPoint(47.504402, 19.045694)  
      }
    ],
    ratings: [
      {
        tripId: 24334,
        rating: 5
      },
      {
        tripId: 24334,
        rating: 4
      }
    ]
  },
  {
    id: '591e28b0-6704-45e1-88f2-adb8c9f03563',
    facebookId: 'f98b8f95-50c1-4385-920a-c758d31b5c8e',
    name: {
      first: 'Győrffy', 
      last: 'Renáta ', 
      middle: 'Anna', 
      short: 'Györffy Renáta Anna'
    },
    picture: '', 
    email: 'GyorffyRenata@dayrep.com',
    creditCards: [4478897242014490], 
    addresses: [
      {
        name: 'Home',
        address: 'Bíró u. 6, Budapest, 1122',
        coordinates: new admin.firestore.GeoPoint(47.504503, 19.016372) 
      },
      {
        name: 'Office',
        address: 'Budapest, Alkotás u. 3, 1123',
        coordinates: new admin.firestore.GeoPoint(47.500394, 19.023689) 
      }
    ],
    ratings: [
      {
        tripId: 2334,
        rating: 5
      },
      {
        tripId: 26534,
        rating: 4
      }
    ]
  },
  {
    id: '03c64216-8e58-41bd-a9d5-eb48cd0eafa2',
    facebookId: 'b0d5403b-3668-4c48-995f-c2d7f785c4b8',
    name: {
      first: 'Dominika', 
      last: 'Bagi ', 
      middle: '', 
      short: 'Dominika Bagi'
    },
    picture: '', 
    email: 'BagiDominika@dayrep.com',
    creditCards: [4478897242014490], 
    addresses: [
      {
        name: 'Home',
        address: 'Ruszti út 13-15, Budapest, 1022',
        coordinates: new admin.firestore.GeoPoint(47.514790, 19.014228) 
      },
      {
        name: 'Office',
        address: 'Alig u. 1, Budapest, 1132',
        coordinates: new admin.firestore.GeoPoint(47.517859, 19.059830)  
      }
    ],
    ratings: [
      {
        tripId: 2334,
        rating: 5
      },
      {
        tripId: 26534,
        rating: 4
      }
    ]
  },
  {
    id: 'bd51bb37-a2dd-483c-8d87-92a6e6b11322',
    facebookId: 'be9766ea-8225-4eac-ad8d-7c4eada6689b',
    name: {
      first: 'Vanda', 
      last: 'Bálint', 
      middle: '', 
      short: 'Bálint Vanda'
    },
    picture: '', 
    email: 'BalintVanda@dayrep.com',
    creditCards: [4478897242014490], 
    addresses: [
      {
        name: 'Home',
        address: 'Király u. 54, Budapest, 1068',
        coordinates: new admin.firestore.GeoPoint(47.508149, 19.023321) 
      },
      {
        name: 'Office',
        address: 'Szabadság tér 15, Budapest, 1054',
        coordinates: new admin.firestore.GeoPoint(47.505332, 19.049843)
      }
    ],
    ratings: [
      {
        tripId: 2334,
        rating: 3
      },
      {
        tripId: 26534,
        rating: 4
      },
      {
        tripId: 26534,
        rating: 4
      },
      {
        tripId: 26534,
        rating: 4
      }
    ]
  }
];


module.exports = User;