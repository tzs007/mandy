const admin = require('firebase-admin');
const { GeoFirestore } = require('geofirestore');

//Import mock colelctions
const Users = require('./collections/users');
const Trips = require('./collections/trip');
const Routes = require('./collections/route');

var serviceAccount = require('../../serviceAccountKey.json');

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: 'https://mandy-app-ikontent.firebaseio.com',
});

const db = admin.firestore();
const geodb = new GeoFirestore(admin.firestore());
let userBatch = db.batch();
let tripBatch = db.batch();
let routeBatch = geodb.batch();

(async () => {
  Users.forEach(user => {
    userBatch.set(db.collection('users').doc(), user);
  });

  Trips.forEach(trip => {
    tripBatch.set(db.collection('trips').doc(), trip);
  });

  Routes.forEach(route => {
    routeBatch.set(geodb.collection('routes').doc(), route);
  });

  try {
    let insertedUsers = await userBatch.commit();
    let insertedTrips = await tripBatch.commit();
    let insertedRoutes = await routeBatch.commit();

    console.log(`${insertedUsers.length} users inserted to the database`);
    console.log(`${insertedTrips.length} trips inserted to the database`);
    console.log(`${insertedRoutes.length} routes inserted to the database`);
  } catch (error) {
    console.log(error);
  }
})();
