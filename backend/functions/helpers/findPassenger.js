const admin = require('firebase-admin');
const { GeoFirestore } = require('geofirestore');

module.exports = (userId, pickUpLocation) => {
  const geofirestore = new GeoFirestore(admin.firestore());
  const geocollection = geofirestore.collection('routes');

  // TODO: Find solution for query based date query.
  // Currently there is no geofirestore solution for this: https://github.com/geofirestore/geofirestore-js/issues/78
  return geocollection
    .near({
      center: pickUpLocation,
      radius: 1,
    })
    .get(value => value);
};
