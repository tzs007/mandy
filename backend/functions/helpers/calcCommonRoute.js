const calcRouteFare = require('../helpers/calcRouteFare');

module.exports = async (origin, firstStop, secondStop) => {
  let trip = {};

  const googleMapsClient = require('@google/maps').createClient({
    key: 'AIzaSyAr9jWtvXBEljHI7eeCYHuyU8noeqC2Kho',
    Promise: Promise
  });

  try {
    const googleResponse = await googleMapsClient
      .directions({
        origin: origin,
        destination: secondStop.destination,
        mode: 'driving',
        waypoints: [firstStop.destination]
      })
      .asPromise();


    const tripKeys = ['intermediateStop', 'dropOffLocation'];

    googleResponse.json.routes[0].legs.forEach((route, index) => {
      trip[tripKeys[index]] = {
        address: route.end_address,
        coordinates: route.end_location,
        userType: tripKeys[index] === 'intermediateStop' ? 'passenger' : 'initiator',
        userId: index === 0 ? firstStop.id : secondStop.id,
        length: route.distance.value,
        duration: route.duration.value,
        fare: calcRouteFare.withIntermediateStop(
          tripKeys[index],
          googleResponse.json.routes[0].legs[0].distance.value,
          googleResponse.json.routes[0].legs[1].distance.value
        )
      };
    });

    trip.totalDuration = trip.intermediateStop.duration + trip.dropOffLocation.duration;
    return trip;
  } catch (error) {
    return error;
  }
};
