const admin = require('firebase-admin');

module.exports = async () => {
  const db = admin.firestore();
  const TIME_LIMIT = 60 * 60; // 1 hour
  const tripRef = await db.collection('trips');

  const tripsQuerySnapshot = await tripRef.where('status', '==', 'started').get();

  let deletedTripPromises = [];
  tripsQuerySnapshot.forEach(doc => {
    const createdAt = doc.data().createdAt._seconds;
    const now = admin.firestore.Timestamp.now()._seconds;
    if (createdAt < now - TIME_LIMIT) {
      let deletedTrip = tripRef.doc(doc.id).delete();
      deletedTripPromises.push(deletedTrip);
    }
  });
  console.log(`${deletedTripPromises.length} trip deleted!`);
  await Promise.all(deletedTripPromises);
};
