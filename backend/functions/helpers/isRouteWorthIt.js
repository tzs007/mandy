module.exports = (originalRouteLength, firstStopLength, secondStopLength) => {
  return originalRouteLength - (firstStopLength / 2 + secondStopLength) > 0;
};
