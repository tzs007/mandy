module.exports = async (coords) => {
  const googleMapsClient = require('@google/maps').createClient({
    key: 'AIzaSyAr9jWtvXBEljHI7eeCYHuyU8noeqC2Kho',
    Promise: Promise
  });

  try {
    const googleResponse = await googleMapsClient
      .reverseGeocode({
        latlng: [coords]
      })
      .asPromise();

    return googleResponse.json.results[0].formatted_address;
  } catch (error) {
    return error;
  }
};
