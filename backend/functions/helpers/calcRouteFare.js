exports.withIntermediateStop = (
  stopType,
  firstPartOfRoute,
  secondPartOfRoute
) => {
  // Temporarily until we can't use real TAXI api.
  const TAXI_BASIC_FEE = 700;
  const TAXI_KM_CHARGE = 300;

  // firstPartOfRoute comes in meter
  const routeFare =
    stopType === 'intermediateStop'
      ? (firstPartOfRoute / 2 / 1000) * TAXI_KM_CHARGE
      : ((firstPartOfRoute / 2 + secondPartOfRoute) / 1000) * TAXI_KM_CHARGE;

  return routeFare + TAXI_BASIC_FEE;
};

exports.withoutIntermediateStop = routeDistance => {
  // Temporarily until we can't use real TAXI api.
  const TAXI_BASIC_FEE = 700;
  const TAXI_KM_CHARGE = 300;

  return (routeDistance / 1000) * TAXI_KM_CHARGE + TAXI_BASIC_FEE;
};
