module.exports = async (mode,origin, destination, output) => {
  const googleMapsClient = require('@google/maps').createClient({
    key: 'AIzaSyAr9jWtvXBEljHI7eeCYHuyU8noeqC2Kho',
    Promise: Promise
  });

  try {
    const googleResponse = await googleMapsClient
      .directions({
        origin: origin,
        destination: destination,
        mode: mode
      })
      .asPromise();

    return googleResponse.json.routes[0].legs[0][output].value;
  } catch (error) {
    return error;
  }
};
