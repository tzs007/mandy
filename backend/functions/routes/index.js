const express = require('express');
const { query, body } = require('express-validator');
const routes = require('../controllers/routes');
const trips = require('../controllers/trips');
const router = express.Router();

router.get(
  '/search-route',
  [
    query('id')
      .exists()
      .withMessage('Id is required')
      .isString()
      .withMessage('Id must be string'),
    query('pickUpLocation')
      .exists()
      .withMessage('Pickup location is required')
      .custom(value => Array.isArray(JSON.parse(value)))
      .withMessage('Pickup location must be array'),
    query('destination')
      .exists()
      .withMessage('Destination is required')
      .custom(value => Array.isArray(JSON.parse(value)))
      .withMessage('Pickup location must be array')
  ],
  routes.searchMatchingRoute
);

router.post(
  '/delete-intermediate-stop',
  [
    body('id')
      .exists()
      .withMessage('Id is required')
      .isString()
      .withMessage('Id must be string'),
    body('deletedUserType')
      .exists()
      .withMessage('Id is required')
      .isString()
      .withMessage('Id must be string'),
    body('pickUpLocation')
      .exists()
      .withMessage('Pickup location is required')
      .custom(value => Array.isArray(JSON.parse(value)))
      .withMessage('Pickup location must be array'),
    body('destination')
      .exists()
      .withMessage('Destination is required')
      .custom(value => Array.isArray(JSON.parse(value)))
      .withMessage('Pickup location must be array')
  ],
  trips.deleteIntermediateRoute
);

router.all('*', (req, res) =>
  res.status(404).send(JSON.stringify({ result: '404 - there is no valid endpoint' }))
);

module.exports = router;
