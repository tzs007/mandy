# Mobile App

## First install

- npm i -g expo-cli firebase-tools eslint
- npm i

## Launch

- cd app
- expo start

# Backend

## First install

- npm i -g eslint
- restart your terminal
- cd backend
- eslint --init

## Launch

- cd backend
- firebase login
- firebase serve --only functions

## Deploy

- firebase deploy --only functions
