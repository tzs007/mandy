import React from "react";
import { View, Text, StyleSheet } from "react-native";
import { Ionicons, MaterialIcons } from "@expo/vector-icons";

const ProfileListItem = ({ firstIcon, lastIcon, text, note }) => {
  return (
    <View style={styles.profileListItem}>
      <View style={styles.profileListItemIconWrapper}>
        {firstIcon ? (
          <Ionicons
            active
            name={firstIcon}
            color="#413B38"
            size={20}
            style={styles.profileIcons}
          />
        ) : null}
      </View>
      <View
        style={[
          styles.profileListItemTextWrapper,
          { paddingVertical: note ? 5 : 15 },
        ]}
      >
        <Text style={styles.profileListItemText}>{text}</Text>
        {note ? (
          <Text note numberOfLines={1} style={styles.listItemNoteText}>
            {note}
          </Text>
        ) : null}
      </View>
      <View style={styles.profileListItemIconWrapper}>
        {lastIcon ? (
          <MaterialIcons
            active
            name={lastIcon}
            color="#413B38"
            size={20}
            style={styles.profileIcons}
          />
        ) : null}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  profileListItem: {
    flex: 1,
    flexDirection: "row",
    paddingHorizontal: 10,
  },
  profileListItemIconWrapper: {
    alignItems: "center",
    justifyContent: "center",
  },
  profileListItemTextWrapper: {
    paddingHorizontal: 15,
    alignItems: "flex-start",
    flex: 1,
  },
  profileListItemText: {
    fontSize: 17,
  },
  profileIcons: {
    opacity: 0.3,
  },
  listItemNoteText: {
    width: "100%",
    textAlign: "left",
    color: "#413B38",
    opacity: 0.3,
  },
});

export default ProfileListItem;
