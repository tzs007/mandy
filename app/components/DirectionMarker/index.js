import React from 'react';
import { StyleSheet, View } from 'react-native';

const DirectionMarker = ({ type }) => {
  return (
    <View style={type === 'intermediateStop' || type === 'currentPosition' ? styles[`${type}Wrapper`] : {}} pointerEvents="none">
      <View style={styles[`${type}Marker`]} />
    </View>
  );
};

const styles = StyleSheet.create({
  // TODO: From the PositionMarker component the currentPosition marker currently is not working properly on 
  //       the RouteOfferScreen/WalkingRoutePlansScreen  therefore I implemented a new one here. 
  //       We should introduce a solution which working on all screens.
  currentPositionWrapper: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 30,
    height: 30,
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: '#f37121',
    backgroundColor: 'rgba(244, 113, 33, 0.2)',
    borderRadius: 100,
  },
  currentPositionMarker: {
    width: 15,
    height: 15,
    backgroundColor: '#f37121',
    borderWidth: 3,
    borderStyle: 'solid',
    borderColor: '#ffffff',
    borderRadius: 100,
  },
  intermediateStopWrapper: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 15,
    height: 15,
    borderWidth: 4,
    borderStyle: 'solid',
    borderColor: '#f37121',
    backgroundColor: 'transparent',
    borderRadius: 100
  },
  intermediateStopMarker: {
    width: 12,
    height: 12,
    backgroundColor: '#f37121',
    borderWidth: 3,
    borderStyle: 'solid',
    borderColor: '#ffffff',
    borderRadius: 100
  },
  pickUpLocationMarker: {
    width: 15,
    height: 15,
    backgroundColor: '#f37121',
    borderWidth: 2,
    borderStyle: 'solid',
    borderColor: '#f37121',
    borderRadius: 100
  },
  destinationMarker: {
    width: 15,
    height: 15,
    backgroundColor: '#ffffff',
    borderWidth: 2,
    borderStyle: 'solid',
    borderColor: '#f37121',
    borderRadius: 100
  }
});

export default DirectionMarker;
