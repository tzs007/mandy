import React, { PureComponent } from 'react';
import { StyleSheet } from 'react-native';
import { withNavigation } from 'react-navigation';
import { Header, Left, Button, Body, Title, Right } from 'native-base';
import { Ionicons } from '@expo/vector-icons';

class AppHeader extends PureComponent {
  _navigate = () => {
    const { navigation } = this.props;
    if (this.props.navigateto) {
      navigation.navigate(this.props.navigateto);
    } else {
      navigation.goBack();
    }
  };

  render() {
    const { title, back, onSaveNavigation } = this.props;
    return (
      <Header noShadow style={styles.header}>
        <Left style={{ flex: 1 }}>
          <Button
            small
            primary
            rounded
            noShadow
            onPress={this._navigate}
            style={back ? {...styles.exitButton, ...styles.backButton} : styles.exitButton}
          >
            <Ionicons
              name={back ? 'ios-arrow-round-back' : 'ios-close'}
              size={25}
              color="#413B38"
            />
          </Button>
        </Left>
        <Body
          style={{ flex: 10, flexDirection: 'row', justifyContent: 'center' }}
        >
          <Title style={{ fontWeight: 'normal' }}>{title}</Title>
        </Body>
        <Right style={{ flex: 1 }} />
        {onSaveNavigation && (
          <Right style={{ flex: 1 }}>
            <Button
              small
              success
              rounded
              noShadow
              onPress={onSaveNavigation ? onSaveNavigation : this._navigate}
              style={styles.confirmButton}
            >
              <Ionicons name={'ios-checkmark'} size={25} color="#9ACA3C" />
            </Button>
          </Right>
        )}
      </Header>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    paddingLeft: 15,
    paddingRight: 15,
    backgroundColor: 'transparent',
    fontFamily: 'Roboto-Bold',
    fontWeight: 'bold',
    height: 100
  },
  exitButton: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 40,
    height: 40,
    borderWidth: 1,
    paddingLeft: 6,
    borderColor: '#D9D9D9',
    backgroundColor: '#fff',
    borderStyle: 'solid'
  },
  backButton: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.3,
    shadowRadius: 5,
    elevation: 5,
    borderColor: 'white'
  },
  confirmButton: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 40,
    height: 40,
    borderWidth: 0,
    paddingLeft: 6,
    borderColor: '#9ACA3C',
    backgroundColor: '#fff',
    borderStyle: 'solid'
  }
});
export default withNavigation(AppHeader);
