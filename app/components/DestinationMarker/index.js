import React from 'react';
import { StyleSheet, View } from 'react-native';

const DestinationMarker = props => {
  return (
    <View
      style={
        props.fixed
          ? styles.currentPositionMarkerWrapperFixed
          : styles.currentPositionMarkerWrapper
      }
      pointerEvents="none"
    >
      <View
        style={
          props.fixed
            ? styles.currentPositionMarkerFixed
            : styles.currentPositionMarker
        }
      />
    </View>
  );
};

const styles = StyleSheet.create({
  currentPositionMarkerWrapper: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 30,
    height: 30,
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: 'rgb(228, 185, 128)',
    backgroundColor: 'rgba(228, 185, 128, 0.2)',
    borderRadius: 100,
  },
  currentPositionMarker: {
    width: 15,
    height: 15,
    backgroundColor: 'rgb(228, 185, 128)',
    borderWidth: 3,
    borderStyle: 'solid',
    borderColor: '#ffffff',
    borderRadius: 100,
  },
  currentPositionMarkerWrapperFixed: {
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    width: 30,
    height: 30,
    top: '50%',
    left: '50%',
    marginTop: -30,
    marginLeft: -15,
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: '#f37121',
    backgroundColor: 'rgba(244, 113, 33, 0.2)',
    borderRadius: 100,
  },
  currentPositionMarkerFixed: {
    width: 15,
    height: 15,
    backgroundColor: '#f37121',
    borderWidth: 3,
    borderStyle: 'solid',
    borderColor: '#ffffff',
    borderRadius: 100,
  },
});

export default DestinationMarker;
