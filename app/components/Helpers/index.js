import React from 'react';
import { Text, View } from 'react-native';
import { Button } from 'native-base';

import definitions from '../../assets/styles/definitions';
import styleDef from '../../assets/styles/mandy';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { AntDesign } from '@expo/vector-icons';

// Layout helpers
const ContentBox = ({ children = [], cells = false }) => (
  <View
    style={{
      marginHorizontal: 20,
      flexDirection: cells ? 'row' : 'column',
      alignItems: cells ? 'center' : 'flex-start',
    }}
  >
    {children}
  </View>
);

const Spacer = ({ w = 0, h = 0, flex = 0 }) => (
  flex
    ? (<View style={{ flex: 1 }} />)
    : (<View style={{ width: w, maxWidth: w, height: h, maxHeight: h }} />)
);

const Separator = () => (
  <View style={{ width: '100%', height: 1, backgroundColor: definitions.colors.MANDY_SEPARATOR }} />
);

const Cells = ({ children = [], cellStyle = [], style = {}, stretch = true }) => {
  const mergedStyle = { flexDirection: 'row', ...style };
  if (stretch) style.width = '100%';
  return (
    <View style={mergedStyle} >
      {[...Array(children.length).keys()].map((index) => (
        <View key={`cellItem${index}`} style={cellStyle[index] || {}}>
          {children[index]}
        </View>
      ))}
    </View>
  )
};

const Center = ({ children = [], style = {} }) => (
  <View style={{ width: '100%', flexDirection: 'row', justifyContent: 'center', ...style }}>{children}</View>
);

// Text helpers
const Caption = ({ children = [], style = {} }) => (
  <Text style={{ color: definitions.colors.MANDY_GRAY, ...style }}>{children}</Text>
);

const Strong = ({ children = [] }) => (
  <Text style={{ fontSize: 15, fontWeight: '800' }} >{children}</Text>
);

const PullDown = ({ children = [] }) => (
  <View
    style={{ flex: 1, justifyContent: 'center', flexDirection: 'row', alignItems: 'flex-end' }}
  >
    <View style={{ alignSelf: 'flex-end', flex: 1, position: 'relative' }}>
      {children}
    </View>
  </View>
);

const BrickButton = (props) => (
  <View style={{ width: '100%', ...props.outerStyle || {} }}>
    <TouchableOpacity
      {...props}
      style={{
        backgroundColor: definitions.colors.MANDY_ORANGE,
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        height: 48,
        ...props.style || {}
      }}
    >
      <Text
        style={{
          textTransform: 'uppercase',
          fontWeight: '800',
          color: definitions.colors.WHITE,
          ...props.textStyle || {}
        }}
      >
        {props.children || []}
      </Text>
    </TouchableOpacity>
  </View>
);

const RoundButton = ({ style, iconStyle, onPress, name }) => (
  <Button
    primary
    small
    rounded
    noShadow
    transparent
    badge
    onPress={onPress}
    style={{
      justifyContent: 'center',
      alignItems: 'center',
      width: iconStyle.size * 2,
      height: iconStyle.size * 2,
      borderWidth: 2,
      borderColor: '#ffffff',
      borderStyle: 'solid',
      ...styleDef.travelDetailsOpenButton,
      ...style
    }}
  >
    <AntDesign
      style={{ marginTop: 3 }}
      name={name} size={iconStyle.size || 15}
      color={iconStyle.color || definitions.colors.MANDY_DARK_GRAY}
    />
  </Button>
)

export { ContentBox, Spacer, Separator, Cells, Center, Caption, Strong, PullDown, BrickButton, RoundButton };
