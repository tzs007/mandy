import React, { PureComponent } from 'react';
import { View } from 'react-native';

import styleDef from '../../assets/styles/mandy';
import definitions from '../../assets/styles/definitions';

import { AntDesign } from '@expo/vector-icons';

export default class VerticalDots extends PureComponent {
  getFillDots = (count) => {
    return [...Array(count).keys()].map((key) => (
      <View key={`fillerDot${key}`} style={styleDef.verticalDotsFillDot} />
    ));
  };

  render() {
    const { type = 'verticalDotsStartDot', fillDotsCount = 4, icon } = this.props;
    return (
      <View
        style={{
          ...styleDef.verticalDotsContainer,
          justifyContent: type !== 'verticalDotsEndDot' ? 'space-between' : 'flex-start',
          paddingBottom: type !== 'verticalDotsEndDot' ? 12 : 0
        }}
      >
        {
          icon
            ? (
              <View style={{ marginTop: -6, marginLeft: 3 }}>
                {icon}
              </View>
            )
            : (<View style={styleDef[type]} />)
        }

        {
          type !== 'verticalDotsEndDot' ? (this.getFillDots(fillDotsCount)) : (<View />)
        }
      </View>
    )
  }
}