import React, { PureComponent } from 'react';
import { View } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { TouchableOpacity } from 'react-native-gesture-handler';

export default class Rating extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      stars: this.props.stars || 0,
    };
  }

  rate = star => {
    const { onRate = () => {}, fixed = false } = this.props;
    if (fixed) return;

    this.setState(
      {
        stars: star + 1,
      },
      () => {
        onRate({ stars: star + 1, user: null });
      },
    );
  };

  render() {
    const { size = 15, fixed = false } = this.props;
    const { stars } = this.state;
    return (
      <View style={{ flexDirection: 'row' }}>
        {[...Array(5).keys()].map(star => (
          <TouchableOpacity
            activeOpacity={fixed ? 1.0 : 0.5}
            key={star}
            onPress={() => {
              this.rate(star);
            }}
          >
            <Ionicons
              active
              name="md-star"
              color={star <= stars - 1 ? '#F47121' : '#ccc'}
              size={size}
              style={{ paddingRight: 3 }}
            />
          </TouchableOpacity>
        ))}
      </View>
    );
  }
}
