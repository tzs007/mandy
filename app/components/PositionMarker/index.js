import React from 'react';
import { StyleSheet, View } from 'react-native';

const PositionMarker = props => {
  return (
    <View
      style={
        props.fixed
          ? props.pin
            ? styles.currentPositionPinWrapperFixed
            : styles.currentPositionMarkerWrapperFixed
          : styles.currentPositionMarkerWrapper
      }
      pointerEvents="none"
    >
      <View
        style={
          props.fixed
            ? props.pin
              ? styles.currentPositionPinFixed
              : styles.currentPositionMarkerFixed
            : styles.currentPositionMarker
        }
      >
        {props.pin && <View style={styles.pinLeg} />}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  currentPositionMarkerWrapper: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 30,
    height: 30,
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: '#f37121',
    backgroundColor: 'rgba(244, 113, 33, 0.2)',
    borderRadius: 100,
  },
  currentPositionMarker: {
    width: 15,
    height: 15,
    backgroundColor: '#f37121',
    borderWidth: 3,
    borderStyle: 'solid',
    borderColor: '#ffffff',
    borderRadius: 100,
  },
  currentPositionMarkerWrapperFixed: {
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    width: 30,
    height: 30,
    top: '50%',
    left: '50%',
    marginTop: -30,
    marginLeft: -15,
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: '#f37121',
    backgroundColor: 'rgba(244, 113, 33, 0.2)',
    borderRadius: 100,
  },
  currentPositionMarkerFixed: {
    width: 15,
    height: 15,
    backgroundColor: '#f37121',
    borderWidth: 3,
    borderStyle: 'solid',
    borderColor: '#ffffff',
    borderRadius: 100,
  },
  currentPositionPinWrapperFixed: {
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    width: 36,
    height: 36,
    top: '50%',
    left: '50%',
    marginTop: -36,
    marginLeft: -18,
  },
  currentPositionPinFixed: {
    width: 36,
    height: 36,
    backgroundColor: '#fff',
    borderWidth: 4,
    borderStyle: 'solid',
    borderColor: '#F47121',
    borderRadius: 100,
    marginTop: -52,
  },
  pinLeg: {
    width: 4,
    height: 16,
    backgroundColor: '#F47121',
    position: 'absolute',
    left: '50%',
    bottom: -16,
    marginLeft: -2,
  },
});

export default PositionMarker;
