import React, { PureComponent } from 'react';
import { View, Text } from 'react-native';
import { H2 } from 'native-base';
import VerticalDots from '../../components/VerticalDots';
import styleDef from '../../assets/styles/mandy';
import definitions from '../../assets/styles/definitions';

export default class DotList extends PureComponent {
  getListItem = (cfg, index) => {
    return (
      <View
        style={styleDef.travelDetailsLocationBlock}
        key={`dotListItem${index}`}
      >
        <View style={styleDef.travelDetailsColumnIcon}>
          <VerticalDots
            fillDotsCount={cfg.dotCount}
            type={cfg.dotType}
            icon={cfg.icon}
          />
        </View>
        <View style={{...styleDef.travelDetailsLocationColumn}, cfg.isOtherPassenger ? {opacity: 0.5} : {}}>
          {cfg.title ? (
            <H2
              style={[
                styleDef.travelDetailsLocationTitle
              ]}
            >
              {cfg.title}
            </H2>
          ) : (
            <View />
          )}
          {cfg.caption ? (
            <Text
              style={[
                styleDef.travelDetailsLocationCaption,
                cfg.captionStyle || {}
              ]}
            >
              {cfg.caption}
            </Text>
          ) : (
            <View />
          )}
        </View>
      </View>
    );
  };

  getListItems = items => {
    return (
      <View>
        {items.map((item, index) => this.getListItem(item, index))}
      </View>
    );
  };

  render() {
    const { items = [] } = this.props;
    return <View style={styleDef.dotListContainer}>{this.getListItems(items)}</View>;
  }
}
