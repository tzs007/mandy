import React from 'react';
import { View } from 'react-native';

import { Spacer, Cells, Caption, Strong } from '../../components/Helpers/index';

export default class PaymentInfo extends React.PureComponent {
  render() {
    const {
      taxiAmountText = '',
      taxiAmount = '',
      fellowAmountText = '',
      fellowAmount = '',
      finalAmountText = '',
      finalAmount = '',
      style = ''
    } = this.props;

    return (
      <>
        {taxiAmountText ? (
          <>
            <Cells cellStyle={[{ flex: 1 }]}>
              <Caption style={style}>{taxiAmountText}</Caption>
              <Spacer flex />
              <Caption style={style}>{taxiAmount}</Caption>
            </Cells>
          </>
        ) : (
          <View />
        )}
        {fellowAmountText ? (
          <>
            <Spacer h={15} />
            <Cells cellStyle={[{ flex: 1 }]}>
              <Caption style={style}>{fellowAmountText}</Caption>
              <Spacer flex />
              <Caption style={style}>{fellowAmount}</Caption>
            </Cells>
          </>
        ) : (
          <View />
        )}
        {finalAmountText ? (
          <>
            <Spacer h={30} />
            <Cells cellStyle={[{ flex: 1 }]}>
              <Strong>{finalAmountText}</Strong>
              <Spacer flex />
              <Strong>{finalAmount}</Strong>
            </Cells>
          </>
        ) : (
          <View />
        )}
      </>
    );
  }
}
