import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { AntDesign } from '@expo/vector-icons';

const DirectionLabel = ({ labelText }) => {
  return (
    <View style={styles.label}>
      <Text note style={styles.labelText}>{labelText}</Text>
      <View style={styles.labelIcon}>
        <AntDesign name={'right'} size={12} color="#D5D5D5" />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  label: {
    paddingVertical: 12,
    paddingHorizontal: 15,
    width: 250,
    flexDirection: 'row',
    flexWrap: 'nowrap',
    justifyContent: 'space-between',
    backgroundColor: '#ffffff'
  },
  labelText: {
    fontWeight: 'bold',
  },
  labelIcon: {
    justifyContent: 'center',
    alignItems: 'flex-end'
  }
});

export default DirectionLabel;
