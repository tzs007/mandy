import React, { PureComponent } from 'react';
import { View, Animated, Text, ScrollView } from 'react-native';
import { H2 } from 'native-base';

import DotList from '../../components/DotList';
import { AntDesign, Ionicons } from '@expo/vector-icons';

import styleDef from '../../assets/styles/mandy';
import definitions from '../../assets/styles/definitions';

import PaymentInfo from '../../components/PaymentInfo';

import {
  ContentBox,
  Spacer,
  RoundButton,
  Cells,
  BrickButton,
  Separator,
  Center
} from '../../components/Helpers/index';
import {
  TouchableOpacity,
  TouchableWithoutFeedback
} from 'react-native-gesture-handler';

export default class TravelDetails extends PureComponent {
  state = {
    heightAnim: new Animated.Value(0),
    isDetailsOpen: false,
    detailsOpenHeight: 0
  };

  onDetailsLayout = e => {
    this.setState({
      detailsOpenHeight: e.nativeEvent.layout.height
    });
  };

  onDetailsToggle = () => {
    const { heightAnim, isDetailsOpen, detailsOpenHeight } = this.state;

    Animated.timing(heightAnim, {
      toValue: isDetailsOpen ? 0 : detailsOpenHeight,
      duration: definitions.animation.duration.FAST
    }).start(() => {
      this.setState({
        isDetailsOpen: !isDetailsOpen
      });
    });
  };

  onHeaderButtonClick = () => {};

  render() {
    const {
      header,
      plateNumber,
      pathDetails,
      paymentInfo,
      onCancel,
      cancelText
    } = this.props;
    const { heightAnim, isDetailsOpen } = this.state;

    return (
      <View style={[styleDef.travelDetailsOuter]}>
        {/* Nyitó */}
        <RoundButton
          name={isDetailsOpen ? 'down' : 'up'}
          style={{
            backgroundColor: isDetailsOpen
              ? definitions.colors.WHITE
              : definitions.colors.WHITE_OPAQUE
          }}
          iconStyle={{
            size: 22,
            color: '#4B4542',
            marginTop: 3
          }}
          onPress={this.onDetailsToggle}
        />
        <TouchableWithoutFeedback onPress={this.onDetailsToggle}>
          <ContentBox>
            <Spacer h={25} />
            <Cells
              cellStyle={[
                { flexBasis: '7%', marginRight: 9 },
                { flexBasis: '43%' },
                { flexBasis: '7%', marginRight: 9 },
                { flexBasis: '43%' }
              ]}
            >
              {header.left.icon}
              <View style={styleDef.travelDetailsColumnTextBlock}>
                <H2 style={styleDef.travelDetailsColumnTitle}>
                  {header.left.title}
                </H2>
                <Text style={styleDef.travelDetailsCaption}>
                  {header.left.caption}
                </Text>
              </View>

              {header.right.icon}
              <View style={styleDef.travelDetailsColumnTextBlock}>
                <H2 style={styleDef.travelDetailsColumnTitle}>
                  {header.right.title}
                </H2>
                <Text style={styleDef.travelDetailsCaption}>
                  {header.right.caption}
                </Text>
              </View>
            </Cells>
            <Spacer h={25} />
          </ContentBox>

          {header.buttonHandler && header.buttonText ? (
            <>
              <ContentBox>
                <BrickButton
                  onPress={header.buttonHandler || this.onHeaderButtonClick}
                  textStyle={{ textTransform: 'none' }}
                >
                  {' '}
                  {header.buttonText}{' '}
                </BrickButton>
              </ContentBox>
              <Spacer h={30} />
            </>
          ) : (
            <View />
          )}

          {/* Details */}

          <Animated.View
            style={{
              height: heightAnim,
              backgroundColor: definitions.colors.MANDY_GRAY,
              overflow: 'hidden'
            }}
          >
            <View />
            <ScrollView
              style={
                styleDef[
                  `travelDetailsInnerContainer${
                    styleDef.deviceHeight < 600 ? 'SmallDevice' : ''
                  }`
                ]
              }
              onLayout={this.onDetailsLayout}
            >
              <Separator />
              <Spacer h={30} />

              {plateNumber ? (
                <ContentBox>
                  <Cells cellStyle={[{ flexBasis: '7%', marginRight: 9 }]}>
                    <AntDesign
                      name="car"
                      size={22}
                      color={definitions.colors.MANDY_DARK_GRAY}
                    />
                    <H2
                      style={[
                        styleDef.travelDetailsColumnTitle,
                        styleDef.travelDetailsPlateNumber
                      ]}
                    >
                      {plateNumber}
                    </H2>
                  </Cells>
                  <Spacer h={30} />
                </ContentBox>
              ) : (
                <View />
              )}
              {/* Travel destinations list */}
              {pathDetails ? (
                <ContentBox>
                  <DotList items={pathDetails} />
                </ContentBox>
              ) : (
                <View />
              )}
              {paymentInfo ? (
                <>
                  <Separator />
                  <ContentBox>
                    {paymentInfo.fellowAmountText ? <Spacer h={15} /> : null}
                    <PaymentInfo {...paymentInfo} />
                    {onCancel && cancelText ? (
                      <Spacer h={80} />
                    ) : (
                      <Spacer h={30} />
                    )}
                  </ContentBox>
                </>
              ) : (
                <View />
              )}
            </ScrollView>
            <View style={styleDef.cancelButtonTextWrapper}>
              {onCancel && cancelText ? (
                <TouchableOpacity activeOpacity={0.8} onPress={onCancel}>
                  <Center style={styleDef.cancelButtonOuter}>
                    <Cells stretch={false} style={{ alignItems: 'center' }}>
                      <View style={styleDef.cancelButton}>
                        <Ionicons
                          name={'ios-close'}
                          size={25}
                          color={definitions.colors.MANDY_DARK_GRAY}
                        />
                      </View>
                      <Text style={styleDef.cancelButtonText}>
                        {' '}
                        {cancelText}{' '}
                      </Text>
                    </Cells>
                  </Center>
                </TouchableOpacity>
              ) : (
                <View />
              )}
            </View>
          </Animated.View>
        </TouchableWithoutFeedback>
      </View>
    );
  }
}
