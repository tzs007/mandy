export default {
    colors: {
        BLACK: '#000000',
        WHITE: '#ffffff',
        WHITE_OPAQUE: 'rgba(255,255,255,0.75)',
        MANDY_ORANGE: '#F47121',
        MANDY_ORANGE_OPAQUE: 'rgba(244, 113, 33, 0.2)',
        MANDY_DARK_GRAY: '#413B38',
        MANDY_GRAY: 'rgba(209, 209, 209, 1)',
        MANDY_LIGHT_GRAY_OPAQUE: 'rgba(0, 0, 0, 0.11)',
        MANDY_SEPARATOR: '#F3F3F3'
    },
    animation: {
        duration: { // in MS
            FAST: 200
        }
    }
}