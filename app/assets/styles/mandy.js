import { StyleSheet, Dimensions } from 'react-native';
import definitions from './definitions';

const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;

const styles = {
  deviceHeight,
  deviceWidth,

  // Travel details block
  travelDetailsLocationBlock: {
    flexDirection: 'row',
    width: '100%'
  },
  travelDetailsCaption: {
    color: definitions.colors.MANDY_LIGHT_GRAY_OPAQUE,
    fontSize: 13,
    marginTop: 5
  },
  travelDetailsLocationCaption: {
    color: definitions.colors.MANDY_DARK_GRAY,
    fontSize: 14,
    marginBottom: 15,
    marginTop: 5
  },
  travelDetailsIMLocationCaption: {
    color: definitions.colors.MANDY_GRAY
  },
  travelDetailsColumn: {
    flex: 1,
    paddingLeft: 10
  },
  travelDetailsLocationColumn: {
    flex: 1,
    paddingLeft: 10,
    width: '100%'
  },
  travelDetailsColumnIcon: {
    marginTop: -4,
    minWidth: 22
  },
  travelDetailsColumnTitle: {
    fontSize: 20,
    color: definitions.colors.MANDY_DARK_GRAY
  },
  travelDetailsLocationTitle: {
    fontSize: 14,
    fontWeight: '800',
    color: definitions.colors.MANDY_DARK_GRAY
  },
  travelDetailsIMLocationTitle: {
    color: definitions.colors.MANDY_GRAY
  },
  travelDetailsOpenButton: {
    marginTop: -22,
    alignSelf: 'center',
    position: 'absolute',
    borderWidth: 1,
    borderColor: definitions.colors.MANDY_GRAY
  },
  travelDetailsOuter: {
    position: 'absolute',
    bottom: 0,
    backgroundColor: definitions.colors.WHITE,
    width: '100%',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    paddingTop: 30
  },
  travelDetailsInnerContainer: {
    backgroundColor: definitions.colors.WHITE,
    top: 0,
    position: 'absolute',
    width: '100%'
  },
  travelDetailsInnerContainerSmallDevice: {
    backgroundColor: definitions.colors.WHITE,
    position: 'absolute',
    width: '100%',
    top: 0,
    height: 340
  },
  cancelButtonTextWrapper: {
    position: 'absolute',
    width: '100%',
    bottom: 0
  },
  travelDetailsPaymentBlock: {
    flexDirection: 'column'
  },
  travelDetailsPaymentRow: {
    flexDirection: 'row',
    marginBottom: 15
  },
  travelDetailsFinalPaymentRow: {
    marginTop: 15,
    marginBottom: 0
  },
  travelDetailsPlateNumber: {
    fontSize: 14
  },
  travelDetailsColumnTextBlock: {
    marginTop: 3
  },

  // Vertical dots list
  dotListContainer: {
    marginBottom: 15,
    width: '100%'
  },
  verticalDotsContainer: {
    flexDirection: 'column',
    flex: 1,
    position: 'absolute',
    width: '100%',
    height: '100%',
    top: 9
  },
  verticalDotsStartDot: {
    alignSelf: 'center',
    width: 10,
    height: 10,
    borderRadius: 10,
    backgroundColor: definitions.colors.MANDY_ORANGE
  },
  verticalDotsIntermediateDot: {
    alignSelf: 'center',
    width: 10,
    height: 10,
    borderRadius: 10,
    borderWidth: 2,
    borderColor: definitions.colors.MANDY_ORANGE_OPAQUE
  },
  verticalDotsEndDot: {
    alignSelf: 'center',
    width: 10,
    height: 10,
    borderRadius: 10,
    borderWidth: 2,
    borderColor: definitions.colors.MANDY_ORANGE
  },
  verticalDotsFillDot: {
    alignSelf: 'center',
    width: 3,
    height: 3,
    borderRadius: 3,
    backgroundColor: definitions.colors.MANDY_ORANGE
  },

  // Avatar
  avatar: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 66,
    height: 66,
    borderWidth: 2,
    borderColor: '#ffffff',
    borderStyle: 'solid',
    borderRadius: 66,
    backgroundColor: definitions.colors.MANDY_ORANGE
  },

  // Buttons
  exitButton: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 40,
    height: 40,
    borderWidth: 1,
    paddingLeft: 1,
    borderColor: '#D5D5D5',
    backgroundColor: '#fff',
    borderStyle: 'solid'
  },

  cancelButton: {
    width: 26,
    height: 26,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'center',
    borderWidth: 1,
    borderRadius: 15,
    backgroundColor: definitions.colors.WHITE,
    marginRight: 5,
    borderColor: definitions.colors.MANDY_GRAY
  },
  cancelButtonOuter: {
    height: 55,
    opacity: 0.9,
    backgroundColor: definitions.colors.MANDY_GRAY,
    alignItems: 'center'
  },
  cancelButtonText: {
    fontSize: 14,
    fontWeight: '600',
    color: definitions.colors.MANDY_DARK_GRAY
  },

  // Taxi order block
  taxiOrderText: {
    color: '#C5C3C3'
  }
};

export default StyleSheet.create(styles);
