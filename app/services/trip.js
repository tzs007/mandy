import * as Sentry from "sentry-expo";
import { firestore, db, geoDb } from "../utils/firebase";
import { APIHOST } from "react-native-dotenv";
import axios from "axios";
import moment from "moment";

function getUserStructure(usertype, users) {
  return {
    id: users[usertype].id,
    name: users[usertype].name,
    rating: users[usertype].ratings,
    picture: users[usertype].picture,
    isAtPickupZone: false,
  };
}

function getPlaceStructure(type, trip) {
  placeStructure = {
    address: trip[type].address,
    coordinates: trip[type].coordinates,
  };
  if (type !== "pickUpLocation") {
    placeStructure.duration = trip[type].duration;
  }
  return placeStructure;
}

export const createInitialTrip = async (matchDatas) => {
  const { trip, users } = matchDatas;
  const newTrip = {
    createdAt: firestore.Timestamp.now(),
    type: "joint",
    duration: trip.totalDuration,
    status: "waiting-for-taxi",
    usersFoundEachOther: false,
    initiatorFoundTaxi: false,
    startTime: firestore.Timestamp.fromDate(moment().add(1, "minute").toDate()),
    endTime: null,
    users: {
      initiator: getUserStructure("initiator", users),
      passenger: getUserStructure("passenger", users),
    },
    finances: {
      initiator: {
        fare: trip.dropOffLocation.fare,
      },
      passenger: {
        fare: trip.intermediateStop.fare,
      },
    },
    places: {
      pickUpLocation: getPlaceStructure("pickUpLocation", trip),
      intermediateStop: getPlaceStructure("intermediateStop", trip),
      dropOffLocation: getPlaceStructure("dropOffLocation", trip),
    },
  };

  try {
    const DBtrip = await db.collection("trips").add(newTrip);
    newTrip.id = DBtrip.id;
    return newTrip;
  } catch (error) {
    Sentry.captureException(error, {
      message: "Error occurred during create initial trip.",
    });
  }
};

export const getTrip = async (tripId) => {
  try {
    const tripDoc = await db.collection("trips").doc(tripId).get();
    const trip = tripDoc.data();
    trip.id = tripId;
    return trip;
  } catch (error) {
    Sentry.captureException(error, {
      message: "Error occurred during fetch trip!",
    });
  }
};

export const deleteIntermediateStop = async (deleteTripBody) => {
  try {
    const result = await axios({
      method: "post",
      url: `${APIHOST}/delete-intermediate-stop`,
      data: deleteTripBody,
    });
    const newTrip = result.data;
    newTrip.id = deleteTripBody.tripId;
    return newTrip;
  } catch (error) {
    Sentry.captureException(error, {
      message: "Error occurred during delete intermediate stop!",
    });
  }
};

export const createDocListener = async (docID, callBack) => {
  const doc = await db.collection("trips").doc(docID);
  const detachListener = doc.onSnapshot(
    (docSnapshot) => {
      if (docSnapshot.data()) {
        callBack(docSnapshot.data());
      }
    },
    (error) => {
      Sentry.captureException(error, {
        message: "Error occurred during create trip doc listener!",
      });
    }
  );
  return detachListener;
};

export const updateTrip = (docID, updateableFieldsObject) => {
  try {
    db.collection("trips")
      .doc(docID)
      .set(updateableFieldsObject, { merge: true });
  } catch (error) {
    Sentry.captureException(error, {
      message: "Error occured during update trip doc listener!",
    });
  }
};
