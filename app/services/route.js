import { firestore, geoDb } from "../utils/firebase";
import { _readAsyncStorage, _writeAsyncStorage } from "../utils/helpers";
import * as Sentry from "sentry-expo";

class RouteService {
  createInitialRoute = async ({ pickUpCoords, destinationCoords }) => {
    try {
      const userId = await _readAsyncStorage("user");

      const route = {
        id: userId,
        createdAt: firestore.Timestamp.now(),
        coordinates: new firestore.GeoPoint(pickUpCoords.lat, pickUpCoords.lng),
        destination: new firestore.GeoPoint(
          destinationCoords.lat,
          destinationCoords.lng
        ),
        tripId: null,
      };

      let initialRouteDoc = await geoDb.collection("routes").doc(userId);
      await initialRouteDoc.set(route);
      return userId;
    } catch (error) {
      Sentry.captureException(error, {
        message: "Error occurred during initial route creation!",
      });
    }
  };

  updateRoute = (docID, updateableFieldsObject) => {
    try {
      geoDb
        .collection("routes")
        .doc(docID)
        .set(updateableFieldsObject, { merge: true });
    } catch (error) {
      Sentry.captureException(error, {
        message: "Error occurred during update route!",
      });
    }
  };
}

export default new RouteService();

export const createDocListener = async (docID, callBack) => {
  const doc = await geoDb.collection("routes").doc(docID);
  const detachListener = await doc.onSnapshot(
    (docSnapshot) => {
      if (docSnapshot && docSnapshot.data() && docSnapshot.data().tripId) {
        callBack(docSnapshot.data().tripId);
      }
    },
    (error) => {
      Sentry.captureException(error, {
        message: "Error occurred during create route doc listener!",
      });
    }
  );
  return detachListener;
};

export const deleteRoute = (docID) => {
  try {
    geoDb.collection("routes").doc(docID).delete();
  } catch (error) {
    Sentry.captureException(error, {
      message: "Error occurred during deleted route!",
    });
  }
};

export const setRouteMatched = (docID, tripId) => {
  try {
    geoDb
      .collection("routes")
      .doc(docID)
      .set({ tripId: tripId }, { merge: true });
  } catch (error) {
    Sentry.captureException(error, {
      message: "Error occurred during find match!",
    });
  }
};
