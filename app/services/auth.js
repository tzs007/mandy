import { Platform, Alert } from "react-native";
import * as Facebook from "expo-facebook";
import * as Sentry from "sentry-expo";
import Constants from "expo-constants";
import firebase, { auth, db, facebook } from "../utils/firebase";
import {
  _writeAsyncStorage,
  _clearAsyncStorage,
  _readAsyncStorage,
} from "../utils/helpers";
import UserDocument from "../utils/defaultDocuments";

const FACEBOOK_APP_ID = Constants.manifest.facebookAppId;

class AuthService {
  /*   constructor() {
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        console.log("A felhasználót bejelentkeztettük.");
        return user;
      } else {
        console.log("Nincs elérhető felhasználóval kapcsolatos adat!");
      }
    });
  } */

  signInOrSignUpWithFacebook = async () => {
    try {
      await Facebook.initializeAsync(FACEBOOK_APP_ID);

      const { type, token } = await Facebook.logInWithReadPermissionsAsync({
        permissions: ["public_profile", "email"],
        behavior: Platform.OS === "android" ? "web" : "native",
      });

      if (type === "cancel") {
        Alert.alert(
          "A bejelentkezés sikertelen!",
          "A Facebook-on megszakadt a bejelentkezés folyamata.",
          [
            {
              text: "Rendben",
              style: "cancel",
              onPress: () => console.log("OK Pressed"),
            },
          ]
        );
        return null;
      }

      if (type === "success") {
        try {
          const credential = await facebook.credential(null, token);

          return auth
            .signInWithCredential(credential)
            .then(({ user }) => {
              _writeAsyncStorage("token", token);
              return db
                .collection("users")
                .doc(user.uid)
                .get()
                .then((doc) => {
                  if (!doc.exists) {
                    return db
                      .collection("users")
                      .doc(user.uid) // create new record with login provider id
                      .set({
                        ...UserDocument,
                        id: user.uid,
                        name: user.displayName,
                        picture: user.photoURL,
                        email: user.email,
                      })
                      .then(() => {
                        _writeAsyncStorage("user", user.uid);
                        console.log("A felhasználót létrehoztuk: ", user.uid);
                      });
                  } else {
                    _writeAsyncStorage("user", user.uid);
                  }
                })
                .catch((error) =>
                  Sentry.captureException(error, {
                    message: "Hiba történt a bejelentkezés közben!",
                  })
                );
            })
            .catch((error) =>
              Sentry.captureException(error, {
                message: "Hiba történt a bejelentkezés közben!",
              })
            );
        } catch (error) {
          Sentry.captureException(error, {
            message: error.message,
          });
          alert(message);
        }
      }
    } catch (error) {
      Sentry.captureException(error, {
        message: error.message,
      });
      alert(`Facebook Login Error: ${error.message}`);
    }
  };

  signOut = () => {
    auth
      .signOut()
      .then(() => {
        _clearAsyncStorage();
        // alert('Sikeres kijelentkezés! Viszlát!');
      })
      .catch((error) => {
        Sentry.captureException(error, {
          message: "Sikertelen kijelentkezés!",
        });
        // alert('Sikertelen kijelentkezés!');
      });
  };
}

export default new AuthService();
