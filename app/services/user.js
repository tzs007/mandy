import * as Sentry from "sentry-expo";
import { db } from "../utils/firebase";
import { _readAsyncStorage } from "../utils/helpers";

class UserService {
  createUser = async () => {
    // ide jön az auth serviceből a signInOrSignUpWithFacebook() adatbázis mentése
  };

  getUser = async () => {
    try {
      const userId = await _readAsyncStorage("user");
      const user = await db.collection("users").doc(userId).get();
      return user.data();
    } catch (error) {
      Sentry.captureException(error, {
        message: "Error occurred during fetch user!",
      });
    }
  };

  getUserBy = async (key, value) => {
    try {
      const user = await db.collection("users").where(key, "==", value).get();

      result = [];
      user.forEach((doc) => {
        result.push(doc.data());
      });

      return result;
    } catch (error) {
      Sentry.captureException(error, {
        message: "Error occurred during get user!",
      });
    }
  };

  updateUser = async (userId, object) => {
    try {
      await db.collection("users").doc(userId).update(object);
      return true;
    } catch (error) {
      Sentry.captureException(error, {
        message: "Error occurred during update user!",
      });
    }
  };

  deleteUser = async () => {
    try {
      const userId = await _readAsyncStorage("user");
      await db.collection("users").doc(userId).delete();
      return true;
    } catch (error) {
      Sentry.captureException(error, {
        message: "Error occurred during delete user!",
      });
    }
  };
}

export default new UserService();
