import React, { Component } from "react";
import { AppLoading } from "expo";
import * as Font from "expo-font";
import { Asset } from "expo-asset";
import { StyleProvider } from "native-base";
import getTheme from "./native-base-theme/components";
import mandy from "./native-base-theme/variables/mandy";

import { Provider } from "react-redux";
import reduxStore from "./redux/store";

import Router from "./screens/Router";

import icon from "./assets/icon.png";
import splash from "./assets/splash.png";

import * as Sentry from "sentry-expo";

Sentry.init({
  dsn: "https://a3e5bcdc71e74665a497b6c766df9672@sentry.io/1530375",
  enableInExpoDevelopment: true,
  debug: true,
});

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fontLoaded: false,
      isAppReady: false,
    };
  }

  async componentDidMount() {
    await Font.loadAsync({
      Roboto: require("./assets/fonts/Roboto/Roboto-Regular.ttf"),
      "Roboto-Medium": require("./assets/fonts/Roboto/Roboto-Medium.ttf"),
      "Roboto-Bold": require("./assets/fonts/Roboto/Roboto-Bold.ttf"),
    });
    this.setState({
      fontLoaded: true,
    });
  }

  _cacheResourceAsync = async () => {
    const images = [icon, splash];

    const cacheImages = images.map((image) =>
      Asset.fromModule(image).downloadAsync()
    );

    return Promise.all(cacheImages);
  };

  componentDidCatch(error, info) {
    Sentry.captureException(error, {
      message: "Error occurred  during application loaded!",
    });
  }

  render() {
    const { fontLoaded, isAppReady } = this.state;
    return fontLoaded && isAppReady ? (
      <StyleProvider style={getTheme(mandy)}>
        <Provider store={reduxStore}>
          <Router />
        </Provider>
      </StyleProvider>
    ) : (
      <AppLoading
        startAsync={this._cacheResourceAsync}
        onFinish={() => this.setState({ isAppReady: true })}
        onError={console.warn}
      />
    );
  }
}

export default App;
