import React, { Component } from 'react';
import { BackHandler } from 'react-native';
import { H2 } from 'native-base';

import AppHeader from '../../components/AppHeader';
import DotList from '../../components/DotList';
import PaymentInfo from '../../components/PaymentInfo';
import { hardwareBackHandler } from '../../utils/helpers';

import {
  ContentBox,
  Spacer,
  Separator,
  PullDown,
  BrickButton,
  Caption
} from '../../components/Helpers/index';

class TaxiOrderScreen extends Component {
  static navigationOptions = {
    header: null
  };

  _hardwareBackHandler = () => {
    hardwareBackHandler(this._navigateTo);
    return true;
  };

  componentDidMount = async () => {
    BackHandler.addEventListener(
      'hardwareBackPress',
      this._hardwareBackHandler
    );
  };

  componentWillUnmount = () => {
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this._hardwareBackHandler
    );
  };

  _navigateTo = (screen, params = {}) => {
    this.props.navigation.navigate(screen, params);
  };

  onTaxiOrder = () => {
    console.log('order some taxi');
  };

  render() {
    // sample data
    const startAddress = 'Dayka Gábor utca 1-3, 1016 Budapest';
    const startTime = '14:35';
    const arriveAddress = 'Tétényi út 29, 1011 Budapest';
    const arriveTime = '14:45';
    const fellowShare = 0;
    const paymentTotal = 7500;
    const myShare = paymentTotal - fellowShare;

    return (
      <>
        <AppHeader navigateto="StartScreen" back={true} />

        <ContentBox>
          <H2>Taxi rendelés</H2>
          <Spacer h={12} />
          <Caption>
            A taxid 10 percen belül megérkezik. 
            {'\n'}
            Utastársaknak addig lehetősége van a csatlakozásra.
          </Caption>
          <Spacer h={12} />
        </ContentBox>

        <Spacer h={30} />
        <Separator />
        <Spacer h={30} />

        <ContentBox>
          <DotList
            items={[
              {
                title: startAddress,
                caption: startTime,
                dotType: 'verticalDotsStartDot'
              },
              {
                title: arriveAddress,
                caption: arriveTime,
                dotType: 'verticalDotsEndDot'
              }
            ]}
          />
        </ContentBox>

        <Separator />
        <Spacer h={30} />

        <ContentBox>
          <PaymentInfo
            taxiAmountText="Taxiban fizetendő összeg (max.)"
            taxiAmount={`${paymentTotal} HUF`}
            fellowAmountText={`Utastársad része`}
            fellowAmount={`${fellowShare} HUF`}
            finalAmountText="Összesen (max.)"
            finalAmount={`${myShare} HUF`}
          />
        </ContentBox>

        <PullDown>
          <ContentBox>
            <BrickButton onPress={this.onTaxiOrder}>
              {' '}
              Taxi Rendelés{' '}
            </BrickButton>
            <Spacer h={30} />
          </ContentBox>
        </PullDown>
      </>
    );
  }
}

export default TaxiOrderScreen;
