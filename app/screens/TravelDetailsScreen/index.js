// route planner map
import React, { PureComponent } from 'react';
import { Platform, StyleSheet, View } from 'react-native';
import { Container } from 'native-base';

import { connect } from 'react-redux';
import { getUser } from '../../redux/actions/user';
import { setLocations } from '../../redux/actions/locations';

import Constants from 'expo-constants';
import * as Location from 'expo-location';
import * as Permissions from 'expo-permissions';
import TravelDetails from '../../components/TravelDetails';

import AppHeader from '../../components/AppHeader';
import geo from '../../utils/geo';
import definitions from '../../assets/styles/definitions';
import styleDef from '../../assets/styles/mandy';

import {
  AntDesign,
  Ionicons,
  MaterialCommunityIcons,
} from '@expo/vector-icons';

class TravelDetailsScreen extends PureComponent {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      isMapReady: false,
      errorMessage: '',
      currentLocation: null,
      pickUpAddress: '',
      pickUpCoords: null,
      destinationAddress: '',
      destinationCoords: null,
    };

    if (Platform.OS === 'android' && !Constants.isDevice) {
      this.setState({
        errorMessage:
          'Oops, this will not work on Sketch in an Android emulator. Try it on your device!',
      });
    } else {
      this._getLocationAsync();
    }
  }

  componentDidMount = async () => {
    await this.props.getUser();
  };

  _navigateTo = (screen, params = {}) => {
    this.props.navigation.navigate(screen, params);
  };

  _getLocationAsync = async () => {
    let { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== 'granted') {
      this.setState({
        errorMessage: 'Permission to access location was denied',
      });
      return;
    }

    let currentLocation = await Location.getCurrentPositionAsync({});
    this.setState({
      currentLocation,
    });
  };

  _onMapLayout = () => {
    this.setState({
      isMapReady: true,
    });
  };

  _setPickUpLocation = async () => {
    const {
      pickUpAddress,
      pickUpCoords,
      destinationAddress,
      destinationCoords,
    } = this.state;

    const locations = {
      pickUpLocation: {
        pickUpAddress,
        pickUpCoords,
      },
      destinationLocation: {
        destinationAddress,
        destinationCoords,
      },
    };

    await this.props.setLocations(locations);

    this._navigateTo('SetNewDestinationScreen');
  };

  _getPickUpCoordinates = async region => {
    const { latitude, longitude } = region;
    const pickUpAddress = await geo.get(region);

    await this.setState({
      pickUpCoords: { lat: latitude, lng: longitude },
      pickUpAddress: pickUpAddress[0].formatted_address,
    });
  };

  openProfile = () => {
    this._navigateTo('ProfileScreen');
  };

  onTaxiFound = () => {
    console.log('taxi has been found');
  };

  onCancel = () => {
    console.log('travel has been canceled');
  };

  render() {
    // static data
    const fellowName = 'Ádám';
    const startAddress = 'Dayka Gábor út 12. 1/b, Budapest';
    const intermediateAddress = 'Keleti pályaudvar, Budapest';
    const arriveAddress = 'Móricz Zsigmond körtér 10, Budapest';
    const startTime = '12:55';
    const arriveTime = '13:29';
    const plateNumber = 'ABC-456';
    const paymentTotal = '3.000';
    const fellowShare = '-1.000';
    const myShare = '2.000';
    const remainingTimeText = '3 perc';

    return (
      <View style={{ backgroundColor: definitions.colors.MANDY_GRAY, flex: 1 }}>
        <>
          <AppHeader title="" />
          <Container style={styles.container}>
            <TravelDetails
              header={{
                left: {
                  title: remainingTimeText,
                  caption: `A taxid úton, ${remainingTimeText} múlva megérkezik!`,
                  icon: (
                    <AntDesign
                      name={'clockcircleo'}
                      size={22}
                      color={definitions.colors.MANDY_DARK_GRAY}
                    />
                  ),
                },
                right: {
                  title: `${fellowShare} HUF`,
                  caption: `${fellowName} csatlakozott az utadhoz`,
                  icon: (
                    <MaterialCommunityIcons
                      name={'cash-multiple'}
                      size={22}
                      color={definitions.colors.MANDY_DARK_GRAY}
                    />
                  ),
                },
                // buttonText: 'Megtaláltam a taxit',
                // buttonHandler: this.onTaxiFound
              }}
              plateNumber={plateNumber}
              pathDetails={[
                {
                  title: startAddress,
                  titleStyle: styleDef.travelDetailsIMLocationTitle,
                  caption: `Indulás: ${startTime}`,
                  captionStyle: styleDef.travelDetailsIMLocationCaption,
                  dotCount: 3,
                  icon: (
                    <Ionicons
                      name={'ios-body'}
                      size={22}
                      color={definitions.colors.MANDY_GRAY}
                    />
                  ),
                },
                {
                  title: intermediateAddress,
                  caption: `${fellowName} úticélja`,
                  dotType: 'verticalDotsIntermediateDot',
                },
                {
                  title: arriveAddress,
                  caption: `Érkezés: ${arriveTime}`,
                  dotType: 'verticalDotsEndDot',
                },
              ]}
              paymentInfo={{
                // taxiAmountText: "Taxiban fizetendő összeg (max.)",
                // taxiAmount: `${paymentTotal} HUF`,
                // fellowAmountText: `${fellowName} része`,
                // fellowAmount: `${fellowShare} HUF`,
                finalAmountText: 'Összesen (max.)',
                finalAmount: `${myShare} HUF`,
              }}
              onCancel={this.onCancel}
              cancelText="ÚT LEMONDÁSA"
            />
          </Container>
        </>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    position: 'relative',
    justifyContent: 'space-between',
    flex: 1,
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: 30,
    paddingHorizontal: 20,
    width: '100%',
    height: 100,
  },
  startTitle: {
    marginTop: 5,
  },
  startCatchPhrase: {
    color: '#999',
  },
  mapWrapper: {
    width: '100%',
    flex: 1,
  },
  map: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    zIndex: -1,
    top: 0,
    left: 0,
  },
  profileButton: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 50,
    height: 50,
    borderWidth: 2,
    borderColor: '#ffffff',
    borderStyle: 'solid',
  },
  startButton: {
    justifyContent: 'space-between',
    marginTop: 20,
    borderColor: '#999',
    height: 50,
  },
  startButtonIcon: {
    marginRight: 10,
  },
  footer: {
    paddingVertical: 30,
    paddingHorizontal: 20,
    backgroundColor: '#ffffff',
    width: '100%',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    height: 'auto',
  },
});

const mapStateToProps = ({ user, locations }) => ({
  ...user,
  ...locations,
});

const mapDispatchToProps = {
  getUser,
  setLocations,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(TravelDetailsScreen);
