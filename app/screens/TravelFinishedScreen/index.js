import React, { Component } from 'react';
import { ScrollView } from 'react-native';
import { updateUser } from '../../redux/actions/user';
import { getAverage } from '../../utils/helpers.js';
import { connect } from 'react-redux';

import { H2, Button, Thumbnail } from 'native-base';

import { Ionicons } from '@expo/vector-icons';

import styleDef from '../../assets/styles/mandy';
import definitions from '../../assets/styles/definitions';

import AppHeader from '../../components/AppHeader';
import Rating from '../../components/Rating';
import DotList from '../../components/DotList';
import PaymentInfo from '../../components/PaymentInfo';

import {
  ContentBox,
  Spacer,
  Separator,
  Center,
  Caption,
} from '../../components/Helpers/index';

class TravelFinishedScreen extends Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      userRating: null,
      isRatingFinished: false,
    };

    this.personalTripData = this._getPersonalTripData();
  }

  _getPersonalTripData = () => {
    const trip = this.props.trip;
    const amIPassenger = this.props.user.id === trip.users.passenger.id;
    const didPassengerQuit = trip.type === 'single';
    const otherPassenger = trip.users[amIPassenger ? 'initiator' : 'passenger'];
    const otherPassengerFare = Math.round(
      trip.finances[amIPassenger ? 'initiator' : 'passenger'].fare,
    );
    const myFare = Math.round(
      trip.finances[amIPassenger ? 'passenger' : 'initiator'].fare,
    );

    return {
      amIPassenger,
      pickupAddress: trip.places.pickUpLocation.address,
      intermediateStopAddress: trip.places.intermediateStop.address,
      destinationAddress:
        trip.places[
          amIPassenger || didPassengerQuit
            ? 'intermediateStop'
            : 'dropOffLocation'
        ].address,
      otherPassenger,
      otherPassengerFare,
      myFare,
      didPassengerQuit,
    };
  };

  _navigateTo = (screen, params = {}) => {
    this.props.navigation.navigate(screen, params);
  };

  onRate = ({ stars }) => {
    this.setState({
      userRating: stars,
      isRatingFinished: true,
    });
  };

  onCheck = async () => {
    const { userRating, isRatingFinished } = this.state;
    const { otherPassenger } = this.personalTripData;

    const userId = otherPassenger.id;

    if (isRatingFinished) {
      await this.props.updateUser(userId, {
        ratings: [...otherPassenger.rating, userRating],
      });
    }

    this._navigateTo('StartScreen');
  };

  render() {
    const { isRatingFinished } = this.state;

    const {
      amIPassenger,
      otherPassenger,
      myFare,
      otherPassengerFare,
      pickupAddress,
      intermediateStopAddress,
      destinationAddress,
      didPassengerQuit,
    } = this.personalTripData;

    const userRatings =
      otherPassenger.rating && otherPassenger.rating.length
        ? getAverage(otherPassenger.rating)
        : 5;

    return (
      <>
        <AppHeader navigateto="StartScreen" />

        <ScrollView>
          <ContentBox>
            <H2>Az utazásod véget ért.</H2>
            <Spacer h={12} />
            <Caption>
              Köszönjük, hogy a Mandyt választottad. Kérjük értékeld
              utastársadat.
            </Caption>
            <Spacer h={30} />
          </ContentBox>

          <ContentBox cells>
            <Thumbnail source={{ uri: otherPassenger.picture }} />
            <Spacer w={20} />
            <Rating size={30} onRate={this.onRate} />
          </ContentBox>

          <Spacer h={30} />
          <Separator />
          <Spacer h={30} />

          <ContentBox>
            <DotList
              items={
                amIPassenger || didPassengerQuit
                  ? [
                      {
                        title: pickupAddress,
                        dotType: 'verticalDotsStartDot',
                        dotCount: 2,
                        titleStyle: { marginBottom: 22 },
                      },
                      {
                        title: destinationAddress,
                        dotType: 'verticalDotsEndDot',
                      },
                    ]
                  : [
                      {
                        title: pickupAddress,
                        dotType: 'verticalDotsStartDot',
                        dotCount: 2,
                        titleStyle: { marginBottom: 22 },
                      },
                      {
                        title: intermediateStopAddress,
                        titleStyle: {
                          ...styleDef.travelDetailsIMLocationTitle,
                          marginBottom: 22,
                        },
                        dotType: 'verticalDotsIntermediateDot',
                        dotCount: 2,
                      },
                      {
                        title: destinationAddress,
                        dotType: 'verticalDotsEndDot',
                      },
                    ]
              }
            />
          </ContentBox>

          <Spacer h={15} />
          <Separator />
          <Spacer h={30} />

          <ContentBox>
            {amIPassenger || didPassengerQuit ? (
              <PaymentInfo
                finalAmountText="Összesen (max.)"
                finalAmount={`${myFare} HUF`}
              />
            ) : (
              <PaymentInfo
                taxiAmountText="Taxiban fizetett összeg (max.)"
                taxiAmount={`${myFare + otherPassengerFare} HUF`}
                fellowAmountText={`${otherPassenger.name} része (átutalva)`}
                fellowAmount={`${otherPassengerFare} HUF`}
                finalAmountText="Összesen (max.)"
                finalAmount={`${myFare} HUF`}
              />
            )}

            <Spacer h={30} />

            <Center>
              <Button
                small
                primary
                bordered
                rounded
                noShadow
                onPress={this.onCheck}
                style={styleDef.exitButton}
              >
                <Ionicons
                  name={'md-checkmark'}
                  size={25}
                  color={
                    isRatingFinished
                      ? definitions.colors.MANDY_DARK_GRAY
                      : definitions.colors.MANDY_GRAY
                  }
                />
              </Button>
            </Center>
          </ContentBox>
        </ScrollView>
      </>
    );
  }
}

const mapStateToProps = ({ user, trip }) => ({
  ...user,
  ...trip,
});

const mapDispatchToProps = {
  updateUser,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(TravelFinishedScreen);
