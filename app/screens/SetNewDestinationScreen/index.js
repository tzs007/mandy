import React, { Component } from "react";
import { connect } from "react-redux";
import { setLocations } from "../../redux/actions/locations";
import { setInitialTrip } from "../../redux/actions/trip";
import { _isMatchingRoute } from "../../utils/helpers";

import {
  Container,
  Button,
  Form,
  Item,
  Input,
  Text,
  Spinner,
} from "native-base";
import { StyleSheet, View, FlatList, KeyboardAvoidingView } from "react-native";
import { Ionicons, MaterialIcons, Entypo } from "@expo/vector-icons";
import geo from "../../utils/geo";

import AppHeader from "../../components/AppHeader";
import { setRouteMatched } from "../../services/route";

class SetNewDestinationScreen extends Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);
    this.state = {
      typeOfLocation: "destinationAddress",
      addressList: null,
      pickUpAddress: "",
      pickUpCoords: null,
      destinationAddress: "",
      destinationCoords: null,
      searchingMatchingRoute: false,
    };
  }

  componentDidMount = () => {
    this.focusListener = this.props.navigation.addListener("didFocus", () => {
      this._screenDidFocus();
    });
  };

  componentWillUnmount() {
    this.focusListener.remove();
  }

  _screenDidFocus = async () => {
    const { pickUpLocation, destinationLocation } = this.props.locations;

    await this.setState({
      ...pickUpLocation,
      ...destinationLocation,
    });
  };

  _navigateTo = async (screen, params = {}) => {
    this.props.navigation.navigate(screen, params);
  };

  _handleMatchOrRouteOffer = async () => {
    this.setState({
      searchingMatchingRoute: true,
    });

    const { pickUpCoords, destinationCoords } = this.state;
    const { user } = this.props;

    const trip = await _isMatchingRoute(
      user.id,
      pickUpCoords,
      destinationCoords
    );

    if (trip) {
      const tripId = await this.props.setInitialTrip(trip);
      setRouteMatched(trip.matchedRouteId, tripId);
      this.setState({
        searchingMatchingRoute: false,
      });
      this._navigateTo("RouteMatchScreen");
    } else {
      this.setState({
        searchingMatchingRoute: false,
      });
      this._navigateTo("RouteOfferScreen");
    }
  };

  _setRouteLocationByOwnAddress = async ({ address, coordinates }) => {
    const { typeOfLocation } = this.state;
    const { setLocations } = this.props;

    if (typeOfLocation === "pickUpAddress") {
      await this.setState({
        pickUpAddress: address,
        pickUpCoords: coordinates,
      });
    } else {
      await this.setState({
        destinationAddress: address,
        destinationCoords: coordinates,
      });
    }

    const {
      destinationAddress,
      destinationCoords,
      pickUpAddress,
      pickUpCoords,
    } = this.state;

    const locations = {
      destinationLocation: {
        destinationAddress,
        destinationCoords,
      },
      pickUpLocation: {
        pickUpAddress,
        pickUpCoords,
      },
    };

    await setLocations(locations);

    if (
      typeOfLocation === "destinationAddress" &&
      pickUpAddress !== "" &&
      destinationAddress !== ""
    ) {
      this._handleMatchOrRouteOffer();
    }
  };

  _setRouteLocation = async ({ formatted_address, geometry: { location } }) => {
    const { typeOfLocation } = this.state;
    const { setLocations } = this.props;

    if (typeOfLocation === "pickUpAddress") {
      await this.setState({
        pickUpAddress: formatted_address,
        pickUpCoords: location,
      });
    } else {
      await this.setState({
        destinationAddress: formatted_address,
        destinationCoords: location,
      });
    }

    const {
      destinationAddress,
      destinationCoords,
      pickUpAddress,
      pickUpCoords,
    } = this.state;

    const locations = {
      destinationLocation: {
        destinationAddress,
        destinationCoords,
      },
      pickUpLocation: {
        pickUpAddress,
        pickUpCoords,
      },
    };

    await setLocations(locations);
    if (
      typeOfLocation === "destinationAddress" &&
      pickUpAddress !== "" &&
      destinationAddress !== ""
    ) {
      this._handleMatchOrRouteOffer();
    }
  };

  _pickRouteLocation = async () => {
    const { typeOfLocation } = this.state;
    this._navigateTo("PickRouteLocationScreen", {
      typeOfLocation,
    });
  };

  _emptyAddress = (typeOfLocation) => {
    this.setState({
      [typeOfLocation]: "",
    });
  };

  _onChangeText = async (userInput) => {
    const { typeOfLocation } = this.state;
    this.setState({
      [typeOfLocation]: userInput,
    });

    let addressList = null;
    try {
      if (userInput.length >= 3) {
        addressList = await geo.get(userInput);
      }

      this.setState({
        addressList,
      });
    } catch (error) {
      console.log(error);
    }
  };

  _onFocusTextInput = (typeOfLocation) => {
    this.setState({
      typeOfLocation,
    });
  };

  _keyExtractor = (item, index) => `list-item-${index}`;

  _renderAddressListItem = (item) => {
    return (
      <View style={styles.listItem}>
        <View style={styles.listItemLeft}>
          <Ionicons
            name={item.name === "home" ? "md-home" : "ios-briefcase"}
            size={20}
            color="#ccc"
          />
        </View>
        <View style={styles.listItemBody}>
          <Button
            block
            transparent
            onPress={
              item.address
                ? () => this._setRouteLocationByOwnAddress(item)
                : () =>
                    this._navigateTo("EditAddressScreen", {
                      key: item.name,
                    })
            }
            style={styles.listItemBodyButton}
          >
            <Text style={styles.listItemBodyButtonText}>{item.label}</Text>
            <Text note numberOfLines={1} style={styles.listItemNoteText}>
              {item.address === "" ? "Kérjük add meg a címet" : item.address}
            </Text>
          </Button>
        </View>
        <View style={styles.listItemRight}>
          <Button
            transparent
            block
            onPress={() =>
              this._navigateTo("EditAddressScreen", {
                key: item.name,
              })
            }
          >
            <MaterialIcons name="edit" size={20} color="#ccc" />
          </Button>
        </View>
      </View>
    );
  };

  _renderLocationListItem = (item, index) => {
    return (
      <View style={styles.listItem} key={item.place_id}>
        <View style={styles.listItemLeft}>
          <Ionicons name="ios-pin" size={24} color="#ccc" />
        </View>
        <View style={styles.listItemBody}>
          <Button
            block
            dark
            transparent
            onPress={() => this._setRouteLocation(item)}
            style={styles.listItemBodyButton}
          >
            {item.address_components.map(
              (component, index) =>
                component.types.includes("locality") && (
                  <Text
                    style={styles.listItemBodyButtonText}
                    key={`address-title-${index}`}
                  >
                    {component.long_name}
                  </Text>
                )
            )}

            <Text note numberOfLines={1} style={styles.listItemNoteText}>
              {item.formatted_address}
            </Text>
          </Button>
        </View>
      </View>
    );
  };

  render() {
    const {
      typeOfLocation,
      addressList,
      pickUpAddress,
      destinationAddress,
    } = this.state;

    const {
      user,
      navigation: { getParam },
    } = this.props;

    const home =
      user && user.addresses.length
        ? user.addresses.filter((value) => value.name === "home")
        : null;

    const office =
      user && user.addresses.length
        ? user.addresses.filter((value) => value.name === "office")
        : null;

    return (
      <KeyboardAvoidingView behavior="padding" enabled style={{ flex: 1 }}>
        <Container>
          <AppHeader title="Úticél megadása" />
          <Form style={styles.form}>
            <Item
              regular
              style={[
                styles.formItem,
                {
                  color:
                    typeOfLocation === "pickUpAddress" ? "#413b38" : "#ccc",
                  backgroundColor:
                    typeOfLocation === "pickUpAddress" ? "#fff" : "#efefef",
                },
              ]}
            >
              <Input
                placeholder="Mi az indulás helye?"
                value={pickUpAddress}
                onChangeText={this._onChangeText}
                multiLine={false}
                textAlignVertical="center"
                onFocus={() => this._onFocusTextInput("pickUpAddress")}
              />
              {pickUpAddress ? (
                <Ionicons
                  name="md-close-circle"
                  size={25}
                  color="#413B38"
                  onPress={() => this._emptyAddress("pickUpAddress")}
                  style={{
                    paddingRight: 15,
                    lineHeight: 50,
                    height: 50,
                    backgroundColor:
                      typeOfLocation === "pickUpAddress" ? "#fff" : "#efefef",
                  }}
                />
              ) : null}
            </Item>
            <Item
              regular
              style={[
                styles.formItem,
                {
                  color:
                    typeOfLocation === "destinationAddress"
                      ? "#413b38"
                      : "#ccc",
                  backgroundColor:
                    typeOfLocation === "destinationAddress"
                      ? "#fff"
                      : "#efefef",
                },
              ]}
            >
              <Input
                placeholder="Mi az úticélod?"
                value={destinationAddress}
                onChangeText={this._onChangeText}
                multiLine={false}
                textAlignVertical="center"
                onFocus={() => this._onFocusTextInput("destinationAddress")}
              />
              {destinationAddress ? (
                <Ionicons
                  name="md-close-circle"
                  size={25}
                  color="#413B38"
                  onPress={() => this._emptyAddress("destinationAddress")}
                  style={{
                    paddingRight: 15,
                    height: 50,
                    lineHeight: 50,
                    backgroundColor:
                      typeOfLocation === "destinationAddress"
                        ? "#fff"
                        : "#efefef",
                  }}
                />
              ) : null}
            </Item>
          </Form>
          <View style={styles.listBox}>
            {pickUpAddress === "" || destinationAddress === "" ? (
              <FlatList
                data={[...office, ...home]}
                renderItem={({ item }) => this._renderAddressListItem(item)}
                keyExtractor={this._keyExtractor}
                keyboardDismissMode="on-drag"
                keyboardShouldPersistTaps="always"
              />
            ) : null}
            {/* populated list by Google Maps */}
            {addressList && addressList.length > 0 && (
              <FlatList
                data={addressList}
                renderItem={({ item }) => this._renderLocationListItem(item)}
                keyExtractor={this._keyExtractor}
                keyboardDismissMode="on-drag"
                keyboardShouldPersistTaps="always"
              />
            )}
            <View style={styles.listItem}>
              <View style={styles.listItemLeft}>
                <Entypo
                  name="location-pin"
                  size={12}
                  color={
                    typeOfLocation === "pickUpAddress" ? "#413b38" : "#ccc"
                  }
                />
                <Entypo name="dots-two-vertical" size={12} color="#ccc" />
                <Entypo
                  name="location"
                  size={12}
                  color={
                    typeOfLocation === "destinationAddress" ? "#413b38" : "#ccc"
                  }
                />
              </View>
              <View style={styles.listItemBody}>
                <Button
                  block
                  dark
                  transparent
                  onPress={this._pickRouteLocation}
                >
                  <Text style={styles.listItemPickAddressButtonText}>
                    Válaszd ki a térképen
                  </Text>
                </Button>
              </View>
            </View>
            <View style={styles.listItem}>
              <View style={styles.listItemBody}>
                {getParam("from") === "PickRouteLocationScreen" &&
                  pickUpAddress !== "" &&
                  destinationAddress !== "" && (
                    <Button
                      block
                      primary
                      style={styles.confirmButton}
                      onPress={this._handleMatchOrRouteOffer}
                    >
                      <Text>Megerősítés</Text>
                    </Button>
                  )}
              </View>
            </View>
          </View>
          {this.state.searchingMatchingRoute && (
            <View style={styles.loading}>
              <Spinner color="#f37121" />
            </View>
          )}
        </Container>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  form: {
    paddingHorizontal: 20,
  },
  formItem: {
    marginBottom: 10,
  },
  formInput: {
    textAlign: "left",
    justifyContent: "center",
  },
  routeButton: {
    flexDirection: "column",
    justifyContent: "center",
    width: 50,
    height: 50,
  },
  routeButtonIcon: {},
  savedLocationButtonText: {},
  savedLocation: {
    fontSize: 14,
    opacity: 0.3,
  },
  listBox: {
    borderTopWidth: 1,
    borderTopColor: "#F3F3F3",
    borderStyle: "solid",
    marginTop: 10,
  },
  listItem: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginHorizontal: 0,
    paddingLeft: 20,
    paddingRight: 10,
    paddingVertical: 10,
    borderStyle: "solid",
    borderBottomWidth: 1,
    borderBottomColor: "#F3F3F3",
  },
  listItemLeft: {
    width: 30,
    alignItems: "flex-start",
    justifyContent: "center",
  },
  listItemBody: {
    flex: 3,
    justifyContent: "flex-start",
  },
  listItemBodyButton: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "space-between",
  },
  listItemBodyButtonText: { width: "100%", textAlign: "left", color: "#000" },
  listItemNoteText: {
    width: "100%",
    textAlign: "left",
  },
  listItemPickAddressButtonText: {
    display: "flex",
    width: "100%",
    textAlign: "left",
    justifyContent: "flex-start",
    flex: 1,
    color: "#413B38",
  },
  listItemRight: {
    width: 40,
    justifyContent: "center",
  },
  loading: {
    position: "absolute",
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: "center",
    justifyContent: "center",
  },
  confirmButton: {
    justifyContent: "center",
    borderColor: "#999",
    height: 50,
  },
});

const mapDispatchToProps = {
  setLocations,
  setInitialTrip,
};

const mapStateToProps = ({ user, locations }) => ({
  ...user,
  ...locations,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SetNewDestinationScreen);
