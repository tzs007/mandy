import React, { PureComponent } from 'react';
import { Alert, Modal } from 'react-native';

import AppHeader from '../../components/AppHeader';
import { Container } from 'native-base';

import {
  ContentBox, Spacer, PullDown, BrickButton
} from '../../components/Helpers/index';

class DidFellowArriveScreen extends PureComponent {

  state = {
    isModalVisible: false
  }

  static navigationOptions = {
    header: null
  };

  onNoPress = async () => {
    await this.setState({
      isModalVisible: false
    });
    console.log('no');
  }

  onYesPress = async () => {
    await this.setState({
      isModalVisible: false
    });
    console.log('yes');
  }

  onOpenAlert = async () => {
    await this.setState({
      isModalVisible: true
    });
    Alert.alert(
      'Megérkezett az utastársad?',
      'Amennyiben nem érkezett meg, kérjük kezdd meg az utazást egyedül.',
      [
        {
          text: 'Nem',
          onPress: this.onNoPress
        },
        {
          text: 'Igen',
          onPress: this.onYesPress
        }
      ],
      { cancelable: true }
    );
  }

  render() {
    const { isModalVisible } = this.state;
    return (
      <>
        <AppHeader title="" />
        <Container>
          <Modal
            animationType="fade"
            transparent={false}
            visible={isModalVisible}
            presentationStyle={"pageSheet"}
            onRequestClose={() => {
              Alert.alert('Modal has been closed.');
            }}>
          </Modal>
          <PullDown>
            <ContentBox>
              <BrickButton onPress={this.onOpenAlert}> Open alert </BrickButton>
              <Spacer h={30} />
            </ContentBox>
          </PullDown>
        </Container>
      </>
    );
  }
}

export default DidFellowArriveScreen;
