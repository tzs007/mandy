import React, { PureComponent } from "react";
import { Platform, StyleSheet, View, Alert, BackHandler } from "react-native";
import { Container, Button, Spinner } from "native-base";
import MapView, { Marker } from "react-native-maps";
import MapViewDirections from "react-native-maps-directions";
import { firestore } from "../../utils/firebase";
import { AntDesign, Ionicons } from "@expo/vector-icons";

import { connect } from "react-redux";
import { setExistingTrip } from "../../redux/actions/trip";

import Constants from "expo-constants";
import * as Location from "expo-location";
import * as Permissions from "expo-permissions";
import mapStyle from "../../utils/mapStyle";
import DirectionMarker from "../../components/DirectionMarker";
import TravelDetails from "../../components/TravelDetails";
import {
  createDocListener,
  updateTrip,
  deleteIntermediateStop,
} from "../../services/trip";
import definitions from "../../assets/styles/definitions";
import styleDef from "../../assets/styles/mandy";
import moment from "moment";

import { GOOGLE_MAP_APIKEY } from "../../utils/constants";
import Geo from "../../utils/geo";
import { hardwareBackHandler } from "../../utils/helpers";

class WalkingRoutePlanScreen extends PureComponent {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      isMapReady: false,
      errorMessage: "",
      currentLocation: null,
      pickUpAddress: "",
      pickUpCoords: null,
      destinationAddress: "",
      destinationCoords: null,
      shouldMapViewBe: true,
      distanceFromPickup: "",
      minutesUntilStart: "",
      passengerDidQuit: false,
    };

    if (Platform.OS === "android" && !Constants.isDevice) {
      this.setState({
        errorMessage:
          "Oops, this will not work on Sketch in an Android emulator. Try it on your device!",
      });
    } else {
      this._getLocationAsync();
    }

    this.personalTripData = this._getPersonalTripData();
  }

  _getPersonalTripData = () => {
    const trip = this.props.trip;
    const amIPassenger = this.props.user.id === trip.users.passenger.id;
    const intermediateStop = amIPassenger
      ? undefined
      : this._formatCoordinates(trip.places.intermediateStop.coordinates);
    const otherPassenger = trip.users[amIPassenger ? "initiator" : "passenger"];
    const otherPassengerFare = Math.round(
      trip.finances[amIPassenger ? "initiator" : "passenger"].fare
    );
    const myFare = Math.round(
      trip.finances[amIPassenger ? "passenger" : "initiator"].fare
    );
    const startTime = trip.startTime
      ? moment(trip.startTime.toDate()).format("HH:mm")
      : moment(firestore.Timestamp.now().toDate()).format("HH:mm");

    const endTime = amIPassenger
      ? moment(trip.startTime.toDate())
          .add(trip.places.intermediateStop.duration, "seconds")
          .format("HH:mm")
      : moment(trip.startTime.toDate())
          .add(trip.duration, "seconds")
          .format("HH:mm");

    return {
      amIPassenger,
      pickUpLocation: this._formatCoordinates(
        trip.places.pickUpLocation.coordinates
      ),
      pickupAddress: trip.places.pickUpLocation.address,
      intermediateStop,
      intermediateStopAddress: intermediateStop
        ? trip.places.intermediateStop.address
        : undefined,
      destination: this._formatCoordinates(
        trip.places[intermediateStop ? "dropOffLocation" : "intermediateStop"]
          .coordinates
      ),
      destinationAddress:
        trip.places[intermediateStop ? "dropOffLocation" : "intermediateStop"]
          .address,
      otherPassenger,
      otherPassengerFare,
      myFare,
      startTime,
      endTime,
    };
  };

  _hardwareBackHandler = () => {
    hardwareBackHandler(this._navigateTo);
    return true;
  };

  componentDidMount = async () => {
    this.willFocusListener = this.props.navigation.addListener(
      "willFocus",
      () => {
        this.setState({ shouldMapViewBe: true });
      }
    );

    this.blurListener = this.props.navigation.addListener("didBlur", () => {
      this.setState({ shouldMapViewBe: false });
    });

    BackHandler.addEventListener(
      "hardwareBackPress",
      this._hardwareBackHandler
    );
  };

  componentWillUnmount = () => {
    this.willFocusListener.remove();
    this.blurListener.remove();
    this._detachLocationWatcher();
    this._detachTripListener();
    BackHandler.removeEventListener(
      "hardwareBackPress",
      this._hardwareBackHandler
    );
  };

  _convertCoordsToArray = (coords) => {
    return [coords.latitude, coords.longitude];
  };

  didFellowFindYouAlert = async () => {
    const { amIPassenger } = this.personalTripData;
    const main = amIPassenger
      ? "Megtaláltad a hirdetőt?"
      : "Megérkezett az útitársad?";
    const sub = amIPassenger
      ? "Amennyiben megtaláltad a hirdetőt, megkezdhetitek a közös utazást."
      : "Amennyiben nem érkezett meg, kérjük kezdd meg az utazást egyedül.";

    Alert.alert(
      main,
      sub,
      [
        {
          text: "Nem",
          onPress: async () => {
            if (!this.usersFoundEachOther) {
              this.iDidNotFindIt = true;
              const deleteIntermediateStopParams = {
                tripId: this.props.trip.id,
                pickUpLocation: this._convertCoordsToArray(
                  this.personalTripData.pickUpLocation
                ),
                destination: this._convertCoordsToArray(
                  this.personalTripData.destination
                ),
              };
              await deleteIntermediateStop(deleteIntermediateStopParams);
            }
          },
        },
        {
          text: "Igen",
          onPress: async () => {
            if (!this.usersFoundEachOther && !this.initiatorDidNotFoundMe) {
              updateTrip(this.props.trip.id, { usersFoundEachOther: true });
            }
          },
        },
      ],
      { cancelable: false }
    );
  };

  onCancel = async () => {
    Alert.alert(
      "Biztosan lemondod az utat?",
      "Amennyiben igen, a kezdőképernyőre jutsz.",
      [
        {
          text: "Nem",
          onPress: undefined,
        },
        {
          text: "Igen",
          onPress: async () => {
            const deleteIntermediateStopParams = {
              tripId: this.props.trip.id,
              pickUpLocation: this._convertCoordsToArray(
                this.personalTripData.pickUpLocation
              ),
              destination: this._convertCoordsToArray(
                this.personalTripData.destination
              ),
            };
            await this._detachTripListener();
            deleteIntermediateStop(deleteIntermediateStopParams);
            this._navigateTo("StartScreen");
          },
        },
      ],
      { cancelable: true }
    );
  };

  _navigateTo = (screen, params = {}) => {
    if (this.taxiArriveTimeExpired) clearTimeout(this.taxiArriveTimeExpired);
    this._detachTripListener();
    this._detachLocationWatcher();
    this.props.navigation.navigate(screen, params);
  };

  _getLocationAsync = async () => {
    let { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== "granted") {
      this.setState({
        errorMessage: "Permission to access location was denied",
      });
      return;
    }

    let currentLocation = await Location.getCurrentPositionAsync({});
    this.setState({
      currentLocation: currentLocation.coords,
    });
  };

  _detachLocationWatcher = () => {
    if (this.detachLocationWatcher) {
      this.detachLocationWatcher();
      this.detachLocationWatcher = undefined;
    }
  };

  _distanceFromPickup = async (currentLocation) => {
    const distanceFromPickup = await Geo.getDistance({
      origin: currentLocation,
      destination: this.personalTripData.pickUpLocation,
      mode: "walking",
    });
    return distanceFromPickup;
  };

  _taxiArrived = () => {
    this.didFellowFindYouAlert();
  };

  _amIInPickupZoneBasedOnDB = (trip) =>
    trip.users[this.personalTripData.amIPassenger ? "passenger" : "initiator"]
      .isAtPickupZone;

  _isOtherUserInPickupZone = (trip) =>
    trip.users[this.personalTripData.amIPassenger ? "initiator" : "passenger"]
      .isAtPickupZone;

  _tripListener = async (trip) => {
    if (!this.taxiArriveTimeExpired) {
      //this will be true only for the tripListener init
      this.taxiArriveTimeExpired = setTimeout(
        (trip) => this._taxiArrived(),
        trip.startTime.toMillis() - Date.now()
      );
    }

    if (
      (!trip.usersFoundEachOther &&
        this._amIInPickupZoneBasedOnDB(trip) &&
        this._isOtherUserInPickupZone(trip)) ||
      (this.personalTripData.amIPassenger &&
        trip.initiatorFoundTaxi &&
        !this.iDidNotFindIt)
    ) {
      this.didFellowFindYouAlert();
    }

    if (!(trip.type === "single") && trip.usersFoundEachOther) {
      this.usersFoundEachOther = true;
      this._navigateTo("OngoingRoutePlanScreen");
    }

    if (trip.type === "single") {
      if (this.personalTripData.amIPassenger) {
        this.initiatorDidNotFoundMe = true;
        this._navigateTo("FellowDidNotFindYouScreen", {
          iDidNotFindIt: this.iDidNotFindIt ? true : false,
        });
      } else {
        if (this.props.trip.type !== "single") {
          await this.props.setExistingTrip(this.props.trip.id);
          this.setState({ passengerDidQuit: true });
        }
        if (!this.iDidNotFindIt) {
          Alert.alert(
            "Utastársad lemondta az utat, sajnáljuk.",
            "Kérjük folytasd az utat egyedül."
          );
        }

        if (this._amIInPickupZoneBasedOnDB(trip) || this.taxiFound) {
          this._navigateTo("OngoingRoutePlanScreen");
        } else {
          this.personalTripData.myFare = trip.finances.initiator.fare;
        }
      }
    }
  };

  _calcMinutesUntilStart = () => {
    return Math.round(
      (this.props.trip.startTime.seconds - Date.now() / 1000) / 60
    );
  };

  _locationWatcher = async (loc) => {
    if (!this.personalTripData.firstAddress) {
      const currentAddress = await Geo.get(loc.coords);
      this.personalTripData.firstAddress = currentAddress[0].formatted_address;
    }
    let { minutesUntilStart } = this.state;
    if (minutesUntilStart > 0 || minutesUntilStart === "") {
      minutesUntilStart = this._calcMinutesUntilStart();
    }
    const distanceFromPickup = await this._distanceFromPickup(loc.coords);

    this.setState({
      currentLocation: loc.coords,
      distanceFromPickup: distanceFromPickup.distance,
      minutesUntilStart,
    });

    if (!this.amIInpickup && distanceFromPickup.distance < 20) {
      updateTrip(this.props.trip.id, {
        users: {
          [this.personalTripData.amIPassenger ? "passenger" : "initiator"]: {
            isAtPickupZone: true,
          },
        },
      });
      this.amIInpickup = true;
    }
  };

  _onMapLayout = async () => {
    this.setState({
      isMapReady: true,
    });
    const detachLocationWatcher = await Location.watchPositionAsync(
      {
        accuracy: Location.Accuracy.High,
        timeInterval: 5000,
        distanceInterval: 0,
      },
      this._locationWatcher
    );
    this.detachLocationWatcher = detachLocationWatcher.remove;
    this.detachTripListenerParam = await createDocListener(
      this.props.trip.id,
      this._tripListener
    );
  };

  onTaxiFound = () => {
    Alert.alert(
      "Biztosan megtaláltad a taxit?",
      "",
      [
        {
          text: "Nem",
          onPress: undefined,
        },
        {
          text: "Igen",
          onPress: () => {
            if (this.state.passengerDidQuit) {
              this._navigateTo("OngoingRoutePlanScreen");
            } else {
              this.taxiFound = true;
              updateTrip(this.props.trip.id, { initiatorFoundTaxi: true });
              if (!this.usersFoundEachOther) {
                this.didFellowFindYouAlert();
              }
            }
          },
        },
      ],
      { cancelable: true }
    );
  };

  _detachTripListener = async () => {
    if (this.detachTripListenerParam) {
      await this.detachTripListenerParam();
      this.detachTripListenerParam = undefined;
    }
  };

  _formatCoordinates = (coordinates) => ({
    latitude: coordinates.lat,
    longitude: coordinates.lng,
  });

  render() {
    const { isMapReady, passengerDidQuit } = this.state;
    const currentLocation = this.state.currentLocation;

    const {
      amIPassenger,
      pickUpLocation,
      intermediateStop,
      destination,
      otherPassenger,
      myFare,
      otherPassengerFare,
      startTime,
      endTime,
      pickupAddress,
      intermediateStopAddress,
      destinationAddress,
      firstAddress,
    } = this.personalTripData;

    const plateNumber = "ABC-456";

    return (
      <Container style={styles.container}>
        {this.state.shouldMapViewBe ? (
          <>
            <View style={styles.mapWrapper}>
              {currentLocation && typeof currentLocation !== undefined ? (
                <>
                  <MapView
                    provider="google"
                    style={styles.map}
                    customMapStyle={mapStyle}
                    initialRegion={{
                      latitude: currentLocation.latitude,
                      longitude: currentLocation.longitude,
                      latitudeDelta: 0.02,
                      longitudeDelta: 0.02,
                    }}
                    onMapReady={this._onMapLayout}
                    onRegionChangeComplete={this._getPickUpCoordinates}
                  >
                    {isMapReady && (
                      <>
                        <Marker coordinate={currentLocation}>
                          <DirectionMarker type="currentPosition" />
                        </Marker>
                        <Marker coordinate={pickUpLocation}>
                          <DirectionMarker type="pickUpLocation" />
                        </Marker>
                        {intermediateStop ? (
                          <Marker coordinate={intermediateStop}>
                            <DirectionMarker type="intermediateStop" />
                          </Marker>
                        ) : (
                          <></>
                        )}
                        <Marker coordinate={destination}>
                          <DirectionMarker type="destination" />
                        </Marker>
                        <MapViewDirections
                          origin={currentLocation}
                          destination={pickUpLocation}
                          apikey={GOOGLE_MAP_APIKEY}
                          strokeWidth={6}
                          mode="WALKING"
                          lineCap="square"
                          lineDashPattern={[10, 10]}
                          strokeColor="#f37121"
                        />
                        {intermediateStop ? (
                          <>
                            <MapViewDirections
                              origin={pickUpLocation}
                              destination={intermediateStop}
                              apikey={GOOGLE_MAP_APIKEY}
                              strokeWidth={6}
                              mode="DRIVING"
                              strokeDasharray="3,3"
                              strokeColor="#f37121"
                            />
                            <MapViewDirections
                              origin={intermediateStop}
                              destination={destination}
                              apikey={GOOGLE_MAP_APIKEY}
                              strokeWidth={6}
                              mode="DRIVING"
                              strokeDasharray="3,3"
                              strokeColor="#f37121"
                            />
                          </>
                        ) : (
                          <MapViewDirections
                            origin={pickUpLocation}
                            destination={destination}
                            apikey={GOOGLE_MAP_APIKEY}
                            strokeWidth={6}
                            mode="DRIVING"
                            strokeDasharray="3,3"
                            strokeColor="#f37121"
                          />
                        )}
                      </>
                    )}
                  </MapView>
                  <TravelDetails
                    header={{
                      left: {
                        title: `${this.state.minutesUntilStart} perc`,
                        caption: `Nemsokára indul a taxi.`,
                        icon: (
                          <AntDesign
                            name={"clockcircleo"}
                            size={22}
                            color={definitions.colors.MANDY_DARK_GRAY}
                          />
                        ),
                      },
                      right: {
                        title: `${this.state.distanceFromPickup} m`,
                        caption: "Sétálj a kijelölt indulási pontra",
                        icon: (
                          <Ionicons
                            name={"md-walk"}
                            size={22}
                            color={definitions.colors.MANDY_DARK_GRAY}
                          />
                        ),
                      },
                      buttonText: amIPassenger
                        ? undefined
                        : "Megtaláltam a taxit",
                      buttonHandler: amIPassenger
                        ? undefined
                        : this.onTaxiFound,
                    }}
                    plateNumber={plateNumber}
                    pathDetails={[
                      {
                        title: firstAddress,
                        titleStyle: styleDef.travelDetailsIMLocationTitle,
                        caption: `Indulás:  ${startTime}`,
                        captionStyle: styleDef.travelDetailsIMLocationCaption,
                        dotCount: 3,
                        icon: (
                          <Ionicons
                            name={"md-walk"}
                            size={22}
                            color={definitions.colors.MANDY_GRAY}
                          />
                        ),
                      },
                      {
                        title: pickupAddress,
                        caption: `Indulás:  ${startTime}`,
                        dotType: "verticalDotsIntermediateDot",
                      },
                      ...(amIPassenger
                        ? [
                            {
                              title: destinationAddress,
                              caption: `Érkezés:  ${endTime}`,
                              dotType: "verticalDotsEndDot",
                            },
                          ]
                        : passengerDidQuit
                        ? [
                            {
                              title: destinationAddress,
                              caption: `Érkezés: ${endTime}`,
                              dotType: "verticalDotsEndDot",
                            },
                          ]
                        : [
                            {
                              title: intermediateStopAddress,
                              titleStyle: styleDef.travelDetailsIMLocationTitle,
                              caption: `${otherPassenger.name} úticélja.`,
                              captionStyle:
                                styleDef.travelDetailsIMLocationCaption,
                              dotType: "verticalDotsIntermediateDot",
                              isOtherPassenger: true,
                            },
                            {
                              title: destinationAddress,
                              caption: `Érkezés: ${endTime}`,
                              dotType: "verticalDotsEndDot",
                            },
                          ]),
                    ]}
                    paymentInfo={{
                      ...(amIPassenger || passengerDidQuit
                        ? {
                            finalAmountText: "Fizetendő összeg",
                            finalAmount: `${myFare} HUF`,
                          }
                        : {
                            taxiAmountText: "Taxiban fizetendő összeg (max.)",
                            taxiAmount: `${myFare + otherPassengerFare} HUF`,
                            fellowAmountText: `${otherPassenger.name} része`,
                            fellowAmount: `${otherPassengerFare} HUF`,
                            finalAmountText: "Fizetendő összeg",
                            finalAmount: `${myFare} HUF`,
                          }),
                    }}
                    onCancel={amIPassenger ? this.onCancel : undefined}
                    cancelText={amIPassenger ? "ÚT LEMONDÁSA" : undefined}
                  />
                </>
              ) : (
                <Spinner color="#f37121" />
              )}
            </View>
          </>
        ) : (
          <></>
        )}
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    position: "relative",
    justifyContent: "space-between",
    flex: 1,
  },
  header: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingVertical: 30,
    paddingHorizontal: 20,
    width: "100%",
    height: 100,
  },
  startTitle: {
    marginTop: 5,
  },
  startCatchPhrase: {
    color: "#999",
  },
  mapWrapper: {
    width: "100%",
    flex: 1,
  },
  map: {
    position: "absolute",
    width: "100%",
    height: "100%",
    zIndex: -1,
    top: 0,
    left: 0,
  },
  profileButton: {
    justifyContent: "center",
    alignItems: "center",
    width: 50,
    height: 50,
    borderWidth: 2,
    borderColor: "#ffffff",
    borderStyle: "solid",
  },
  startButton: {
    justifyContent: "space-between",
    marginTop: 20,
    borderColor: "#999",
    height: 50,
  },
  startButtonIcon: {
    marginRight: 10,
  },
  footer: {
    paddingVertical: 30,
    paddingHorizontal: 20,
    backgroundColor: "#ffffff",
    width: "100%",
    flexDirection: "column",
    justifyContent: "flex-start",
    height: "auto",
  },
});

const mapStateToProps = ({ user, locations, trip }) => ({
  ...user,
  ...locations,
  ...trip,
});

const mapDispatchToProps = {
  setExistingTrip,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(WalkingRoutePlanScreen);
