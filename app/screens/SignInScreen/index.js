import React, { Component } from "react";
import { View, StyleSheet } from "react-native";
import { Container, Footer, H1, Text, Button } from "native-base";
import * as WebBrowser from "expo-web-browser";
import AuthService from "../../services/auth";
import { _readAsyncStorage } from "../../utils/helpers";
import { Feather } from "@expo/vector-icons";
import defaultStyle from "../../native-base-theme/variables/mandy";

class SignInScreen extends Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);
  }

  _authWithFacebook = async () => {
    await AuthService.signInOrSignUpWithFacebook();
    let isUserLoggedIn = await _readAsyncStorage("user");

    if (isUserLoggedIn) {
      this.props.navigation.navigate("App");
    } else {
      this.props.navigation.push("SignInScreen");
    }
  };

  _linkMoreInfo = () => {
    this.props.navigation.navigate("MoreInfoScreen");
  };

  _triggerWebBrowserByLink = (link) => {
    link && WebBrowser.openBrowserAsync(link);
  };

  render() {
    return (
      <Container>
        <View style={styles.content}>
          <H1 style={styles.loginTitle}>Hello itt!</H1>
          <Text style={styles.loginLead}>
            Mandyvel megoszthatod utazásod költségeit, így akár 45%-al olcsóbban
            utazhatsz!
          </Text>
          <Button
            primary
            block
            iconRight
            noShadow
            transparent
            onPress={this._authWithFacebook}
            style={styles.loginButton}
          >
            <Text style={{ color: defaultStyle.btnInfoColor }}>
              Tovább Facebookkal
            </Text>
            <Feather
              name="arrow-right"
              size={24}
              style={{
                ...styles.loginButtonIcon,
                color: defaultStyle.btnInfoColor,
              }}
            />
          </Button>
        </View>
        <Footer style={styles.footer}>
          <Button
            small
            block
            transparent
            dark
            style={styles.footerButton}
            onPress={() =>
              this._triggerWebBrowserByLink("https://getmandy.app")
            }
          >
            <Feather name="info" size={20} color="#413b38" />
            <Text>További információ</Text>
          </Button>
        </Footer>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  content: {
    justifyContent: "center",
    flex: 1,
    paddingHorizontal: 20,
  },
  loginTitle: {
    width: "100%",
  },
  loginLead: {
    marginTop: 15,
    color: "#999",
  },
  loginButton: {
    backgroundColor: defaultStyle.btnPrimaryBg,
    marginTop: 35,
  },
  footer: {
    justifyContent: "flex-start",
    alignItems: "center",
    paddingHorizontal: 20,
    backgroundColor: "#fff",
  },
  footerButton: {
    alignSelf: "center",
  },
});

export default SignInScreen;
