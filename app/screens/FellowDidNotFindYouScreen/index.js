import React, { Component } from 'react';
import { H2 } from 'native-base';

import AppHeader from '../../components/AppHeader';
import { BackHandler } from 'react-native';
import {
  ContentBox, Spacer, PullDown, BrickButton, Caption, Cells
} from '../../components/Helpers/index';
import definitions from '../../assets/styles/definitions';
import {hardwareBackHandler} from '../../utils/helpers';

class FellowDidNotFindYouScreen extends Component {
  static navigationOptions = {
    header: null
  };

  _hardwareBackHandler = ()=>{hardwareBackHandler(this._navigateTo);return true};

  componentDidMount = async () => {
    BackHandler.addEventListener('hardwareBackPress', this._hardwareBackHandler);
  };

  componentWillUnmount = () => {
    BackHandler.removeEventListener('hardwareBackPress', this._hardwareBackHandler);
  };

  _navigateTo = (screen, params = {}) => {
    this.props.navigation.navigate(screen, params);
  };

  onTaxiOrder = () => {
    this._navigateTo('TaxiOrderScreen');
  }

  onWait = () => {
    this._navigateTo('RouteOfferScreen');
  }

  render() {
    return (
      <>
        <AppHeader navigateto='StartScreen'/>

        <ContentBox>
          <H2 style={{ lineHeight: 26 }}>
            {this.props.navigation.getParam('iDidNotFindIt',false)? 'Sajnáljuk, hogy nem találtad meg az út hirdetőjét' :
            'Nem talált meg a másik fél, a taxi elindult. Sajnáljuk.'}
          </H2>
          <Spacer h={12} />
          <Caption>
            Javasoljuk, hogy hívj egy taxit, vagy várakozz tovább, hogy kedvezményes útra találj.
          </Caption>
          <Spacer h={12} />
        </ContentBox>

        <PullDown>
          <ContentBox>
            <Cells
              cellStyle={[
                { flexBasis: '48.75%' },
                { flexBasis: '2.5%' },
                { flexBasis: '48.75%' }
              ]}
            >
              <BrickButton
                style={{ backgroundColor: definitions.colors.MANDY_LIGHT_GRAY_OPAQUE }}
                textStyle={{ color: definitions.colors.MANDY_DARK_GRAY }}
                onPress={this.onWait}
              >
                Várakozás
              </BrickButton>

              <Spacer />

              <BrickButton onPress={this.onTaxiOrder}>
                Taxi rendelés
              </BrickButton>

            </Cells>
            <Spacer h={30} />
          </ContentBox>
        </PullDown>
      </>
    )
  }
}

export default FellowDidNotFindYouScreen;
