import React, { Component } from 'react';
import { Text, Container, Content, H1, Button } from 'native-base';

class MoreInfoScreen extends Component {
  static navigationOptions = {
    header: null,
  };

  render() {
    return (
      <Container
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
          paddingHorizontal: 20,
        }}
      >
        <Content
          contentContainerStyle={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            paddingHorizontal: 20,
          }}
        >
          <H1>Hello!</H1>
          <Text>További információk</Text>
          <Button
            primary
            block
            onPress={() => this.props.navigation.navigate('SignInScreen')}
          >
            <Text>Vissza</Text>
          </Button>
        </Content>
      </Container>
    );
  }
}

export default MoreInfoScreen;
