import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { View, StyleSheet } from 'react-native';
import { Container, Content, Item, Input, Toast, Root } from 'native-base';
import AppHeader from '../../components/AppHeader';
import { updateUser } from '../../redux/actions/user';
import { _readAsyncStorage } from '../../utils/helpers';
import { validateEmail } from '../../utils/validate';
import UserService from '../../services/user';
import { Ionicons } from '@expo/vector-icons';

class EditEmail extends PureComponent {
  static navigationOptions = {
    header: null,
  };

  state = {
    email: '',
    isValidEmail: false,
    typingStarted: false,
    firstVisit: true,
  };

  async componentDidMount() {
    const { email } = await this.props.user;

    this.setState({
      email,
    });
  }

  _onSaveNavigation = async () => {
    const { email, firstVisit, isValidEmail } = this.state;

    let isUserAlreadyRegistered = await UserService.getUserBy('email', email);

    // TODO: find better solution instead of this ugly form validation.
    if (!email) {
      Toast.show({
        duration: 1500,
        text: 'Email megadása kötelező!',
        type: 'danger',
      });
    } else if (
      firstVisit ||
      (isUserAlreadyRegistered.length &&
        isUserAlreadyRegistered[0].email === email)
    ) {
      this.props.navigation.navigate('ProfileScreen');
    } else if (!isValidEmail) {
      Toast.show({
        duration: 1500,
        text: 'A megadott email nem valid!',
        type: 'danger',
      });
    } else if (isUserAlreadyRegistered.length) {
      Toast.show({
        duration: 1500,
        text: 'Ez az email már regisztrálva lett!',
        buttonText: 'Okay',
        type: 'danger',
      });
    } else {
      const userId = await _readAsyncStorage('user');
      await this.props.updateUser(userId, { email });
      this.props.navigation.navigate('ProfileScreen');
    }
  };

  onInputChange = email => {
    const isValidEmail = validateEmail(String(email).toLowerCase());

    this.setState({
      email,
      isValidEmail,
      typingStarted: true,
      firstVisit: false,
    });
  };

  emptyEmail = () => {
    this.setState({
      email: '',
    });
  };

  render() {
    const { typingStarted, isValidEmail, email } = this.state;

    return (
      <Root>
        <Container style={styles.container}>
          <AppHeader
            title="Profil"
            onSaveNavigation={typingStarted && this._onSaveNavigation}
          />
          <Content scrollEnabled={false}>
            <View style={styles.inputWrapper}>
              <Item
                regular
                style={
                  typingStarted && email
                    ? isValidEmail
                      ? styles.inputSuccess
                      : styles.inputDanger
                    : {}
                }
              >
                <Input value={email} onChangeText={this.onInputChange} />
                {email ? (
                  <Ionicons
                    name="md-close-circle"
                    size={25}
                    color="#413B38"
                    onPress={this.emptyEmail}
                    style={{ paddingRight: 15 }}
                  />
                ) : null}
              </Item>
            </View>
          </Content>
        </Container>
      </Root>
    );
  }
}

const styles = StyleSheet.create({
  inputWrapper: {
    paddingHorizontal: 20,
  },
  inputSuccess: {
    borderColor: '#9ACA3C',
  },
  inputDanger: {
    borderColor: '#D24858',
  },
  item: {
    borderWidth: 1,
    borderColor: '#D5D5D5',
    borderStyle: 'solid',
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
});

const mapStateToProps = ({ user }) => ({
  ...user,
});

const mapDispatchToProps = {
  updateUser,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(EditEmail);
