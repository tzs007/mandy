import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { Container, Spinner } from 'native-base';
import {
  createSwitchNavigator,
  createStackNavigator,
  createAppContainer,
} from 'react-navigation';
import { _readAsyncStorage } from '../utils/helpers';
import AuthService from '../services/auth';

import SignInScreen from './SignInScreen';
import MoreInfoScreen from './MoreInfoScreen';
import StartScreen from './StartScreen';
import ProfileScreen from './ProfileScreen';
import EditEmailScreen from './EditEmailScreen';
import EditAddressScreen from './EditAddressScreen';
import EditPaymentScreen from './EditPaymentScreen';
import SetNewDestinationScreen from './SetNewDestinationScreen';
import RouteOfferScreen from './RouteOfferScreen';
import PickRouteLocationScreen from './PickRouteLocationScreen';
import TaxiOrderScreen from './TaxiOrderScreen';
import TravelDetailsScreen from './TravelDetailsScreen';
import RouteMatchScreen from './RouteMatchScreen';
import TaxiWaitingScreen from './TaxiWaitingScreen';
import TravelFinishedScreen from './TravelFinishedScreen';
import FellowDidNotFindYouScreen from './FellowDidNotFindYouScreen';
import DidFellowArriveScreen from './DidFellowArriveScreen';
import WalkingRoutePlanScreen from './WalkingRoutePlanScreen';
import OngoingRoutePlanScreen from './OngoingRoutePlanScreen';

class FirstScreen extends Component {
  constructor(props) {
    super(props);
    this._userIsLoggedIn();
  }

  _userIsLoggedIn = async () => {
    const user = await _readAsyncStorage('user');
    const token = await _readAsyncStorage('token');
    if (user === '' || token === '') {
      AuthService.signOut();
      return;
    }
    this.props.navigation.navigate(user && token ? 'App' : 'Auth');
  };

  render() {
    return (
      <Container style={styles.container}>
        <Spinner color="#f37121" />
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

const AppStack = createStackNavigator({
  StartScreen,
  SetNewDestinationScreen,
  RouteOfferScreen,
  ProfileScreen,
  EditEmailScreen,
  EditPaymentScreen,
  EditAddressScreen,
  PickRouteLocationScreen,
  TaxiOrderScreen,
  TravelDetailsScreen,
  RouteMatchScreen,
  TaxiWaitingScreen,
  TravelFinishedScreen,
  FellowDidNotFindYouScreen,
  DidFellowArriveScreen,
  WalkingRoutePlanScreen,
  OngoingRoutePlanScreen,
});
const AuthStack = createStackNavigator({
  SignInScreen,
  MoreInfoScreen,
});

export default createAppContainer(
  createSwitchNavigator(
    {
      AuthLoading: FirstScreen,
      App: AppStack,
      Auth: AuthStack,
    },
    {
      initialRouteName: 'AuthLoading',
    },
  ),
);
