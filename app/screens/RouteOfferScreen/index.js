import React, { PureComponent } from "react";
import {
  Platform,
  StyleSheet,
  View,
  Dimensions,
  Alert,
  Modal,
  Image,
  TouchableOpacity,
} from "react-native";
import { Feather } from "@expo/vector-icons";
import { Container, Text, Button, Spinner, Thumbnail } from "native-base";
import { connect } from "react-redux";
import { setInitialRoute, deleteRoute } from "../../redux/actions/route";
import { setExistingTrip } from "../../redux/actions/trip";
import { createDocListener } from "../../services/route";

import Constants from "expo-constants";
import * as Location from "expo-location";
import * as Permissions from "expo-permissions";
import { Ionicons } from "@expo/vector-icons";

import CountDown from "react-native-countdown-component";

import MapView, { Marker, Callout } from "react-native-maps";
import MapViewDirections from "react-native-maps-directions";
import DirectionMarker from "../../components/DirectionMarker";
import DirectionLabel from "../../components/DirectionLabel";
import mapStyle from "../../utils/mapStyle";
import { GOOGLE_MAP_APIKEY } from "../../utils/constants";

import RouteService from "../../services/route";
import { firestore } from "../../utils/firebase";
import Rating from "../../components/Rating";

const { width, height } = Dimensions.get("window");

class RouteOfferScreen extends PureComponent {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);
    this.mapView = null;
    this.state = {
      isMapReady: false,
      expirationTime: 60 * 15,
      countDownId: "",
      shouldMapViewBe: true,
      modalVisible: false,
      secondModal: false,
    };

    if (Platform.OS === "android" && !Constants.isDevice) {
      this.setState({
        errorMessage:
          "Oops, this will not work on Sketch in an Android emulator. Try it on your device!",
      });
    } else {
      this._getLocationAsync();
    }
  }

  componentDidMount = async () => {
    this.willFocusListener = this.props.navigation.addListener(
      "willFocus",
      () => {
        this.setState({ shouldMapViewBe: true });
      }
    );

    await this._screenDidFocus();

    this.blurListener = this.props.navigation.addListener("didBlur", () => {
      this.setState({ shouldMapViewBe: false });
      clearInterval(this.updateRoute);
    });
  };

  componentWillUnmount() {
    this.willFocusListener.remove();
    this.blurListener.remove();
    clearInterval(this.updateRoute);
  }

  _detachListener = () => {
    if (this.detachRouteListener) {
      this.detachRouteListener();
      this.detachRouteListener = undefined;
    }
  };

  _screenDidFocus = async () => {
    console.log("_screenDidFocus");
    const { pickUpCoords } = this.props.locations.pickUpLocation;
    const { destinationCoords } = this.props.locations.destinationLocation;

    const routeId = await this.props.setInitialRoute({
      pickUpCoords,
      destinationCoords,
    });

    const detachRouteListener = await createDocListener(
      routeId,
      async (tripId) => {
        console.log("callback");
        await this.props.setExistingTrip(tripId);
        this.setState({ modalVisible: true });
        this.props.deleteRoute(routeId);
      }
    );
    this.detachRouteListener = detachRouteListener;

    this.updateRoute = setInterval(
      () =>
        RouteService.updateRoute(routeId, {
          createdAt: firestore.Timestamp.now(),
        }),
      5000
    );
  };

  _getLocationAsync = async () => {
    let { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== "granted") {
      this.setState({
        errorMessage: "Permission to access location was denied",
      });
    }

    let currentLocation = await Location.getCurrentPositionAsync({});
    this.setState({ currentLocation });
  };

  _onMapLayout = () => {
    this.setState({ isMapReady: true });
  };

  _navigateTo = (screen, props = {}) => {
    clearInterval(this.updateRoute);
    this._detachListener();
    this.props.navigation.navigate(screen, props);
  };

  _timeExpired = () => {
    Alert.alert(
      "Sajnos nincs találat.",
      "Jelenleg senki nem keres utastársat a közeledben.",
      [
        {
          text: "Taxit hívok!",
          onPress: () => this._navigateTo("TaxiOrderScreen"),
        },
        {
          text: "Várakozom",
          onPress: async () =>
            await this.setState({ countDownId: Math.random().toString() }),
        },
      ],
      { cancelable: true }
    );
    return;
  };

  render() {
    const {
      currentLocation,
      isMapReady,
      expirationTime,
      countDownId,
      shouldMapViewBe,
    } = this.state;
    const { pickUpLocation, destinationLocation } = this.props.locations;
    const { pickUpAddress, pickUpCoords } = pickUpLocation;
    const { destinationAddress, destinationCoords } = destinationLocation;

    const { goBack } = this.props.navigation;

    const isAllDataAvailable =
      pickUpLocation &&
      destinationLocation &&
      currentLocation &&
      typeof currentLocation !== undefined;

    if (shouldMapViewBe && isAllDataAvailable && isMapReady) {
      setTimeout(() => this.marker.showCallout(), 1);
    }

    let amIPassenger, otherUser, myFare, otherFare, userRatings;

    if (this.props.trip) {
      const { trip } = this.props;

      amIPassenger = this.props.user.id === trip.users.passenger.id;
      otherUser = trip.users[amIPassenger ? "initiator" : "passenger"];
      myFare = Math.round(
        trip.finances[amIPassenger ? "passenger" : "initiator"].fare
      );
      otherFare = Math.round(
        trip.finances[!amIPassenger ? "passenger" : "initiator"].fare
      );
      userRatings =
        otherUser.rating && otherUser.rating.length
          ? getAverage(otherUser.rating)
          : 5;
    }
    console.log("trip?", this.props.trip ? "yes :)" : "no :(");
    console.log("modal?", this.state.modalVisible ? "yes :)" : "no :(");
    console.log(this.props.trip);
    return (
      <Container style={styles.container}>
        {shouldMapViewBe ? (
          <>
            <View style={styles.mapWrapper}>
              <Button
                small
                primary
                bordered
                rounded
                noShadow
                style={styles.exitButton}
                onPress={() => {
                  this._detachListener();
                  goBack();
                }}
              >
                <Ionicons name={"md-arrow-back"} size={25} color="#413B38" />
              </Button>

              {isAllDataAvailable ? (
                <MapView
                  provider="google"
                  style={styles.map}
                  customMapStyle={mapStyle}
                  minZoomLevel={9}
                  initialRegion={{
                    latitude: currentLocation.coords.latitude,
                    longitude: currentLocation.coords.longitude,
                    latitudeDelta: 0.05,
                    longitudeDelta: 0.05,
                  }}
                  ref={(c) => (this.mapView = c)}
                  onMapReady={this._onMapLayout}
                  onCalloutPress={() => this.props.navigation.goBack()}
                >
                  {isMapReady &&
                    Object.keys(pickUpCoords).length > 0 &&
                    Object.keys(destinationCoords).length > 0 && (
                      <>
                        <Marker
                          coordinate={{
                            latitude: currentLocation.coords.latitude,
                            longitude: currentLocation.coords.longitude,
                          }}
                        >
                          <DirectionMarker type="currentPosition" />
                        </Marker>
                        <Marker
                          coordinate={{
                            latitude: pickUpCoords.lat,
                            longitude: pickUpCoords.lng,
                          }}
                        >
                          <DirectionMarker type="pickUpLocation" />
                          <Callout tooltip={true}>
                            <DirectionLabel labelText={pickUpAddress} />
                          </Callout>
                        </Marker>
                        <Marker
                          coordinate={{
                            latitude: destinationCoords.lat,
                            longitude: destinationCoords.lng,
                          }}
                          ref={(ref) => {
                            this.marker = ref;
                          }}
                        >
                          <DirectionMarker type="destination" />
                          <Callout tooltip={true}>
                            <DirectionLabel labelText={destinationAddress} />
                          </Callout>
                        </Marker>
                        <MapViewDirections
                          origin={{
                            latitude: pickUpCoords.lat,
                            longitude: pickUpCoords.lng,
                          }}
                          destination={{
                            latitude: destinationCoords.lat,
                            longitude: destinationCoords.lng,
                          }}
                          apikey={GOOGLE_MAP_APIKEY}
                          strokeWidth={6}
                          mode="DRIVING"
                          strokeColor="#f37121"
                          onReady={(result) => {
                            this.mapView.fitToCoordinates(result.coordinates, {
                              edgePadding: {
                                right: 300,
                                bottom: 20,
                                left: 300,
                                top: 50,
                              },
                            });
                          }}
                        />
                      </>
                    )}
                </MapView>
              ) : (
                <Spinner color="#f37121" />
              )}
            </View>
            <View style={styles.footer}>
              <View style={styles.searchingWrapper}>
                <Spinner color="#f37121" />
                <CountDown
                  size={height / 20}
                  until={expirationTime}
                  id={countDownId}
                  onFinish={() => this._timeExpired()}
                  style={styles.countDownStyle}
                  digitStyle={styles.digitStyle}
                  digitTxtStyle={styles.digitTxtStyle}
                  separatorStyle={styles.separatorStyle}
                  timeToShow={["M", "S"]}
                  timeLabels={{ m: null, s: null }}
                  showSeparator
                />
                <Text style={styles.timerText}>
                  Utastárs keresése folyamatban...
                </Text>
              </View>
              <View style={styles.taxiOrderWrapper}>
                <View style={{ flexDirection: "column" }}>
                  <Text style={styles.taxiOrderLabel}>Unod a várakozást?</Text>
                  <Text style={styles.taxiOrderText}>Hívj taxit most!</Text>
                </View>
                <Button
                  transparent
                  style={styles.taxiOrderButton}
                  onPress={() => this._navigateTo("TaxiOrderScreen")}
                >
                  <Text style={styles.taxiOrderButtonText}>Taxi rendelés</Text>
                </Button>
              </View>
            </View>
          </>
        ) : (
          <></>
        )}
        {this.props.trip && this.state.modalVisible ? (
          <View style={styles.modalContainer}>
            <Modal
              animationType="slide"
              transparent
              visible={this.state.modalVisible}
              presentationStyle={"overFullScreen"}
              onRequestClose={() => {
                if (!this.state.secondModal)
                  this.props.navigation.navigate("StartScreen");
                this.setState({ secondModal: false });
              }}
            >
              <View style={styles.modalContainer}>
                <View style={styles.modalBlock}>
                  {this.state.secondModal ? (
                    <>
                      <Thumbnail
                        source={{ uri: otherUser.picture }}
                        style={{ marginTop: "5%" }}
                      />
                      <Rating stars={userRatings} />
                    </>
                  ) : (
                    <Image source={require("../../assets/logo.png")} />
                  )}
                  {this.state.secondModal ? (
                    <>
                      <Text
                        style={{
                          fontSize: 20,
                          marginTop: "8%",
                          textAlign: "center",
                        }}
                      >
                        {otherUser.name}{" "}
                        {amIPassenger
                          ? "útjához csatlakoztál."
                          : "csatlakozott hozzád."}
                      </Text>
                      <Text style={styles.greyText}>Jó utat kívánunk</Text>
                    </>
                  ) : (
                    <>
                      <Text style={{ fontSize: 20, marginTop: "8%" }}>
                        Találtunk neked {amIPassenger ? "utat!" : "útitársat!"}
                      </Text>
                      <Text style={styles.greyText}>
                        Kérjük sétálj a taxi indulási pontjára és várj az
                        útitárs megérkezésére.
                      </Text>
                      <Text
                        style={{
                          fontSize: 30,
                          marginTop: "8%",
                          textAlign: "center",
                        }}
                      >
                        {amIPassenger ? myFare : "-" + otherFare} HUF{" "}
                      </Text>
                      <Text style={styles.greyText}>
                        Az utad {amIPassenger ? "teljes ára." : "árából."}
                      </Text>
                    </>
                  )}
                  <View style={{ marginTop: "12%" }}>
                    <TouchableOpacity
                      style={styles.modalButton}
                      onPress={async () => {
                        if (this.state.secondModal) {
                          await this.setState({ modalVisible: false });
                          this._navigateTo("WalkingRoutePlanScreen", {
                            amIPassenger: amIPassenger,
                          });
                        } else {
                          this.setState({ secondModal: true });
                        }
                      }}
                    >
                      <Feather
                        name={this.state.secondModal ? "check" : "arrow-right"}
                        size={24}
                        color="#413B38"
                      />
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </Modal>
          </View>
        ) : (
          <></>
        )}
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: width,
    height: height,
    position: "relative",
    justifyContent: "space-between",
    flex: 1,
  },
  searchingWrapper: {
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    paddingVertical: 35,
  },
  timer: {
    fontSize: 40,
    paddingTop: 15,
  },
  timerText: {
    width: width,
    fontSize: 16,
    textAlign: "center",
    opacity: 0.3,
  },
  countDownStyle: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    width: width,
  },
  digitStyle: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 0,
  },
  digitTxtStyle: {
    flexDirection: "row",
    justifyContent: "center",
    textAlign: "center",
    color: "#413B38",
    fontWeight: "400",
    width: width / 2,
    fontSize: height / 14,
  },
  separatorStyle: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center",
    color: "#413B38",
    top: -2,
    width: 20,
  },
  taxiOrderButton: {
    backgroundColor: "#F3F3F3",
  },
  taxiOrderWrapper: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingHorizontal: 20,
    paddingVertical: 35,
    borderWidth: 1,
    borderStyle: "solid",
    borderTopColor: "#F3F3F3",
    borderLeftWidth: 0,
    borderRightWidth: 0,
    borderBottomWidth: 0,
  },
  taxiOrderLabel: {
    fontSize: 14,
    fontWeight: "bold",
  },
  taxiOrderText: {
    fontSize: 14,
  },
  taxiOrderButtonText: {
    color: "#413B38",
    textTransform: "uppercase",
    fontWeight: "bold",
    fontSize: 13,
  },
  mapWrapper: {
    width: "100%",
    flex: 1,
  },
  map: {
    position: "absolute",
    width: "100%",
    height: "100%",
    zIndex: -1,
    top: 0,
    left: 0,
  },
  exitButton: {
    position: "absolute",
    zIndex: 2,
    top: 30,
    left: 20,
    justifyContent: "center",
    alignItems: "center",
    width: 40,
    height: 40,
    borderWidth: 1,
    borderColor: "#D5D5D5",
    backgroundColor: "#fff",
    borderStyle: "solid",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.3,
    shadowRadius: 5,
    elevation: 5,
    borderColor: "white",
  },
  footer: {
    backgroundColor: "#ffffff",
    width: "100%",
    flexDirection: "column",
    justifyContent: "flex-start",
    height: "auto",
  },
  modalContainer: {
    position: "absolute",
    height: "100%",
    width: "100%",
    top: 0,
    left: 0,
    justifyContent: "center",
    alignItems: "center",
    flex: 1,
    backgroundColor: "rgba(0, 0, 0, .3)",
  },
  modalBlock: {
    width: "80%",
    backgroundColor: "white",
    borderRadius: 10,
    alignItems: "center",
    justifyContent: "center",
    padding: "5%",
  },
  modalButton: {
    borderRadius: Math.round(Dimensions.get("window").width * 0.1) / 2,
    width: Dimensions.get("window").width * 0.1,
    height: Dimensions.get("window").width * 0.1,
    backgroundColor: "white",
    borderColor: "lightgrey",
    borderWidth: 0.5,
    flex: 0,
    alignItems: "center",
    justifyContent: "center",
  },
});

const mapDispatchToProps = {
  setInitialRoute,
  deleteRoute,
  setExistingTrip,
};

const mapStateToProps = ({ user, route, locations, trip }) => ({
  ...user,
  ...route,
  ...locations,
  ...trip,
});

export default connect(mapStateToProps, mapDispatchToProps)(RouteOfferScreen);
