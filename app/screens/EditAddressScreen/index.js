import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { _readAsyncStorage } from '../../utils/helpers';

import {
  Container,
  Left,
  Body,
  Content,
  ListItem,
  Form,
  Item,
  Input,
  Text,
} from 'native-base';
import { updateUser } from '../../redux/actions/user';
import { StyleSheet } from 'react-native';
import { Ionicons } from '@expo/vector-icons';

import geo from '../../utils/geo';

import AppHeader from '../../components/AppHeader';

class EditAddressScreen extends PureComponent {
  static navigationOptions = {
    header: null,
  };

  state = {
    addressList: null,
    addresses: null,
    address: null,
    coordinates: [],
    isAddressSelected: false,
  };

  componentDidMount() {
    const { key } = this.props.navigation.state.params;
    const { addresses } = this.props.user;

    const address = addresses.length
      ? addresses.filter(value => value.name === key)
      : '';

    this.setState({
      addresses,
      address: address.length ? address[0].address : '',
    });
  }

  _navigateTo = (screen, props = {}) => {
    this.props.navigation.navigate(screen, props);
  };

  _onChangeText = async userInput => {
    let filteredAddresses = null;

    this.setState({
      address: userInput,
    });

    if (userInput.length >= 3) {
      filteredAddresses = await geo.get(userInput);
    }

    this.setState({
      addressList: filteredAddresses,
    });
  };

  _onSaveNavigation = async () => {
    const { addresses } = this.state;
    const { key } = this.props.navigation.state.params;

    const { address, coordinates } = this.state;
    addresses.map(item => {
      if (item.name === key) {
        item.address = address;
        item.coordinates = coordinates;
      }
      return;
    });
    const userId = await _readAsyncStorage('user');
    await this.props.updateUser(userId, { addresses });
    this.props.navigation.goBack();
  };

  _onSelectAddress = (address, coordinates, addressList) => {
    this.setState({
      address,
      addressList,
      coordinates,
      isAddressSelected: true,
    });
    return;
  };

  _emptyAddress = () => {
    this.setState({
      address: '',
    });
  };

  render() {
    const { addressList, address, isAddressSelected } = this.state;
    const { key } = this.props.navigation.state.params;

    const editScreenTexts = {
      home: {
        title: 'Otthon szerkesztése',
        inputPlaceholder: 'Adj meg az otthonod címét!',
      },
      office: {
        title: 'Munkahely szerkesztése',
        inputPlaceholder: 'Adj meg egy munkahelyi címet!',
      },
    };

    return (
      <Container>
        <AppHeader
          title={editScreenTexts[key].title}
          onSaveNavigation={isAddressSelected && this._onSaveNavigation}
          back={true}
        />
        <Form style={styles.form}>
          <Item regular style={styles.formItem}>
            <Input
              placeholder={editScreenTexts[key].inputPlaceholder}
              value={address}
              onChangeText={e => this._onChangeText(e)}
              multiLine={false}
              textAlignVertical="center"
            />
            {address ? (
              <Ionicons
                name="md-close-circle"
                size={25}
                color="#413B38"
                onPress={this._emptyAddress}
                style={{ paddingRight: 15 }}
              />
            ) : null}
          </Item>
        </Form>
        <Content>
          {/* populated list by Google Maps */}
          {addressList &&
            addressList.length > 0 &&
            addressList.map(address => (
              <ListItem
                icon
                noIndent
                key={address.place_id}
                onPress={() =>
                  this._onSelectAddress(
                    address.formatted_address,
                    address.geometry.location,
                    null,
                  )
                }
              >
                <Left>
                  <Ionicons name="ios-pin" size={24} color="#ccc" />
                </Left>
                <Body>
                  <Text>
                    {address.address_components.map(
                      component =>
                        component.types.includes('locality') &&
                        component.long_name,
                    )}
                  </Text>
                  <Text note numberOfLines={1}>
                    {address.formatted_address}
                  </Text>
                </Body>
              </ListItem>
            ))}
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  form: {
    paddingHorizontal: 20,
  },
  formItem: {
    marginBottom: 10,
  },
  routeButton: {
    flexDirection: 'column',
    justifyContent: 'center',
    width: 50,
    height: 50,
  },
  routeButtonIcon: {},
  savedLocationButtonText: {},
});

const mapStateToProps = ({ user, isUserUpdated }) => ({
  ...user,
  ...isUserUpdated,
});

const mapDispatchToProps = {
  updateUser,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(EditAddressScreen);
