import React, { PureComponent } from "react";
import moment from "moment";
import { Platform, StyleSheet, View, BackHandler } from "react-native";
import { Container, Button, Spinner } from "native-base";
import MapView, { Marker } from "react-native-maps";
import MapViewDirections from "react-native-maps-directions";

import { connect } from "react-redux";
import { firestore, db, geoDb } from "../../utils/firebase";
import Constants from "expo-constants";
import * as Location from "expo-location";
import * as Permissions from "expo-permissions";
import mapStyle from "../../utils/mapStyle";
import DirectionMarker from "../../components/DirectionMarker";
import TravelDetails from "../../components/TravelDetails";
import definitions from "../../assets/styles/definitions";
import styleDef from "../../assets/styles/mandy";
import { hardwareBackHandler, calcDistance } from "../../utils/helpers";

import {
  AntDesign,
  FontAwesome,
  MaterialCommunityIcons,
} from "@expo/vector-icons";

import { GOOGLE_MAP_APIKEY } from "../../utils/constants";

class OnGoingRoutePlanScreen extends PureComponent {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      isMapReady: false,
      errorMessage: "",
      currentLocation: null,
      pickUpAddress: "",
      pickUpCoords: null,
      destinationAddress: "",
      destinationCoords: null,
      shouldMapViewBe: true,
    };

    if (Platform.OS === "android" && !Constants.isDevice) {
      this.setState({
        errorMessage:
          "Oops, this will not work on Sketch in an Android emulator. Try it on your device!",
      });
    } else {
      this._getLocationAsync();
    }

    this.personalTripData = this._getPersonalTripData();
  }

  _getPersonalTripData = () => {
    const trip = this.props.trip;
    const amIPassenger = this.props.user.id === trip.users.passenger.id;
    const didPassengerQuit = trip.type === "single";
    const intermediateStop =
      amIPassenger || didPassengerQuit
        ? undefined
        : this._formatCoordinates(trip.places.intermediateStop.coordinates);
    const otherPassenger = trip.users[amIPassenger ? "initiator" : "passenger"];
    const otherPassengerFare = Math.round(
      trip.finances[amIPassenger ? "initiator" : "passenger"].fare
    );
    const myFare = Math.round(
      trip.finances[amIPassenger ? "passenger" : "initiator"].fare
    );
    const startTime = trip.startTime
      ? moment(trip.startTime.toDate()).format("HH:mm")
      : moment(firestore.Timestamp.now().toDate()).format("HH:mm");

    const endTime = amIPassenger
      ? moment(trip.startTime.toDate())
          .add(trip.places.intermediateStop.duration, "seconds")
          .format("HH:mm")
      : moment(trip.startTime.toDate())
          .add(trip.duration, "seconds")
          .format("HH:mm");

    return {
      amIPassenger,
      pickUpLocation: this._formatCoordinates(
        trip.places.pickUpLocation.coordinates
      ),
      pickupAddress: trip.places.pickUpLocation.address,
      intermediateStop,
      intermediateStopAddress: intermediateStop
        ? trip.places.intermediateStop.address
        : undefined,
      destination: this._formatCoordinates(
        trip.places[
          intermediateStop || didPassengerQuit
            ? "dropOffLocation"
            : "intermediateStop"
        ].coordinates
      ),
      destinationAddress:
        trip.places[intermediateStop ? "dropOffLocation" : "intermediateStop"]
          .address,
      otherPassenger,
      otherPassengerFare,
      myFare,
      startTime,
      endTime,
      didPassengerQuit,
    };
  };

  _hardwareBackHandler = () => {
    hardwareBackHandler(this._navigateTo);
    return true;
  };

  componentDidMount = async () => {
    this.willFocusListener = this.props.navigation.addListener(
      "willFocus",
      () => {
        this.setState({ shouldMapViewBe: true });
      }
    );

    this.blurListener = this.props.navigation.addListener("didBlur", () => {
      this.setState({ shouldMapViewBe: false });
    });

    BackHandler.addEventListener(
      "hardwareBackPress",
      this._hardwareBackHandler
    );
  };

  componentWillUnmount = () => {
    this.willFocusListener.remove();
    this.blurListener.remove();
    this._detachLocationWatcher();
    BackHandler.removeEventListener(
      "hardwareBackPress",
      this._hardwareBackHandler
    );
  };

  _navigateTo = (screen, params = {}) => {
    this._detachLocationWatcher();
    this.props.navigation.navigate(screen, params);
  };

  _getLocationAsync = async () => {
    let { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== "granted") {
      this.setState({
        errorMessage: "Permission to access location was denied",
      });
      return;
    }

    let currentLocation = await Location.getCurrentPositionAsync({});
    this.setState({
      currentLocation: currentLocation.coords,
    });
  };

  _detachLocationWatcher = () => {
    if (this.detachLocationWatcher) {
      this.detachLocationWatcher();
      this.detachLocationWatcher = undefined;
    }
  };

  _convertCoordsToArray = (coords) => {
    return [coords.latitude, coords.longitude];
  };

  _distanceFromDestination = async (currentLocation) => {
    const distanceFromDestination = await calcDistance(
      this._convertCoordsToArray(currentLocation),
      this._convertCoordsToArray(this.personalTripData.destination)
    );
    return distanceFromDestination * 1000; //to get this in meter
  };

  _onMapLayout = async () => {
    this.setState({
      isMapReady: true,
    });
    const detachLocationWatcher = await Location.watchPositionAsync(
      {
        accuracy: Location.Accuracy.High,
        timeInterval: 2000,
        distanceInterval: 0,
      },
      async (loc) => {
        this.setState({ currentLocation: loc.coords });
        const distance = await this._distanceFromDestination(loc.coords);
        if (distance < 100) {
          this._navigateTo("TravelFinishedScreen");
        }
      }
    );
    this.detachLocationWatcher = detachLocationWatcher.remove;
  };

  _formatCoordinates = (coordinates) => ({
    latitude: coordinates.lat,
    longitude: coordinates.lng,
  });

  render() {
    const { isMapReady, currentLocation } = this.state;

    const {
      amIPassenger,
      pickUpLocation,
      intermediateStop,
      destination,
      otherPassenger,
      myFare,
      otherPassengerFare,
      startTime,
      endTime,
      pickupAddress,
      intermediateStopAddress,
      destinationAddress,
      didPassengerQuit,
    } = this.personalTripData;

    const plateNumber = "ABC-456";

    return (
      <Container style={styles.container}>
        {this.state.shouldMapViewBe ? (
          <>
            <View style={styles.mapWrapper}>
              {currentLocation && typeof currentLocation !== undefined ? (
                <>
                  <MapView
                    provider="google"
                    style={styles.map}
                    customMapStyle={mapStyle}
                    minZoomLevel={9}
                    initialRegion={{
                      latitude: currentLocation.latitude,
                      longitude: currentLocation.longitude,
                      latitudeDelta: 0.02,
                      longitudeDelta: 0.02,
                    }}
                    onMapReady={this._onMapLayout}
                    onRegionChangeComplete={this._getPickUpCoordinates}
                  >
                    {isMapReady && (
                      <>
                        <Marker coordinate={currentLocation}>
                          <DirectionMarker type="currentPosition" />
                        </Marker>
                        <Marker coordinate={pickUpLocation}>
                          <DirectionMarker type="pickUpLocation" />
                        </Marker>
                        {amIPassenger || didPassengerQuit ? null : (
                          <Marker coordinate={intermediateStop}>
                            <DirectionMarker type="intermediateStop" />
                          </Marker>
                        )}
                        <Marker coordinate={destination}>
                          <DirectionMarker type="destination" />
                        </Marker>
                        {amIPassenger || didPassengerQuit ? (
                          <MapViewDirections
                            origin={pickUpLocation}
                            destination={destination}
                            apikey={GOOGLE_MAP_APIKEY}
                            strokeWidth={6}
                            mode="DRIVING"
                            strokeColor="#f37121"
                          />
                        ) : (
                          <>
                            <MapViewDirections
                              origin={pickUpLocation}
                              destination={intermediateStop}
                              apikey={GOOGLE_MAP_APIKEY}
                              strokeWidth={6}
                              mode="DRIVING"
                              strokeDasharray="3,3"
                              strokeColor="#f37121"
                            />
                            <MapViewDirections
                              origin={intermediateStop}
                              destination={destination}
                              apikey={GOOGLE_MAP_APIKEY}
                              strokeWidth={6}
                              mode="DRIVING"
                              strokeColor="#f37121"
                            />
                          </>
                        )}
                      </>
                    )}
                  </MapView>
                  <TravelDetails
                    header={{
                      left: {
                        title: endTime,
                        caption: `Várható érkezési idő.`,
                        icon: (
                          <AntDesign
                            name={"clockcircleo"}
                            size={22}
                            color={definitions.colors.MANDY_DARK_GRAY}
                          />
                        ),
                      },
                      right: {
                        title: `${
                          amIPassenger || didPassengerQuit
                            ? myFare
                            : myFare + otherPassengerFare
                        } HUF`,
                        caption: amIPassenger
                          ? "Fizetendő összeg (kártyával)"
                          : "Taxiban általad fizetendő összeg (max.)",
                        icon: (
                          <MaterialCommunityIcons
                            name={"cash-multiple"}
                            size={22}
                            color={definitions.colors.MANDY_DARK_GRAY}
                          />
                        ),
                      },
                    }}
                    plateNumber={plateNumber}
                    pathDetails={[
                      {
                        title: pickupAddress,
                        titleStyle: styleDef.travelDetailsIMLocationTitle,
                        caption: `Indulás:  ${startTime}`,
                        dotCount: 3,
                        isOtherPassenger: false,
                      },
                      ...(amIPassenger || didPassengerQuit
                        ? []
                        : [
                            {
                              title: intermediateStopAddress,
                              caption: `${otherPassenger.name} úticélja`,
                              dotType: "verticalDotsIntermediateDot",
                              isOtherPassenger: true,
                            },
                          ]),
                      {
                        title: destinationAddress,
                        caption: `Érkezés: ${endTime}`,
                        dotType: "verticalDotsEndDot",
                        isOtherPassenger: false,
                      },
                    ]}
                    paymentInfo={{
                      ...(amIPassenger || didPassengerQuit
                        ? {
                            finalAmountText: "Összesen (max.)",
                            finalAmount: `${myFare} HUF`,
                          }
                        : {
                            taxiAmountText: "Taxiban fizetendő összeg (max.)",
                            taxiAmount: `${myFare + otherPassengerFare} HUF`,
                            fellowAmountText: `${otherPassenger.name} része`,
                            fellowAmount: `${otherPassengerFare} HUF`,
                            finalAmountText: "Összesen (max.)",
                            finalAmount: `${myFare} HUF`,
                          }),
                    }}
                  />
                </>
              ) : (
                <Spinner color="#f37121" />
              )}
            </View>
          </>
        ) : (
          <></>
        )}
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    position: "relative",
    justifyContent: "space-between",
    flex: 1,
  },
  header: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingVertical: 30,
    paddingHorizontal: 20,
    width: "100%",
    height: 100,
  },
  startTitle: {
    marginTop: 5,
  },
  startCatchPhrase: {
    color: "#999",
  },
  mapWrapper: {
    width: "100%",
    flex: 1,
  },
  map: {
    position: "absolute",
    width: "100%",
    height: "100%",
    zIndex: -1,
    top: 0,
    left: 0,
  },
  profileButton: {
    justifyContent: "center",
    alignItems: "center",
    width: 50,
    height: 50,
    borderWidth: 2,
    borderColor: "#ffffff",
    borderStyle: "solid",
  },
  startButton: {
    justifyContent: "space-between",
    marginTop: 20,
    borderColor: "#999",
    height: 50,
  },
  startButtonIcon: {
    marginRight: 10,
  },
  footer: {
    paddingVertical: 30,
    paddingHorizontal: 20,
    backgroundColor: "#ffffff",
    width: "100%",
    flexDirection: "column",
    justifyContent: "flex-start",
    height: "auto",
  },
});

const mapStateToProps = ({ user, locations, trip }) => ({
  ...user,
  ...locations,
  ...trip,
});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OnGoingRoutePlanScreen);
