import React, { PureComponent } from 'react';
import { getAverage } from '../../utils/helpers.js';
import { Text, Thumbnail, Container } from 'native-base';
import { StyleSheet, View, Image, Modal, TouchableOpacity, Alert, Dimensions } from 'react-native';
import { Feather } from '@expo/vector-icons';
import Rating from '../../components/Rating';
import { connect } from 'react-redux';

class RouteMatchScreen extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: true,
      secondModal: false
    };
  }

  static navigationOptions = {
    headerLeft: null,
    headerRight: null,
    headerStyle: {
      opacity: 0,
      height: 0
    }
  };

  componentDidMount = () => {
    this.focusListener = this.props.navigation.addListener('didFocus', () => {
      if (!this.state.modalVisible && this.state.secondModal) {
        this.setState({ modalVisible: true, secondModal: false });
      }
    });
  };

  componentWillUnmount() {
    this.focusListener.remove();
  }

  _navigateTo = (screen, params = {}) => {
    this.props.navigation.navigate(screen, params);
  };

  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }

  render() {
    const trip = this.props.trip;
    const amIPassenger = this.props.user.id === trip.users.passenger.id;
    const otherUser = trip.users[amIPassenger ? 'initiator' : 'passenger'];
    const myFare = Math.round(trip.finances[amIPassenger ? 'passenger' : 'initiator'].fare);
    const otherFare = Math.round(trip.finances[!amIPassenger ? 'passenger' : 'initiator'].fare);

    const userRatings =
      otherUser.rating && otherUser.rating.length ? getAverage(otherUser.rating) : 5;
  
    return (
      <Container>
        <View style={styles.container}>
          <Modal
            animationType="slide"
            transparent={false}
            visible={this.state.modalVisible}
            presentationStyle={'fullScreen'}
            onRequestClose={() => {
              if (!this.state.secondModal) this.props.navigation.navigate('StartScreen');
              this.setState({ secondModal: false });
            }}
          >
            <View style={styles.container}>
              <View style={styles.block}>
                {this.state.secondModal ? (
                  <>
                    <Thumbnail source={{ uri: otherUser.picture }} />
                    <Rating stars={userRatings} />
                  </>
                ) : (
                  <Image source={require('../../assets/logo.png')} />
                )}
                {this.state.secondModal ? (
                  <>
                    <Text style={{ fontSize: 20, marginTop: '8%', textAlign: 'center' }}>
                      {otherUser.name}{' '}
                      {amIPassenger ? 'útjához csatlakoztál.' : 'csatlakozott hozzád.'}
                    </Text>
                    <Text style={styles.greyText}>Jó utat kívánunk</Text>
                  </>
                ) : (
                  <>
                    <Text style={{ fontSize: 20, marginTop: '8%' }}>
                      Találtunk neked {amIPassenger ? 'utat!' : 'útitársat!'}
                    </Text>
                    <Text style={styles.greyText}>
                      Kérjük sétálj a taxi indulási pontjára és várj az útitárs megérkezésére.
                    </Text>
                    <Text style={{ fontSize: 30, marginTop: '8%', textAlign: 'center' }}>
                      {amIPassenger ? myFare : '-' + otherFare} HUF{' '}
                    </Text>
                    <Text style={styles.greyText}>
                      Az utad {amIPassenger ? 'teljes ára.' : 'árából.'}
                    </Text>
                  </>
                )}
                <View style={{ marginTop: '16%' }}>
                  <TouchableOpacity
                    style={styles.button}
                    onPress={async () => {
                      if (this.state.secondModal) {
                        await this.setState({ modalVisible: false });
                        this._navigateTo('WalkingRoutePlanScreen', { amIPassenger: amIPassenger });
                      } else {
                        this.setState({ secondModal: true });
                      }
                    }}
                  >
                    <Feather
                      name={this.state.secondModal ? 'check' : 'arrow-right'}
                      size={24}
                      color="#413B38"
                    />
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </Modal>
        </View>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'lightgrey'
  },
  block: {
    width: '80%',
    height: '64%',
    backgroundColor: 'white',
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    padding: '5%'
  },
  greyText: {
    fontSize: 14,
    opacity: 0.3,
    textAlign: 'center'
  },
  button: {
    borderRadius: Math.round(Dimensions.get('window').width * 0.1) / 2,
    width: Dimensions.get('window').width * 0.1,
    height: Dimensions.get('window').width * 0.1,
    backgroundColor: 'white',
    borderColor: 'lightgrey',
    borderWidth: 0.5,
    flex: 0,
    alignItems: 'center',
    justifyContent: 'center'
  }
});

const mapDispatchToProps = {};

const mapStateToProps = ({ user, trip }) => ({
  ...user,
  ...trip
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RouteMatchScreen);
