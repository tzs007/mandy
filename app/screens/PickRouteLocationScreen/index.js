// route planner map
import React, { PureComponent } from "react";
import { Platform, StyleSheet, View } from "react-native";
import { connect } from "react-redux";
import { setLocations } from "../../redux/actions/locations";
import { Container, Text, H2, H3, Button, Spinner } from "native-base";
import MapView, { Marker } from "react-native-maps";

import Constants from "expo-constants";
import * as Location from "expo-location";
import * as Permissions from "expo-permissions";
import mapStyle from "../../utils/mapStyle";

import AppHeader from "../../components/AppHeader";
import PositionMarker from "../../components/PositionMarker";

import { _isMatchingRoute } from "../../utils/helpers";

import geo from "../../utils/geo";

class PickRouteLocationScreen extends PureComponent {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      isMapReady: false,
      isMapOnDragging: false,
      errorMessage: "",
      currentLocation: null,
      pickUpAddress: "",
      pickUpCoords: null,
      destinationAddress: "",
      destinationCoords: null,
      searchingMatchingRoute: false,
      shouldMapViewBe: true,
      typeOfLocation: null,
    };

    if (Platform.OS === "android" && !Constants.isDevice) {
      this.setState({
        errorMessage:
          "Oops, this will not work on Sketch in an Android emulator. Try it on your device!",
      });
    } else {
      this._getLocationAsync();
    }
  }

  componentDidMount = () => {
    this.setState({
      typeOfLocation: this.props.navigation.getParam("typeOfLocation"),
    });

    this.focusListener = this.props.navigation.addListener("didFocus", () => {
      this._screenDidFocus();
    });

    this.willFocusListener = this.props.navigation.addListener(
      "willFocus",
      () => {
        this.setState({
          shouldMapViewBe: true,
        });
      }
    );

    this.blurListener = this.props.navigation.addListener("didBlur", () => {
      this.setState({
        shouldMapViewBe: false,
      });
    });
  };

  componentWillUnmount() {
    this.focusListener.remove();
    this.willFocusListener.remove();
    this.blurListener.remove();
  }

  _screenDidFocus = async () => {
    const { pickUpLocation, destinationLocation } = this.props.locations;

    await this.setState({
      ...pickUpLocation,
      ...destinationLocation,
    });
  };

  _navigateTo = (screen, props = {}) => {
    this.props.navigation.navigate(screen, props);
  };

  _getLocationAsync = async () => {
    let { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== "granted") {
      this.setState({
        errorMessage: "Permission to access location was denied",
      });
    }

    let currentLocation = await Location.getCurrentPositionAsync({});
    this.setState({
      currentLocation,
    });
  };

  _onMapLayout = () => {
    this.setState({ isMapReady: true });
  };

  _setRouteLocation = async () => {
    const {
      destinationAddress,
      destinationCoords,
      pickUpAddress,
      pickUpCoords,
    } = this.state;

    const locations = {
      destinationLocation: {
        destinationAddress,
        destinationCoords,
      },
      pickUpLocation: {
        pickUpAddress,
        pickUpCoords,
      },
    };

    // store the destination location
    await this.props.setLocations(locations);
    this._navigateTo("SetNewDestinationScreen", {
      from: "PickRouteLocationScreen",
    });
  };

  _getRouteLocationCoordinates = async (region) => {
    const { typeOfLocation } = this.state;
    const { latitude, longitude } = region;
    const routeLocationAddress = await geo.get(region);

    if (typeOfLocation === "pickUpAddress") {
      await this.setState({
        pickUpAddress: routeLocationAddress[0].formatted_address,
        pickUpCoords: { lat: latitude, lng: longitude },
        isMapOnDragging: false,
      });
    } else {
      await this.setState({
        destinationAddress: routeLocationAddress[0].formatted_address,
        destinationCoords: { lat: latitude, lng: longitude },
        isMapOnDragging: false,
      });
    }
  };

  _handleMapDragging = () => {
    this.setState({
      isMapOnDragging: true,
    });
  };

  render() {
    const {
      isMapOnDragging,
      currentLocation,
      pickUpAddress,
      destinationAddress,
      isMapReady,
      typeOfLocation,
    } = this.state;

    return (
      <Container style={styles.container}>
        {this.state.shouldMapViewBe ? (
          <>
            <View style={styles.mapWrapper}>
              <AppHeader back />
              {currentLocation && typeof currentLocation !== undefined ? (
                <MapView
                  provider="google"
                  style={styles.map}
                  customMapStyle={mapStyle}
                  minZoomLevel={9}
                  maxZoomLevel={17}
                  initialRegion={{
                    latitude: currentLocation.coords.latitude,
                    longitude: currentLocation.coords.longitude,
                    latitudeDelta: 0.015,
                    longitudeDelta: 0.015,
                  }}
                  onMapReady={this._onMapLayout}
                  onRegionChangeComplete={this._getRouteLocationCoordinates}
                  onPanDrag={this._handleMapDragging}
                >
                  {isMapReady &&
                    currentLocation &&
                    typeof currentLocation !== undefined && (
                      <Marker
                        coordinate={{
                          latitude: currentLocation.coords.latitude,
                          longitude: currentLocation.coords.longitude,
                        }}
                      >
                        <PositionMarker />
                      </Marker>
                    )}
                </MapView>
              ) : (
                <Spinner color="#f37121" />
              )}
              {isMapReady &&
                currentLocation &&
                typeof currentLocation !== undefined && (
                  <PositionMarker fixed pin />
                )}
            </View>
            <View style={styles.footer}>
              <H3 style={styles.startCatchPhrase}>
                {typeOfLocation === "pickUpAddress"
                  ? "Indulási pont megerőstése"
                  : "Úticél megerősítése"}
              </H3>
              <H2 style={styles.startTitle}>
                {typeOfLocation === "pickUpAddress"
                  ? pickUpAddress
                  : destinationAddress}
              </H2>
              <Button
                block
                primary
                disabled={isMapOnDragging}
                style={styles.confirmButton}
                onPress={this._setRouteLocation}
              >
                {!isMapOnDragging ? (
                  <Text>Megerősítés</Text>
                ) : (
                  <Spinner color="#ccc" />
                )}
              </Button>
            </View>
            {this.state.searchingMatchingRoute && (
              <View style={styles.loading}>
                <Spinner color="#f37121" />
              </View>
            )}
          </>
        ) : (
          <></>
        )}
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    position: "relative",
    justifyContent: "space-between",
    flex: 1,
  },
  header: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingVertical: 30,
    paddingHorizontal: 20,
    width: "100%",
    height: 100,
  },
  startTitle: {
    marginTop: 5,
  },
  startCatchPhrase: {
    color: "#999",
    paddingTop: 3,
    lineHeight: 20,
  },
  mapWrapper: {
    width: "100%",
    flex: 1,
  },
  map: {
    position: "absolute",
    width: "100%",
    height: "100%",
    zIndex: -1,
    top: 0,
    left: 0,
  },
  profileButton: {
    justifyContent: "center",
    alignItems: "center",
    width: 50,
    height: 50,
    borderWidth: 2,
    borderColor: "#ffffff",
    borderStyle: "solid",
  },
  confirmButton: {
    justifyContent: "center",
    marginTop: 20,
    borderColor: "#999",
    height: 50,
  },
  footer: {
    paddingVertical: 30,
    paddingHorizontal: 20,
    backgroundColor: "#ffffff",
    width: "100%",
    flexDirection: "column",
    justifyContent: "flex-start",
    height: "auto",
  },
  loading: {
    position: "absolute",
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: "center",
    justifyContent: "center",
  },
});

const mapStateToProps = ({ locations, user }) => ({
  ...locations,
  ...user,
});

const mapDispatchToProps = {
  setLocations,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PickRouteLocationScreen);
