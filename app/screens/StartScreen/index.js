// route planner map
import React, { PureComponent } from "react";
import { Platform, StyleSheet, View } from "react-native";
import { Container, Text, H2, H3, Button, Spinner } from "native-base";
import MapView, { Marker } from "react-native-maps";

import { connect } from "react-redux";
import { getUser } from "../../redux/actions/user";
import { setLocations } from "../../redux/actions/locations";

import Constants from "expo-constants";
import * as Location from "expo-location";
import * as Permissions from "expo-permissions";
import mapStyle from "../../utils/mapStyle";
import PositionMarker from "../../components/PositionMarker";
import { Feather, AntDesign } from "@expo/vector-icons";
import geo from "../../utils/geo";

class StartScreen extends PureComponent {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      isMapReady: false,
      errorMessage: "",
      currentLocation: null,
      pickUpAddress: "",
      pickUpCoords: null,
      destinationAddress: "",
      destinationCoords: null,
      shouldMapViewBe: true,
    };

    if (Platform.OS === "android" && !Constants.isDevice) {
      this.setState({
        errorMessage:
          "Oops, this will not work on Sketch in an Android emulator. Try it on your device!",
      });
    } else {
      this._getLocationAsync();
    }
  }

  componentDidMount = async () => {
    await this.props.getUser();

    this.willFocusListener = this.props.navigation.addListener(
      "willFocus",
      () => {
        this.setState({ shouldMapViewBe: true });
      }
    );

    this.blurListener = this.props.navigation.addListener("didBlur", () => {
      this.setState({ shouldMapViewBe: false });
    });
  };

  componentWillUnmount = () => {
    this.willFocusListener.remove();
    this.blurListener.remove();
  };

  _navigateTo = (screen, params = {}) => {
    this.props.navigation.navigate(screen, params);
  };

  _getLocationAsync = async () => {
    let { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== "granted") {
      this.setState({
        errorMessage: "Permission to access location was denied",
      });
      return;
    }

    let currentLocation = await Location.getCurrentPositionAsync({});

    const { latitude, longitude } = currentLocation.coords;

    const pickUpAddress = await geo.get({
      latitude,
      longitude,
      latitudeDelta: 0.05,
      longitudeDelta: 0.05,
    });

    await this.setState({
      currentLocation,
      pickUpCoords: { lat: latitude, lng: longitude },
      pickUpAddress: pickUpAddress[0].formatted_address,
    });
  };

  _onMapLayout = () => {
    this.setState({
      isMapReady: true,
    });
  };

  _setPickUpLocation = async () => {
    const {
      pickUpAddress,
      pickUpCoords,
      destinationAddress,
      destinationCoords,
    } = this.state;

    const locations = {
      pickUpLocation: {
        pickUpAddress,
        pickUpCoords,
      },
      destinationLocation: {
        destinationAddress,
        destinationCoords,
      },
    };

    await this.props.setLocations(locations);

    this._navigateTo("SetNewDestinationScreen");
  };

  render() {
    const { currentLocation, isMapReady } = this.state;

    return (
      <Container style={styles.container}>
        {this.state.shouldMapViewBe ? (
          <>
            <View style={styles.mapWrapper}>
              <View style={styles.header}>
                <Button
                  primary
                  small
                  rounded
                  noShadow
                  onPress={() => this._navigateTo("ProfileScreen")}
                  style={styles.profileButton}
                >
                  <AntDesign name="user" size={22} color="#ffffff" />
                </Button>
              </View>
              {currentLocation && typeof currentLocation !== undefined ? (
                <MapView
                  provider="google"
                  style={styles.map}
                  customMapStyle={mapStyle}
                  minZoomLevel={9}
                  maxZoomLevel={17}
                  initialRegion={{
                    latitude: currentLocation.coords.latitude,
                    longitude: currentLocation.coords.longitude,
                    latitudeDelta: 0.015,
                    longitudeDelta: 0.015,
                  }}
                  onMapReady={this._onMapLayout}
                >
                  {isMapReady &&
                    currentLocation &&
                    typeof currentLocation !== undefined && (
                      <Marker
                        coordinate={{
                          latitude: currentLocation.coords.latitude,
                          longitude: currentLocation.coords.longitude,
                        }}
                      >
                        <PositionMarker />
                      </Marker>
                    )}
                </MapView>
              ) : (
                <Spinner color="#f37121" />
              )}
            </View>
            <View style={styles.footer}>
              <H3 style={styles.startCatchPhrase}>
                Taxizz olcsóbban, mint bármikor.
              </H3>
              <H2 style={styles.startTitle}>Merre mész?</H2>
              <Button
                bordered
                block
                dark
                iconRight
                disabled={currentLocation ? false : true}
                style={styles.startButton}
                onPress={this._setPickUpLocation}
              >
                {currentLocation ? (
                  <>
                    <Text>Úticél megadása</Text>
                    <Feather
                      name="arrow-right"
                      size={24}
                      color="#413B38"
                      style={styles.startButtonIcon}
                    />
                  </>
                ) : (
                  <>
                    <Text />
                    <Spinner color="#ccc" />
                    <Text />
                  </>
                )}
              </Button>
            </View>
          </>
        ) : (
          <></>
        )}
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    position: "relative",
    justifyContent: "space-between",
    flex: 1,
  },
  header: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingVertical: 30,
    paddingHorizontal: 20,
    width: "100%",
    height: 100,
  },
  startTitle: {
    marginTop: 5,
  },
  startCatchPhrase: {
    color: "#999",
  },
  mapWrapper: {
    width: "100%",
    flex: 1,
  },
  map: {
    position: "absolute",
    width: "100%",
    height: "100%",
    zIndex: -1,
    top: 0,
    left: 0,
  },
  profileButton: {
    justifyContent: "center",
    alignItems: "center",
    width: 50,
    height: 50,
    borderWidth: 2,
    borderColor: "#ffffff",
    borderStyle: "solid",
  },
  startButton: {
    justifyContent: "space-between",
    marginTop: 20,
    borderColor: "#999",
    height: 50,
  },
  startButtonIcon: {
    marginRight: 10,
  },
  footer: {
    paddingVertical: 30,
    paddingHorizontal: 20,
    backgroundColor: "#ffffff",
    width: "100%",
    flexDirection: "column",
    justifyContent: "flex-start",
    height: "auto",
  },
});

const mapStateToProps = ({ user, locations }) => ({
  ...user,
  ...locations,
});

const mapDispatchToProps = {
  getUser,
  setLocations,
};

export default connect(mapStateToProps, mapDispatchToProps)(StartScreen);
