import React, { PureComponent } from "react";
import { getAverage } from "../../utils/helpers.js";
import { View, StyleSheet, FlatList, Modal, StatusBar } from "react-native";
import * as WebBrowser from "expo-web-browser";
import { connect } from "react-redux";
import {
  Container,
  Content,
  Text,
  Button,
  List,
  ListItem,
  Thumbnail,
} from "native-base";

import AuthService from "../../services/auth";
import UserService from "../../services/user";
import AppHeader from "../../components/AppHeader";
import { _readAsyncStorage } from "../../utils/helpers";

import Rating from "../../components/Rating";
import ProfileListItem from "../../components/ProfileListItem";

class Profile extends PureComponent {
  static navigationOptions = {
    header: null,
  };

  state = {
    isLogoutModalVisible: false,
    isProfileDeleteModalVisible: false,
  };

  componentWillMount() {
    StatusBar.setHidden(true);
  }

  componentWillUnmount() {
    StatusBar.setHidden(false);
  }

  _toggleLogoutModal = () => {
    this.setState({
      isLogoutModalVisible: !this.state.isLogoutModalVisible,
    });
  };

  _toggleDeleteProfileModal = () => {
    this.setState({
      isProfileDeleteModalVisible: !this.state.isProfileDeleteModalVisible,
    });
  };

  _triggerWebBrowserByLink = (link) => {
    link && WebBrowser.openBrowserAsync(link);
  };

  _signOut = async () => {
    try {
      await AuthService.signOut();
      this.props.navigation.navigate("Auth");
    } catch (error) {
      alert(error);
    }
  };

  _navigateTo = (screen, props = {}) => {
    this.props.navigation.navigate(screen, props);
  };

  _deleteUser = async () => {
    try {
      await UserService.deleteUser();
      this.props.navigation.navigate("Auth");
    } catch (error) {
      alert(error);
    }
  };

  _keyExtractor = (item, index) => `list-item-${index}`;

  _renderAddressListItem = (item) => {
    return (
      <ListItem
        noIndent
        style={styles.listItem}
        onPress={() =>
          this._navigateTo("EditAddressScreen", { key: item.name })
        }
      >
        <ProfileListItem
          firstIcon={item.name === "home" ? "md-home" : "ios-briefcase"}
          lastIcon="edit"
          text={item.label}
          note={item.address}
        />
      </ListItem>
    );
  };

  render() {
    const { isLogoutModalVisible, isProfileDeleteModalVisible } = this.state;

    const { picture, name, addresses, ratings } = this.props.user;

    const avgRating = ratings && ratings.length ? getAverage(ratings) : 5;

    const home =
      this.props.user && addresses.length
        ? addresses.filter((value) => value.name === "home")
        : "";

    const office =
      this.props.user && addresses.length
        ? addresses.filter((value) => value.name === "office")
        : "";

    return (
      <>
        <Container style={styles.container}>
          <AppHeader title="Profil" />
          <Content>
            {this.props.user ? (
              <>
                <View style={styles.profileWrapper}>
                  <View>
                    <Thumbnail source={{ uri: picture }} />
                  </View>
                  <View
                    style={{
                      paddingLeft: 15,
                      flexDirection: "column",
                      justifyContent: "center",
                    }}
                  >
                    <Text>{name}</Text>
                    <Rating fixed stars={avgRating} />
                  </View>
                </View>
                <List>
                  <ListItem itemDivider style={styles.divider}>
                    <Text uppercase={true} style={styles.dividerText}>
                      Személyes adatok
                    </Text>
                  </ListItem>

                  <ListItem
                    noIndent
                    onPress={() => this._navigateTo("EditEmailScreen")}
                  >
                    <ProfileListItem
                      firstIcon="md-mail"
                      lastIcon="edit"
                      text="Email cím"
                    />
                  </ListItem>

                  <ListItem
                    noIndent
                    onPress={() => this._navigateTo("EditPaymentScreen")}
                  >
                    <ProfileListItem
                      firstIcon="ios-card"
                      lastIcon="chevron-right"
                      text="Fizetési beállítások"
                    />
                  </ListItem>
                  <ListItem itemDivider style={styles.divider}>
                    <Text uppercase={true} style={styles.dividerText}>
                      Rögzített címek
                    </Text>
                  </ListItem>
                </List>

                <FlatList
                  data={[...office, ...home]}
                  renderItem={({ item }) => this._renderAddressListItem(item)}
                  keyExtractor={this._keyExtractor}
                />

                <List>
                  <ListItem itemDivider style={styles.divider}>
                    <Text uppercase={true} style={styles.dividerText}>
                      Egyéb
                    </Text>
                  </ListItem>

                  <ListItem noIndent onPress={this._toggleLogoutModal}>
                    <ProfileListItem firstIcon="md-exit" text="Kijelentkezés" />
                  </ListItem>

                  <ListItem
                    noIndent
                    onPress={() =>
                      this._triggerWebBrowserByLink(
                        "https://getmandy.app/legal/terms.pdf"
                      )
                    }
                  >
                    <ProfileListItem firstIcon="ios-paper" text="ÁSZF" />
                  </ListItem>

                  <ListItem
                    noIndent
                    onPress={() =>
                      this._triggerWebBrowserByLink(
                        "https://getmandy.app/legal/privacy.pdf"
                      )
                    }
                  >
                    <ProfileListItem
                      firstIcon="md-globe"
                      text="Adatvédelmi nyilatkozat"
                    />
                  </ListItem>

                  <ListItem noIndent onPress={this._toggleDeleteProfileModal}>
                    <ProfileListItem
                      firstIcon="md-trash"
                      text="Profil törlése"
                    />
                  </ListItem>
                </List>
              </>
            ) : null}
          </Content>
        </Container>

        {/* LOGOUT MODAL */}
        <Modal
          animationType="fade"
          transparent
          hardwareAccelerated
          presentationStyle="overFullScreen"
          visible={isLogoutModalVisible}
        >
          <View style={styles.screenModal}>
            <View style={styles.screenModalView}>
              <Button onPress={this._signOut} style={styles.screenModalButton}>
                <Text style={styles.screenModalButtonText}>Kijelentkezés</Text>
              </Button>
            </View>
            <View style={styles.screenModalView}>
              <Button
                onPress={this._toggleLogoutModal}
                style={styles.screenModalCancelButton}
              >
                <Text style={styles.screenModalButtonText}>Mégse</Text>
              </Button>
            </View>
          </View>
        </Modal>

        {/* PROFIL TÖRLÉSE MODAL */}
        <Modal
          animationType="fade"
          transparent
          hardwareAccelerated
          presentationStyle="overFullScreen"
          visible={isProfileDeleteModalVisible}
        >
          <View style={styles.screenModal}>
            <View style={styles.screenModalView}>
              <Button
                onPress={this._deleteUser}
                style={styles.screenModalButton}
              >
                <Text style={styles.screenModalButtonText}>Profil törlése</Text>
              </Button>
            </View>
            <View style={styles.screenModalView}>
              <Button
                onPress={this._toggleDeleteProfileModal}
                style={styles.screenModalCancelButton}
              >
                <Text style={styles.screenModalButtonText}>Mégse</Text>
              </Button>
            </View>
          </View>
        </Modal>
      </>
    );
  }
}

const styles = StyleSheet.create({
  profileWrapper: {
    flexDirection: "row",
    paddingBottom: 35,
    paddingLeft: 20,
  },
  divider: {
    paddingTop: 30,
    paddingBottom: 15,
    paddingLeft: 20,
    borderTopWidth: 1,
    borderTopColor: "#ddd",
    borderBottomWidth: 1,
    borderBottomColor: "#ddd",
  },
  dividerText: {
    backgroundColor: "#f3f3f3",
    fontSize: 12,
    fontWeight: "bold",
  },
  profileSection: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginLeft: 0,
    paddingLeft: 20,
    paddingVertical: 10,
  },
  profileSectionTitle: {
    marginLeft: 15,
  },
  profileIcons: {
    opacity: 0.3,
  },
  profileIconsInvisible: {
    opacity: 0,
  },
  userAddress: {
    fontSize: 14,
    opacity: 0.3,
  },
  listBox: {
    borderTopWidth: 1,
    borderTopColor: "#F3F3F3",
    borderStyle: "solid",
    marginTop: 10,
  },
  listItem: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginHorizontal: 0,
    paddingHorizontal: 0,
    paddingVertical: 10,
    borderStyle: "solid",
    borderBottomWidth: 1,
    borderBottomColor: "#F3F3F3",
  },
  listItemLeft: {
    width: 30,
    alignItems: "flex-start",
    justifyContent: "center",
  },
  listItemBody: {
    flex: 3,
    justifyContent: "flex-start",
  },
  listItemBodyButton: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "space-between",
  },
  listItemBodyButtonText: { width: "100%", textAlign: "left", color: "#000" },
  listItemNoteText: {
    width: "100%",
    textAlign: "left",
  },
  listItemPickAddressButtonText: {
    display: "flex",
    width: "100%",
    textAlign: "left",
    justifyContent: "flex-start",
    flex: 1,
    color: "#413B38",
  },
  listItemRight: {
    width: 40,
    justifyContent: "center",
  },
  screenModal: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "flex-end",
    padding: 20,
    backgroundColor: "rgba(0,0,0,0.5)",
  },
  screenModalButton: {
    width: "100%",
    backgroundColor: "#ccc",
    justifyContent: "center",
  },
  screenModalCancelButton: {
    marginTop: 20,
    width: "100%",
    backgroundColor: "#fff",
    justifyContent: "center",
  },
  screenModalButtonText: {
    color: "#007AFF",
  },
});

const mapStateToProps = ({ user }) => ({
  ...user,
});
export default connect(mapStateToProps)(Profile);
