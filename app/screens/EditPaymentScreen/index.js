import React, { PureComponent } from 'react';
import { View, StyleSheet } from 'react-native';
import { TextInputMask } from 'react-native-masked-text';
import { Container, Content, Item, Input, Label } from 'native-base';
import AppHeader from '../../components/AppHeader';
import { _readAsyncStorage } from '../../utils/helpers';

class EditPaymentScreen extends PureComponent {
  static navigationOptions = {
    header: null,
  };

  state = {
    cardNumber: '',
    cardName: '',
    expirationDate: '',
    ccv: '',
    typingStarted: false,
  };

  render() {
    return (
      <Container style={styles.container}>
        <AppHeader title="Fizetési beállítások" />
        <Content scrollEnabled={false}>
          <View style={styles.inputWrapper}>
            <Label style={styles.label}>Kártyaszám</Label>
            <Item regular>
              <TextInputMask
                type={'credit-card'}
                options={{
                  obfuscated: false,
                }}
                value={this.state.cardNumber}
                onChangeText={text => {
                  this.setState({
                    cardNumber: text,
                  });
                }}
                customTextInput={Input}
              />
            </Item>
            <Label style={styles.label}>Kártyaszán szereplő név</Label>
            <Item regular>
              <Input
                onChangeText={text => {
                  this.setState({
                    cardName: text,
                    typingStarted: true,
                  });
                }}
                style={{ color: '#E74C3C' }}
                autoCorrect={false}
              />
            </Item>
            <View style={{ flexDirection: 'row', width: '70%' }}>
              <View style={{ flex: 2, paddingRight: 20 }}>
                <Label style={styles.label}>Lejárati idő</Label>
                <Item regular>
                  <TextInputMask
                    type={'datetime'}
                    options={{
                      format: 'MM/YY',
                    }}
                    value={this.state.expirationDate}
                    onChangeText={text => {
                      this.setState({
                        expirationDate: text,
                        typingStarted: true,
                      });
                    }}
                    customTextInput={Input}
                  />
                </Item>
              </View>
              <View style={{ flex: 1 }}>
                <Label style={styles.label}>CCV</Label>
                <Item regular>
                  <TextInputMask
                    type={'only-numbers'}
                    customTextInput={Input}
                    value={this.state.ccv}
                    onChangeText={text => {
                      if (text.split('').length <= 3) {
                        this.setState({
                          ccv: text,
                          typingStarted: true,
                        });
                      }
                    }}
                  />
                </Item>
              </View>
            </View>
          </View>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  inputWrapper: {
    paddingHorizontal: 20,
  },
  itemActive: {
    borderColor: '#413B38',
  },
  label: {
    fontSize: 14,
    color: '#413B38',
    opacity: 0.3,
    marginBottom: 7,
    marginTop: 20,
  },
});

export default EditPaymentScreen;
