import { combineReducers } from 'redux';
import me from './me';
import user from './user';
import locations from './locations';
import route from './route';
import trip from './trip';

const rootReducer = combineReducers({
  me,
  user,
  locations,
  route,
  trip,
});

export default rootReducer;
