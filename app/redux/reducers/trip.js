import {
  SET_INITIAL_TRIP_PENDING,
  SET_INITIAL_TRIP_SUCCESS,
  SET_INITIAL_TRIP_FAILED
} from '../actionTypes';

export default function(state = {}, { type = '', payload = {} }) {
  switch (type) {
    case SET_INITIAL_TRIP_PENDING: {
      return {
        ...state,
      };
    }

    case SET_INITIAL_TRIP_SUCCESS: {
      return {
        ...state,
        trip: payload,
      };
    }

    case SET_INITIAL_TRIP_FAILED: {
      return {
        ...state,
        route: null,
      };
    }

    default:
      return state;
  }
}