import {
  SET_INITIAL_ROUTE_PENDING,
  SET_INITIAL_ROUTE_SUCCESS,
  SET_INITIAL_ROUTE_FAILED,
  DELETE_ROUTE_PENDING,
  DELETE_ROUTE_SUCCESS,
  DELETE_ROUTE_FAILED
} from '../actionTypes';

export default function(state = {}, { type = '', payload = {} }) {
  switch (type) {
    case SET_INITIAL_ROUTE_PENDING: {
      return {
        ...state,
      };
    }

    case SET_INITIAL_ROUTE_SUCCESS: {
      return {
        ...state,
        route: payload,
      };
    }

    case SET_INITIAL_ROUTE_FAILED: {
      return {
        ...state,
        route: null,
      };
    }

    case DELETE_ROUTE_PENDING: {
      return {
        ...state,
      };
    }

    case DELETE_ROUTE_SUCCESS: {
      return {
        ...state,
        route: payload,
      };
    }

    case DELETE_ROUTE_FAILED: {
      return {
        ...state,
        route: null,
      };
    }

    default:
      return state;
  }
}
