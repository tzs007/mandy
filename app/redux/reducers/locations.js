import {
  SET_LOCATIONS_PENDING,
  SET_LOCATIONS_SUCCESS,
  SET_LOCATIONS_FAILED,
} from '../actionTypes';

const INITIAL_STATE = {
  locations: {
    pickUpLocation: {
      pickUpAddress: '',
      pickUpCoords: null,
    },
    destinationLocation: {
      destinationAddress: '',
      destinationCoords: null,
    },
  },
};

export default function(state = INITIAL_STATE, { type = '', payload = {} }) {
  switch (type) {
    case SET_LOCATIONS_PENDING: {
      return {
        ...state,
      };
    }

    case SET_LOCATIONS_SUCCESS: {
      return {
        ...state,
        locations: payload,
      };
    }

    case SET_LOCATIONS_FAILED: {
      return {
        ...state,
        locations: null,
      };
    }

    default:
      return state;
  }
}
