import { GET_ME_PENDING, GET_ME_SUCCESS, GET_ME_FAILED } from '../actionTypes';

export default function(state = {}, { type = '', payload = {} }) {
  switch (type) {
    case GET_ME_PENDING: {
      return {
        ...state,
      };
    }

    case GET_ME_SUCCESS: {
      return {
        ...state,
        me: payload,
      };
    }

    case GET_ME_FAILED: {
      return {
        ...state,
        me: null,
      };
    }

    default:
      return state;
  }
}
