import {
  GET_USER_PENDING,
  GET_USER_SUCCESS,
  GET_USER_FAILED,
  GET_USERS_BY_FIELD_PENDING,
  GET_USERS_BY_FIELD_SUCCESS,
  GET_USERS_BY_FIELD_FAILED,
  UPDATE_USER_PENDING,
  UPDATE_USER_SUCCESS,
  UPDATE_USER_FAILED,
} from '../actionTypes';

export default function(state = {}, { type = '', payload = {} }) {
  switch (type) {
    // GET USER
    case GET_USER_PENDING: {
      return {
        ...state,
      };
    }

    case GET_USER_SUCCESS: {
      return {
        ...state,
        user: payload,
      };
    }

    case GET_USER_FAILED: {
      return {
        ...state,
        user: null,
      };
    }

    // GET USER BY FIELD
    case GET_USERS_BY_FIELD_PENDING: {
      return {
        ...state,
      };
    }

    case GET_USERS_BY_FIELD_SUCCESS: {
      return {
        ...state,
        users: payload,
      };
    }

    case GET_USERS_BY_FIELD_FAILED: {
      return {
        ...state,
        users: null,
      };
    }

    // UPDATE USER
    case UPDATE_USER_PENDING: {
      return {
        ...state,
      };
    }

    case UPDATE_USER_SUCCESS: {
      return {
        ...state,
        user: payload,
      };
    }

    case UPDATE_USER_FAILED: {
      return {
        ...state,
        user: null,
      };
    }

    default:
      return state;
  }
}
