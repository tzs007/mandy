export const GET_ME_PENDING = 'get_me_pending';
export const GET_ME_SUCCESS = 'get_me_success';
export const GET_ME_FAILED = 'get_me_failed';

export const GET_USER_PENDING = 'get_user_pending';
export const GET_USER_SUCCESS = 'get_user_success';
export const GET_USER_FAILED = 'get_user_failed';

export const GET_USERS_BY_FIELD_PENDING = 'get_users_by_field_pending';
export const GET_USERS_BY_FIELD_SUCCESS = 'get_users_by_field_success';
export const GET_USERS_BY_FIELD_FAILED = 'get_users_by_field_failed';

export const UPDATE_USER_PENDING = 'update_user_pending';
export const UPDATE_USER_SUCCESS = 'update_user_success';
export const UPDATE_USER_FAILED = 'update_user_failed';

export const SET_LOCATIONS_PENDING = 'set_locations_pending';
export const SET_LOCATIONS_SUCCESS = 'set_locations_success';
export const SET_LOCATIONS_FAILED = 'set_locations_failed';

export const SET_INITIAL_ROUTE_PENDING = 'set_initial_route_pending';
export const SET_INITIAL_ROUTE_SUCCESS = 'set_initial_route_success';
export const SET_INITIAL_ROUTE_FAILED = 'set_initial_route_failed';

export const DELETE_ROUTE_PENDING = 'delete_route_pending';
export const DELETE_ROUTE_SUCCESS = 'delete_route_success';
export const DELETE_ROUTE_FAILED = 'delete_route_failed';

export const SET_INITIAL_TRIP_PENDING = 'set_initial_trip_pending';
export const SET_INITIAL_TRIP_SUCCESS = 'set_initial_trip_success';
export const SET_INITIAL_TRIP_FAILED = 'set_initial_trip_failed';
