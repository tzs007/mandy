import {
  GET_USER_PENDING,
  GET_USER_SUCCESS,
  GET_USER_FAILED,
  GET_USERS_BY_FIELD_PENDING,
  GET_USERS_BY_FIELD_SUCCESS,
  GET_USERS_BY_FIELD_FAILED,
  UPDATE_USER_PENDING,
  UPDATE_USER_SUCCESS,
  UPDATE_USER_FAILED,
} from '../actionTypes';

import UserService from '../../services/user';
import { _readAsyncStorage } from '../../utils/helpers';

// GET_USER_PENDING
export const getUserPending = () => ({
  type: GET_USER_PENDING,
  payload: {},
});

// GET_USER_SUCCESS
export const getUserSuccess = user => ({
  type: GET_USER_SUCCESS,
  payload: user,
});

// GET_USER_FAILED
export const getUserFailed = error => ({
  type: GET_USER_FAILED,
  payload: error,
});

// GET_USER_BY_ID
export const getUser = () => async dispatch => {
  try {
    dispatch(getUserPending());
    const userId = await _readAsyncStorage('user');
    const user = await UserService.getUser(userId);
    dispatch(getUserSuccess(user));
  } catch (error) {
    dispatch(getUserFailed(error));
  }
};

//
// GET_USERS_BY_FIELD_PENDING
export const getUsersByFieldPending = () => ({
  type: GET_USERS_BY_FIELD_PENDING,
  payload: {},
});

// GET_USERS_BY_FIELD_SUCCESS
export const getUsersByFieldSuccess = users => ({
  type: GET_USERS_BY_FIELD_SUCCESS,
  payload: users,
});

// GET_USERS_BY_FIELD_FAILED
export const getUsersByFieldFailed = error => ({
  type: GET_USERS_BY_FIELD_FAILED,
  payload: error,
});

// GET_USERS_BY_FIELD
export const getUsersByField = (key, field) => async dispatch => {
  try {
    dispatch(getUsersByFieldPending());
    const users = await UserService.getUserBy(key, field);
    dispatch(getUsersByFieldSuccess(users));
  } catch (error) {
    dispatch(getUsersByFieldFailed(error));
  }
};

//
// UPDATE_USER_PENDING
export const updateUserPending = () => ({
  type: UPDATE_USER_PENDING,
  payload: {},
});

// UPDATE_USER_SUCCESS
export const updateUserSuccess = user => ({
  type: UPDATE_USER_SUCCESS,
  payload: user,
});

// UPDATE_USER_FAILED
export const updateUserFailed = error => ({
  type: UPDATE_USER_FAILED,
  payload: error,
});

// UPDATE_USER_BY_ID
export const updateUser = (userId, object) => async dispatch => {
  try {
    dispatch(updateUserPending());
    await UserService.updateUser(userId, object);
    const user = await UserService.getUser(object);
    dispatch(updateUserSuccess(user));
  } catch (error) {
    dispatch(updateUserFailed(error));
  }
};
