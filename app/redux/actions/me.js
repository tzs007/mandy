import { auth } from '../../utils/firebase';
import { GET_ME_PENDING, GET_ME_SUCCESS, GET_ME_FAILED } from '../actionTypes';

// GET_USERS_PENDING
export const getMePending = () => ({
  type: GET_ME_PENDING,
  payload: {},
});

// GET_ME_SUCCESS
export const getMeSuccess = me => ({
  type: GET_ME_SUCCESS,
  payload: me,
});

// GET_Me_FAIL
export const getMeFailed = error => ({
  type: GET_ME_FAILED,
  payload: error,
});

// GET_Me
export const getMe = () => async dispatch => {
  try {
    dispatch(getMePending());
    const me = await auth.currentUser();
    dispatch(getMeSuccess(me));
  } catch (error) {
    dispatch(getMeFailed(error));
  }
};
