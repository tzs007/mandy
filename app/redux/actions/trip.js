import { createInitialTrip, getTrip, deleteIntermediateStop } from '../../services/trip';

import {
  SET_INITIAL_TRIP_PENDING,
  SET_INITIAL_TRIP_SUCCESS,
  SET_INITIAL_TRIP_FAILED
} from '../actionTypes';

// SET_INITIAL_TRIP_PENDING
export const setInitialTripPending = () => ({
  type: SET_INITIAL_TRIP_PENDING,
  payload: {}
});

// SET_INITIAL_TRIP_SUCCESS
export const setInitialTripSuccess = trip => ({
  type: SET_INITIAL_TRIP_SUCCESS,
  payload: trip
});

// SET_INITIAL_TRIP_FAILED
export const setInitialTripFailed = error => ({
  type: SET_INITIAL_TRIP_FAILED,
  payload: error
});

// SET_INITIAL_TRIP
export const setInitialTrip = trip => async dispatch => {
  try {
    dispatch(setInitialTripPending());
    const newTrip = await createInitialTrip(trip);
    await dispatch(setInitialTripSuccess(newTrip));
    return newTrip.id;
  } catch (error) {
    console.log(error)
    dispatch(setInitialTripFailed(error));
  }
};

export const setExistingTrip = tripId => async dispatch => {
  try {
    dispatch(setInitialTripPending());
    const trip = await getTrip(tripId);
    trip.id = tripId;
    await dispatch(setInitialTripSuccess(trip));
  } catch (error) {
    console.log(error);
    dispatch(setInitialTripFailed(error));
  }
};
