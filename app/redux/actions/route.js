import RouteService from '../../services/route';
import {deleteRoute as deleteRouteFromDB} from '../../services/route';

import {
  SET_INITIAL_ROUTE_PENDING,
  SET_INITIAL_ROUTE_SUCCESS,
  SET_INITIAL_ROUTE_FAILED,
  DELETE_ROUTE_PENDING,
  DELETE_ROUTE_SUCCESS,
  DELETE_ROUTE_FAILED
} from '../actionTypes';

// Store initial route coordinates in store and firebase
// =====================================================
// SET_INITIAL_ROUTE_PENDING
export const setInitialRoutePending = () => ({
  type: SET_INITIAL_ROUTE_PENDING,
  payload: {},
});

// SET_INITIAL_ROUTE_SUCCESS
export const setInitialRouteSuccess = route => ({
  type: SET_INITIAL_ROUTE_SUCCESS,
  payload: route,
});

// SET_INITIAL_ROUTE_FAILED
export const setInitialRouteFailed = error => ({
  type: SET_INITIAL_ROUTE_FAILED,
  payload: error,
});

export const deleteRoutePending = ()=>({
  type: DELETE_ROUTE_PENDING,
  payload: {}
});

export const deleteRouteSuccess = ()=>({
  type: DELETE_ROUTE_SUCCESS,
  payload: {}
});

export const deleteRouteFailed = (error)=>({
  type: DELETE_ROUTE_FAILED,
  payload: error
});

// SET_INITIAL_ROUTE
export const setInitialRoute = route => async dispatch => {
  try {
    dispatch(setInitialRoutePending());
    await dispatch(setInitialRouteSuccess(route));
    return await RouteService.createInitialRoute(route);
  } catch (error) {
    dispatch(setInitialRouteFailed(error));
  }
};

export const deleteRoute = routeId => async dispatch =>{
  try{
    dispatch(setInitialRoutePending());
    deleteRouteFromDB(routeId);
    dispatch(setInitialRouteSuccess());
  }catch(error){
    console.log(error);
    dispatch(setInitialRouteFailed(error));
  }
}
