import {
  SET_LOCATIONS_PENDING,
  SET_LOCATIONS_SUCCESS,
  SET_LOCATIONS_FAILED,
} from '../actionTypes';

// Set full location object in store
// ======================================
// SET_LOCATION_PENDING
export const setLocationsPending = () => ({
  type: SET_LOCATIONS_PENDING,
  payload: {},
});

// SET_LOCATION_SUCCESS
export const setLocationsSuccess = locations => ({
  type: SET_LOCATIONS_SUCCESS,
  payload: locations,
});

//  SET_LOCATION_FAILED
export const setLocationsFailed = error => ({
  type: SET_LOCATIONS_FAILED,
  payload: error,
});

// SET_LOCATION
export const setLocations = locations => async dispatch => {
  try {
    dispatch(setLocationsPending());
    await dispatch(setLocationsSuccess(locations));
  } catch (error) {
    dispatch(setLocationsFailed(error));
  }
};
