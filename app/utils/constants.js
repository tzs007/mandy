// Firebase
export const FIREBASE_APIKEY = "AIzaSyB0DUtgQsOOsuz4nvaHYOLbQJgIb4Xdf70";
export const FIREBASE_AUTH_DOMAIN = "mandy-app-ikontent.firebaseapp.com";
export const FIREBASE_DATABASE_URL =
  "https://mandy-app-ikontent.firebaseio.com";
export const FIREBASE_PROJECT_ID = "mandy-app-ikontent";
export const FIREBASE_STORAGE_BUCKET = "mandy-app-ikontent.appspot.com";
export const FIREBASE_MESSAGING_SENDER_ID = "725319137388";

// Devices
export const ANDROID =
  "1061903660735-qs1dhh40fk3ams17c83m0vtg0u86que0.apps.googleusercontent.com";
export const IOS =
  "1061903660735-lmsht59nvtuufij51cq271mg90k24g71.apps.googleusercontent.com";
export const IOS_STANDALONE =
  "1061903660735-fh87f8lgohfkb4qpnk1cpi9f2aclcsln.apps.googleusercontent.com";

// APIs
// facebook
export const FACEBOOK_APP_ID = "451828468985820";

// google
export const GOOGLE_MAP_API = "https://maps.googleapis.com/maps/api/js";
export const GOOGLE_MAP_GEOCODE_API =
  "https://maps.google.com/maps/api/geocode/json?language=hu&region=hu";
export const GOOGLE_MAP_DISTANCE_API =
  "https://maps.google.com/maps/api/directions/json";
export const GOOGLE_MAP_APIKEY = "AIzaSyCJLPA3rFxEQtFG-t1fCcd-2Bd1xfQBG4w";
