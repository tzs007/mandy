import * as Sentry from "sentry-expo";
import api from "./api";
import {
  GOOGLE_MAP_DISTANCE_API,
  GOOGLE_MAP_GEOCODE_API,
  GOOGLE_MAP_APIKEY,
} from "./constants";

class Geo {
  get = async (payload) => {
    let input = null;

    if (typeof payload === "object") {
      input = {
        latlng: `${payload.latitude},${payload.longitude}`,
      };
    } else {
      input = {
        address: payload,
      };
    }

    try {
      const response = await api.get(GOOGLE_MAP_GEOCODE_API, {
        key: GOOGLE_MAP_APIKEY,
        ...input,
      });

      if (response && response.status === 200) {
        const { results } = response.data;
        return results;
      } else {
        Sentry.captureException(error, {
          message:
            "Error occurred get address/coordinates from Google Maps API!",
        });
        console.log(response.status);
      }
    } catch (error) {
      Sentry.captureException(error, {
        message: "Error occurred get address/coordinates from Google Maps API!",
      });
    }
  };

  getDistance = async (payload) => {
    let input = null;
    const { origin, destination, mode } = payload;

    input = {
      origin: `${origin.latitude},${origin.longitude}`,
      destination: `${destination.latitude},${destination.longitude}`,
      mode: mode,
    };

    try {
      const response = await api.get(GOOGLE_MAP_DISTANCE_API, {
        key: GOOGLE_MAP_APIKEY,
        ...input,
      });

      if (response && response.status === 200) {
        const { distance, duration } = response.data.routes[0].legs[0];
        return { time: duration.value, distance: distance.value };
      } else {
        return response.status;
      }
    } catch (error) {
      Sentry.captureException(error, {
        message: "Error occurred get distance from Google Maps API!",
      });
    }
  };
}

export default new Geo();
