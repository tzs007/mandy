import { AsyncStorage, Alert } from 'react-native';
import axios from 'axios';
// import { APIHOST } from 'react-native-dotenv'

export const _isMatchingRoute = async (
  userId,
  pickupCoords,
  destinationCoords,
) => {
  if (userId && pickupCoords && destinationCoords) {
    const result = await axios.get(
      `https://us-central1-mandy-app-ikontent.cloudfunctions.net/api/search-route?id=${userId}` +
        `&pickUpLocation=[${pickupCoords.lat},${
          pickupCoords.lng
        }]&destination=[${destinationCoords.lat},${destinationCoords.lng}]`,
    );
    return typeof result.data.result.length === 'number'
      ? false
      : result.data.result;
  }
};

// asyncStorage helpers

export const _writeAsyncStorage = async (
  asyncStorageKey,
  asyncStorageValue,
) => {
  // console.log('askey', asyncStorageKey);
  // console.log('asval', asyncStorageValue);
  try {
    await AsyncStorage.setItem(`@mandy:${asyncStorageKey}`, asyncStorageValue);
  } catch (error) {
    console.log(error);
    return false;
  }
};

export const _readAsyncStorage = async asyncStorageKey => {
  try {
    const value = await AsyncStorage.getItem(`@mandy:${asyncStorageKey}`);
    if (value !== null) {
      return value;
    }
  } catch (error) {
    console.log(error);
    return false;
  }
};

export const _removeAsyncStorage = async asyncStorageKey => {
  try {
    await AsyncStorage.removeItem(asyncStorageKey);
    return true;
  } catch (error) {
    console.log(error);
    return false;
  }
};

export const _clearAsyncStorage = async () => {
  try {
    await AsyncStorage.clear();
    return true;
  } catch (error) {
    console.log(error);
    return false;
  }
};

// Calculate the distance between two position in km unit.
export const calcDistance = (position1, position2) => {
  if (Array.isArray(position1) && Array.isArray(position2)) {
    let lat1 = position1[0];
    let lat2 = position2[0];
    let lon1 = position1[1];
    let lon2 = position2[1];

    if (lat1 == lat2 && lon1 == lon2) {
      return 0;
    } else {
      const radlat1 = (Math.PI * lat1) / 180;
      const radlat2 = (Math.PI * lat2) / 180;
      const theta = lon1 - lon2;
      const radtheta = (Math.PI * theta) / 180;
      let dist =
        Math.sin(radlat1) * Math.sin(radlat2) +
        Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
      if (dist > 1) {
        dist = 1;
      }
      dist = Math.acos(dist);
      dist = (dist * 180) / Math.PI;
      dist = dist * 60 * 1.1515;
      dist = dist * 1.609344;

      return dist;
    }
  } else {
    return false;
  }
};

export const hardwareBackHandler = navigateTo => {
  Alert.alert(
    'Visszalépsz az alkalmazás kezdőképernyőre?',
    '',
    [
      {
        text: 'Nem',
        onPress: undefined,
      },
      {
        text: 'Igen',
        onPress: () => {
          navigateTo('StartScreen');
        },
      },
    ],
    { cancelable: true },
  );
};

export const getAverage = array => array.reduce((a, b) => a + b) / array.length;
