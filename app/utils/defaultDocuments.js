const UserDocument = {
  id: '',
  name: '',
  picture: '',
  email: '',
  creditCards: [],
  addresses: [
    {
      name: 'home',
      label: 'Otthon',
      address: '',
      coordinates: '',
    },
    {
      name: 'office',
      label: 'Munkahelyi cím',
      address: '',
      coordinates: '',
    },
  ],
  ratings: [],
};

export default UserDocument;
