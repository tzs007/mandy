import axios from 'axios';
import { httpMethods } from './enums';

/**
 * call it with full endpoint url
 * e.g.:
 * api.get('https://api.endpoint.tld/v1/users');
 * or
 * const ENDPOINT = 'https://api.endpoint.tld/v1';
 * api.get(`${ENDPOINT}/users`)
 */

class Api {
  constructor() {
    this.axios = axios.create({
      headers: {
        'Content-Type': 'application/json',
      },
    });
  }

  request = async (endpoint, params) => {
    try {
      return await this.axios(endpoint, params);
    } catch (error) {
      console.log('api request error: ', error);
    }
  };

  get = (endpoint, params, headers) => {
    return this.request(endpoint, {
      method: httpMethods.GET,
      params,
      headers,
    });
  };

  post = (endpoint, data, headers) => {
    return this.request(endpoint, {
      method: httpMethods.POST,
      data,
      headers,
    });
  };

  put = (endpoint, data, headers) => {
    return this.request(endpoint, {
      method: httpMethods.PUT,
      data,
      headers,
    });
  };

  delete = (endpoint, data, headers) => {
    return this.request(endpoint, {
      method: httpMethods.DELETE,
      data,
      headers,
    });
  };
}

export default new Api();
