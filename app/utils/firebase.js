import * as firebase from "firebase";
import "firebase/firestore";
import { GeoFirestore } from "geofirestore";

import {
  FIREBASE_APIKEY,
  FIREBASE_AUTH_DOMAIN,
  FIREBASE_DATABASE_URL,
  FIREBASE_PROJECT_ID,
  FIREBASE_STORAGE_BUCKET,
  FIREBASE_MESSAGING_SENDER_ID,
} from "./constants";

const config = {
  apiKey: FIREBASE_APIKEY,
  authDomain: FIREBASE_AUTH_DOMAIN,
  databaseURL: FIREBASE_DATABASE_URL,
  projectId: FIREBASE_PROJECT_ID,
  storageBucket: FIREBASE_STORAGE_BUCKET,
  messagingSenderId: FIREBASE_MESSAGING_SENDER_ID,
};

firebase.initializeApp(config);

export const db = firebase.firestore();
export const firestore = firebase.firestore;
export const geoDb = new GeoFirestore(db);
export const auth = firebase.auth();
export const facebook = new firebase.auth.FacebookAuthProvider();
